<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver investor
 */
class Api extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $militime =round(microtime(true) * 1000);
        $datetime =date('Y-m-d h:i:s');
        define('militime', $militime);
        define('datetime', $datetime);
        date_default_timezone_get();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
      //  $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
     //   $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
       // $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
   
    }

    function notificationtest_post()
    {
        $token = 'ftd2GtZZu44:APA91bEOcDqdKbFaPvc7gx_sT88VPSY1j1I_9us16_1PQaXwV7uFL8y8y51gQLBABekjIPFFOkPaWdvHVgMDM-JlBRzlVerVZasYoqJEVbAEcJoZDoN1g6t8PrJg197eN0yyi-5bk7Dk';
        $arr = array('title'=>'New mock test created','msg'=>"Mock test created by yadav coaching",'image'=>'','type'=>1);
        $this->common_model->sendPushNotification($token,$arr);
    }

    function create_mock_test_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->course_id) && !empty($json_array->subject_id) && !empty($json_array->question_pattern))
                        {
                            $currdate = date('Y-m-d 00:00:00');
                            $userid = $tokencheck['data']->id;
                            $course_id = $json_array->course_id;
                            $subject_id = $json_array->subject_id;
                            $chapter = $json_array->question_pattern;
                            $organise_date = $json_array->organised_date;       
                            $time = $json_array->time;    
                            $newwallet = $tokencheck['data']->wallet;
                            $checkname = $this->db->select('course_name,time_per_question,sectional_price')->get_where("course",array('id'=>$course_id,'status'=>1))->row();
                            if(!empty($checkname))
                            {   
                                $coinstatus = 'false'; $wallet_coin = ''; $testprice = 0;
                                $selecttest = $this->common_model->common_getRow("mock_test",array('coaching_id'=>$userid));
                                if(!empty($selecttest))
                                {
                                    $testprice = $checkname->sectional_price;
                                    if($tokencheck['data']->user_subscription_status==1 && $tokencheck['data']->user_subscription_valid_till <= $currdate) 
                                    {
                                        $coinstatus = 'true';
                                    }else
                                    {
                                        if($tokencheck['data']->wallet >= $checkname->sectional_price)
                                        {
                                            $coinstatus = 'true';
                                            $wallet_coin = $tokencheck['data']->wallet;
                                        }
                                    }
                                }else
                                {
                                    $coinstatus = 'true'; //first test free
                                }

                                if($coinstatus =='true')
                                {
                                    if($json_array->total_no_of_q >20 && $json_array->total_no_of_q <= 30)
                                    {
                                        $comphcount = 1;
                                    }elseif($json_array->total_no_of_q >30 && $json_array->total_no_of_q <= 40)
                                    {
                                        $comphcount = 2;
                                    }elseif($json_array->total_no_of_q >40 && $json_array->total_no_of_q <= 50)
                                    {
                                        $comphcount = 3;
                                    }else
                                    {
                                        $comphcount = 4;    
                                    }
                                    $idarr =array();
                                    $count = count($chapter);
                                    for ($i=0; $i < $count; $i++) { 
                                        $ch_q_count = $chapter[$i]->no_of_q;
                                        if($comphcount>0)
                                        {
                                            $selecomph = $this->db->query("SELECT questions.comprehensive_id FROM `questions` INNER JOIN questions_comprehensive ON questions.comprehensive_id = questions_comprehensive.id WHERE questions_comprehensive.no_of_question <= ".$chapter[$i]->no_of_q." AND questions.chapter_id = ".$chapter[$i]->chapter_id." AND questions.question_type = 10 LIMIT 1")->row();
                                          //print_r($this->db->last_query());
                                            if(!empty($selecomph))
                                            {
                                                $checkdata = $this->db->select('id,subject_id')->get_where("questions",array('chapter_id'=>$chapter[$i]->chapter_id,'comprehensive_id'=>$selecomph->comprehensive_id))->result();
                                                if(!empty($checkdata))
                                                {
                                                    $newar =array();
                                                    foreach ($checkdata as $key) {
                                                     $idarr[] = $key;
                                                     $iddar[] = $key->id;
                                                     $newar[] = $key->id;   
                                                    }
                                                    $cqcount = count($newar);
                                                    $ch_q_count = $ch_q_count - $cqcount;
                                                }
                                                $comphcount = $comphcount-1;
                                                
                                            }else
                                            {
                                                $comphcount = $comphcount-1;
                                            }
                                        }
                                        $this->db->order_by('id','RANDOM');
                                        $this->db->limit($ch_q_count);
                                        $checkdata = $this->db->select('id,subject_id')->get_where("questions",array('chapter_id'=>$chapter[$i]->chapter_id,'question_type !='=>10))->result();
                                        if(!empty($checkdata))
                                        {
                                            foreach ($checkdata as $key) {
                                             $idarr[] = $key;
                                             $iddar[] = $key->id;
                                            }
                                        }
                                    }
                                    //print_r(implode(',',$iddar));exit;
                                    if(!empty($idarr))
                                    {
                                       $qcount = count($idarr);
                                       $imp_q_id = implode(',', $iddar);
                                       $selsubject = $this->db->query("SELECT id,title as subject_name  FROM course_subject WHERE id in ($subject_id)")->result();
                                       if(!empty($selsubject))
                                       {    
                                            foreach ($selsubject as $value) {

                                                $subarr =array();
                                                for ($i=0; $i < $qcount ; $i++) { 
                                                    if($value->id ==$idarr[$i]->subject_id)
                                                    {
                                                        $subarr[] = $idarr[$i]->id;
                                                    }
                                                }
                                                    $arr[] = array(
                                                            'subject_id'=>$value->id,
                                                            'subject_name'=>$value->subject_name,
                                                            'question'=>$subarr
                                                            );
                                            }
                                        $arrr = array('que'=>$arr);
                                       }
                                       $test_name = $checkname->course_name.'_'.$this->common_model->randomuniqueCode();
                                       $json_array->test_name = $test_name;
                                       $json_array->questions_id = $imp_q_id;
                                       $json_array->subject_pettern = json_encode($arrr);
                                       $json_array->create_at = date('Y-m-d h:i:s');
                                       $json_array->no_of_que = $qcount;
                                       $json_array->time = $time;
                                       $json_array->coaching_id = $userid;
                                       $json_array->type = 1;
                                       $json_array->question_pattern = json_encode($chapter);
                                       $json_array->token = md5($this->common_model->uniquetoken().$header['secret_key'].militime); 
                                      //print_r($json_array);
                                        unset($json_array->total_no_of_q);
                                        $insert = $this->common_model->common_insert("mock_test",$json_array);
                                        

                                        if($insert!=false)
                                        {   
                                            if(($wallet_coin!='') || ($tokencheck['data']->user_subscription_status==1))
                                            {
                                                $paystatus='free';
                                                 if($wallet_coin >= $checkname->sectional_price && $tokencheck['data']->user_subscription_status!=1)
                                                {
                                                    $newwallet = $wallet_coin - $checkname->sectional_price;
                                                    $this->common_model->updateData('user',array('wallet'=>$newwallet),array('id'=>$userid));
                                                    $paystatus='paid';
                                                }
                                                $walle_history = $this->common_model->common_insert("user_transactions",array('user_id'=>$userid,'test_id'=>$insert,'test_type'=>2,'coin_paid'=>$testprice,'pay_mode'=>$paystatus,'transaction_type'=>0,'create_at'=>date('Y-m-d H:i:s')));
                                            }
                                            $this->db->where('id',$insert);
                                            $this->db->select('token');
                                            $res = $this->db->get('mock_test')->row();
                                            $final_output['statusText'] = 'success';
                                            $final_output['data']=$res->token;
                                            $final_output['wallet'] = $newwallet;
                                            $final_output['message'] = 'Mock paper successfully created.';
                                            $final_output['status'] = 200;   
                                        }else
                                        {
                                            $final_output['statusText'] = 'failed';
                                            $final_output['message'] = 'Something went wrong! please try again later.';    
                                            $final_output['status'] = 500;       
                                        }
                                    }else
                                    {
                                        $final_output['statusText'] = 'failed';
                                        $final_output['message'] = 'Something went wrong! please try again later.';    
                                        $final_output['status'] = 500;
                                    }
                                }else
                                {
                                    $final_output['statusText'] = 'failed';
                                    $final_output['message'] = 'You dont have sufficient witcoins to create the test.';    
                                    $final_output['status'] = 200;
                                }
                            }else
                            {
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'Currently this course not available.';    
                                $final_output['status'] = 200;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        http_response_code($final_output['status']);
        echo json_encode($final_output);
    }
    //create mock test

    // function create_mock_test_post()
    // {
    //     $header  = apache_request_headers();
    //     if(!empty($header))
    //     {
    //         $tokencheck = $this->ChechAuth($header['secret_key']);

    //         if($tokencheck['status'] == 'true')
    //         {
    //             if($tokencheck['data']->admin_status==1)
    //             {
    //                 $json1 = file_get_contents('php://input');
    //                 $json_array = json_decode($json1);
    //                 $final_output =array();
    //                 if($json_array)
    //                 {
    //                     if(!empty($json_array->course_id) && !empty($json_array->subject_id) && !empty($json_array->question_pattern))
    //                     {
    //                         $currdate = date('Y-m-d 00:00:00');
    //                         $userid = $tokencheck['data']->id;
    //                         $course_id = $json_array->course_id;
    //                         $subject_id = $json_array->subject_id;
    //                         $chapter = $json_array->question_pattern;
    //                         $organise_date = $json_array->organised_date;       
    //                         $time = $json_array->time;
    //                         $newwallet = $tokencheck['data']->wallet;          
    //                         $checkname = $this->db->select('course_name,time_per_question,sectional_price')->get_where("course",array('id'=>$course_id,'status'=>1))->row();
    //                         if(!empty($checkname))
    //                         {   
    //                            $coinstatus = 'false'; $wallet_coin = ''; $testprice = 0;
    //                             $selecttest = $this->common_model->common_getRow("mock_test",array('coaching_id'=>$userid));
    //                             if(!empty($selecttest))
    //                             {
    //                                 $testprice = $checkname->sectional_price;
    //                                 if($tokencheck['data']->user_subscription_status==1 && $tokencheck['data']->user_subscription_valid_till >= $currdate) 
    //                                 {
    //                                     $coinstatus = 'true';
    //                                 }else
    //                                 {
    //                                     if($tokencheck['data']->wallet >= $checkname->sectional_price)
    //                                     {
    //                                         $coinstatus = 'true';
    //                                         $wallet_coin = $tokencheck['data']->wallet;
    //                                     }
    //                                 }
    //                             }else
    //                             {
    //                                 $coinstatus = 'true'; //first test free
    //                             }

    //                             if($coinstatus =='true')
    //                             {
    //                                 $idarr =array();
    //                                 $count = count($chapter);
    //                                 for ($i=0; $i < $count; $i++) { 
    //                                     $this->db->order_by('id','RANDOM');
    //                                     $this->db->limit($chapter[$i]->no_of_q);
    //                                     $checkdata = $this->db->select('id,subject_id')->get_where("questions",array('chapter_id'=>$chapter[$i]->chapter_id,'question_type !='=>'10'))->result();
    //                                     if(!empty($checkdata))
    //                                     {
    //                                         foreach ($checkdata as $key) {
    //                                          $idarr[] = $key;
    //                                          $iddar[] = $key->id;
    //                                         }
    //                                     }
    //                                 }
    //                                 if(!empty($idarr))
    //                                 {
    //                                    $qcount = count($idarr);
    //                                    $imp_q_id = implode(',', $iddar);
    //                                    $selsubject = $this->db->query("SELECT id,title as subject_name  FROM course_subject WHERE id in ($subject_id)")->result();
    //                                    if(!empty($selsubject))
    //                                    {    
    //                                         foreach ($selsubject as $value) {

    //                                             $subarr =array();
    //                                             for ($i=0; $i < $qcount ; $i++) { 
    //                                                 if($value->id ==$idarr[$i]->subject_id)
    //                                                 {
    //                                                     $subarr[] = $idarr[$i]->id;
    //                                                 }
    //                                             }
    //                                                 $arr[] = array(
    //                                                         'subject_id'=>$value->id,
    //                                                         'subject_name'=>$value->subject_name,
    //                                                         'question'=>$subarr
    //                                                             );
    //                                         }
    //                                     $arrr = array('que'=>$arr);
    //                                    }
    //                                    $test_name = $checkname->course_name.'_'.$this->common_model->randomuniqueCode();
    //                                    $json_array->test_name = $test_name;
    //                                    $json_array->questions_id = $imp_q_id;
    //                                    $json_array->subject_pettern = json_encode($arrr);
    //                                    $json_array->create_at = date('Y-m-d h:i:s');
    //                                    $json_array->no_of_que = $qcount;
    //                                    $json_array->time = $time;
    //                                    $json_array->coaching_id = $userid;
    //                                    $json_array->type = 1;
    //                                    $json_array->question_pattern = json_encode($chapter);
    //                                    $json_array->token = md5($this->common_model->uniquetoken().$header['secret_key'].militime); 
    //                                   //print_r($json_array);
                                        
    //                                     $insert = $this->common_model->common_insert("mock_test",$json_array);
                                        

    //                                     if($insert!=false)
    //                                     {   
    //                                         if(($wallet_coin!='') || ($tokencheck['data']->user_subscription_status==1))
    //                                         {
    //                                             $paystatus='free';
    //                                             if($wallet_coin >= $checkname->sectional_price && $tokencheck['data']->user_subscription_status!=1)
    //                                             {
    //                                                 $newwallet = $wallet_coin - $checkname->sectional_price;
    //                                                 $this->common_model->updateData('user',array('wallet'=>$newwallet),array('id'=>$userid));
    //                                                 $paystatus='paid';
    //                                             }
    //                                             $walle_history = $this->common_model->common_insert("user_transactions",array('user_id'=>$userid,'test_id'=>$insert,'test_type'=>2,'coin_paid'=>$testprice,'pay_mode'=>$paystatus,'transaction_type'=>0,'create_at'=>date('Y-m-d H:i:s')));
    //                                         }
    //                                         $this->db->where('id',$insert);
    //                                         $this->db->select('token');
    //                                         $
    //                                         $res = $this->db->get('mock_test')->row();
    //                                         $final_output['statusText'] = 'success';
    //                                         $final_output['data']=$res->token;
    //                                         $final_output['wallet']= $newwallet;
    //                                         $final_output['message'] = 'Mock paper successfully created.';
    //                                         $final_output['status'] = 200;   
    //                                     }else
    //                                     {
    //                                         $final_output['statusText'] = 'failed';
    //                                         $final_output['message'] = 'Something went wrong! please try again later.';    
    //                                         $final_output['status'] = 500;       
    //                                     }
    //                                 }else
    //                                 {
    //                                     $final_output['statusText'] = 'failed';
    //                                     $final_output['message'] = 'Something went wrong! please try again later.';    
    //                                     $final_output['status'] = 500;
    //                                 }
    //                             }else
    //                             {
    //                                 $final_output['statusText'] = 'failed';
    //                                 $final_output['message'] = 'You dont have sufficient witcoins to create the test.';    
    //                                 $final_output['status'] = 200;
    //                             }
    //                         }else
    //                         {
    //                             $final_output['statusText'] = 'failed';
    //                             $final_output['message'] = 'Currently this course not available.';    
    //                             $final_output['status'] = 200;
    //                         }
    //                     }else
    //                     {
    //                         $final_output['statusText'] = 'failed';
    //                         $final_output['message'] = 'Invalid request parameter';    
    //                         $final_output['status'] = 412;
    //                     }
    //                 }
    //                 else{
    //                     $final_output['statusText'] = 'failed';
    //                     $final_output['message'] = 'No request parameter';
    //                     $final_output['status'] = 412;
    //                 }
    //             }else
    //             {
    //                 $final_output['statusText'] = 'failed';
    //                 $final_output['message'] = 'Your account temporary suspended for security reason.';
    //                 $final_output['status'] = 200;
    //             }
    //         }else
    //         {
    //             $final_output['statusText'] = 'false';
    //             $final_output['message'] = 'Invalid token';
    //             $final_output['status'] = 401;
    //         }
    //     }else
    //     {
    //         $final_output['statusText'] = 'false';
    //         $final_output['message'] = 'Unauthorized access';
    //         $final_output['status'] = 401;
    //     }
    //     header("content-type: application/json");
    //     http_response_code($final_output['status']);
    //     echo json_encode($final_output);
    // }
    //create mock test

    function submit_test_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {   
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output = $test_result_detail = array();
                    if(!empty($json_array))
                    {
                        if(!empty($json_array->que))
                        {
                            $msg = '';
                            $this->db->join("course",'course.id=mock_test.course_id');
                            $testcourse = $this->db->select('course.id,course.sectional_positive_mark,course.sectional_negative_mark,mock_test.id as testid,mock_test.time as test_time,mock_test.test_status,mock_test.type')->get_where("mock_test",array('mock_test.token'=>$json_array->reference_t_id,'course.status'=>1))->row();
                            
                            if(!empty($testcourse))
                            {
                                if($testcourse->type == 1 && $testcourse->test_status !=0)
                                {
                                    $msg = 'Already attempted';
                                }
                                if($testcourse->type == 2 && $testcourse->test_status ==1)
                                {
                                    $this->db->join("submit_test",'submit_test.test_id=user_transactions.test_id AND submit_test.user_id=user_transactions.user_id','inner');
                                    
                                    $testcourse1 = $this->db->select('user_transactions.id,submit_test.id')->get_where("user_transactions",array('user_transactions.test_id'=>$testcourse->testid,'submit_test.test_id !='=>$testcourse->testid,'submit_test.user_id !='=>$tokencheck['data']->id,'user_transactions.user_id'=>$tokencheck['data']->id,'user_transactions.test_type'=>2))->row();
                                    if(empty($testcourse1))
                                    {
                                    }else
                                    {
                                        $msg = 'Either you have not purchased test or you have already submit this test.';
                                    }
                                }

                                if(($testcourse->type == 2) && ($testcourse->test_status !=1))
                                {
                                    $msg = 'not active';
                                }
                                if($msg=='')
                                {
                                    $quecount = count($json_array->que);
                                    $positivemark = $testcourse->sectional_positive_mark;
                                    $nagativemark = $testcourse->sectional_negative_mark;
                                    $test_total_time = $testcourse->test_time;
                                    $total_time_taken_in_all_sub = $correct_ans_count = $wrong_ans_count = $notvisit_ans_count = $total_attemptquestion = 0;
                                    for ($i=0; $i < $quecount; $i++) {

                                        $attempt_quearr = $not_attempt_quearr = $attempt_ansarr = array();
                                        $quest_count = count($json_array->que[$i]->question);
                                        $questions= $json_array->que[$i]->question;
                                        $total_takentime = 0;
                                        for ($j=0; $j < $quest_count; $j++) { 
                                                if($questions[$j]->status==1)
                                                {
                                                    $attempt_quearr[] = $questions[$j]->id;
                                                    $attempt_ansarr[] = $questions[$j]->response_id;
                                                    $total_takentime += $questions[$j]->taken_time;
                                                }else
                                                {
                                                    $not_attempt_quearr[] = $questions[$j]->id;
                                                    $total_takentime += $questions[$j]->taken_time;
                                                }
                                        }
                                        $correct_ans_cnt = $wrong_ans_cnt = array();
                                        if(!empty($attempt_quearr))
                                        {
                                            $implodearr= implode(',', $attempt_quearr);
                                            $quecountt = count($attempt_quearr);
                                           // print_r($attempt_ansarr);exit;
                                            $que_ans = $this->db->query("SELECT id,question_type,answer FROM questions WHERE id IN ($implodearr)")->result();
                                            if(!empty($que_ans))
                                            {
                                                $anscount = count($que_ans);   
                                                for ($k=0; $k < $anscount; $k++) { 
                                                   
                                                    for ($l=0; $l < $quecountt; $l++) { 
                                                        if($que_ans[$k]->id==$attempt_quearr[$l])
                                                        {
                                                            if($que_ans[$k]->question_type==5)
                                                            {
                                                                $fid =explode(',', $que_ans[$k]->answer);
                                                                $secid = explode(',', $attempt_ansarr[$l]);
                                                                $result = array_merge(array_diff($fid, $secid), array_diff($secid, $fid));
                                                                if(!empty($result))
                                                                {
                                                                    $wrong_ans_cnt[] = $attempt_quearr[$l];
                                                                }else
                                                                {
                                                                    $correct_ans_cnt[] = $attempt_quearr[$l];
                                                                }
                                                            }else
                                                            {
                                                                if($que_ans[$k]->answer == $attempt_ansarr[$l])
                                                                {
                                                                    $correct_ans_cnt[] = $attempt_quearr[$l];
                                                                }else
                                                                {
                                                                    $wrong_ans_cnt[] = $attempt_quearr[$l];
                                                                }
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        $total_question = $quest_count;
                                        $total_subject_score = $quest_count*$positivemark;
                                        $total_attempt_question = count($attempt_quearr);
                                        $total_correct_answer = count($correct_ans_cnt);
                                        $total_wrong_answer = count($wrong_ans_cnt);
                                        $total_notvisited = count($not_attempt_quearr);
                                        $total_time_taken = gmdate("i:s", $total_takentime);
                                        if(!empty($attempt_quearr) && count($attempt_quearr)!=0)
                                        {
                                            $accuracy = $total_correct_answer * 100 / $total_attempt_question;
                                        }else
                                        {
                                            $accuracy = 0;
                                        }
                                        $positivemarkss = $positivemark*$total_correct_answer;
                                        $nagativemarkss = $nagativemark*$total_wrong_answer;
                                        $total_score = $positivemarkss - $nagativemarkss;
                                        $total_time_taken_in_all_sub += $total_takentime;
                                        $test_result_detail[] = array('subject_name'=>$json_array->que[$i]->subject_name,'total_question'=>$quest_count,'total_attempt_question'=>$total_attempt_question,'total_correct_answer'=>$total_correct_answer,'total_wrong_answer'=>$total_wrong_answer,'not_visited'=>$total_notvisited,'total_time_taken'=>$total_time_taken,'accuracy'=>round($accuracy,2),'total_score'=>$total_score,'total_subject_score'=>$total_subject_score); 
                                        $correct_ans_count += $total_correct_answer;
                                        $wrong_ans_count += $total_wrong_answer;
                                        $notvisit_ans_count += $total_notvisited;
                                        $total_attemptquestion += $total_attempt_question;
                                       // echo $accuracy;
                                        // echo '<pre>';
                                        // echo $total_correct_answer;
                                        // echo '<pre>';
                                        // echo $total_wrong_answer;
                                        // echo '<pre>';
                                        // echo $total_time_taken;
                                        // echo '<pre>';
                                        // echo $total_attempt_question;
                                    }
                                    if(!empty($test_result_detail))
                                    {
                                        $insert = $this->common_model->common_insert("submit_test",array('user_id'=>$tokencheck['data']->id,'correct_ans'=>$correct_ans_count,'wrong_ans'=>$wrong_ans_count,'not_visited'=>$notvisit_ans_count,'total_attemt_ques'=>$total_attemptquestion,'test_type'=>2,'test_token'=>$json_array->reference_t_id,'taken_time'=>gmdate("i:s", $total_time_taken_in_all_sub),'test_total_time'=>$test_total_time,'response'=>json_encode($json_array->que),'test_id'=>$testcourse->testid,'score'=>$total_score,'test_detail'=>json_encode($test_result_detail),'create_at'=>date('Y-m-d H:i:s')));
                                        if($insert!=false)
                                        {
                                            $this->common_model->updateData("mock_test",array('test_status'=>1,'update_at'=>date("Y-m-d H:i:s")),array('id'=>$testcourse->testid));                                       
                                            $final_output['statusText'] = 'success';
                                            $final_output['message'] = 'Mock paper successfully submited.';    
                                            $final_output['status'] = 200;   
                                        }else
                                        {
                                            $final_output['statusText'] = 'failed';
                                            $final_output['message'] = 'Something went wrong! please try again later.';    
                                            $final_output['status'] = 500;       
                                        }
                                    }
                                }else
                                {   
                                    $final_output['statusText'] = 'failed';
                                    $final_output['message'] = $msg;    
                                    $final_output['status'] = 200; 
                                }    
                            
                            }else
                            {
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'Test not found.';    
                                $final_output['status'] = 200;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        http_response_code($final_output['status']);
        echo json_encode($final_output);
    }
    //submit mock test

    function get_question_for_mock_new_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->course_id) && !empty($json_array->question_pattern))
                        {
                            if($tokencheck['data']->user_type == 2)
                            {
                                $currdate = date('Y-m-d 00:00:00');
                                $userid = $tokencheck['data']->id;
                                $course_id = $json_array->course_id;
                                $subject_id = $json_array->subject_id;
                                $chapter = $json_array->question_pattern;
                                // $organise_date = $json_array->organised_date;
                                // $end_date = $json_array->end_date;       
                                // $time = $json_array->time;     

                                $compcountt[] = 0;     
                                $checkname = $this->db->select('course_name')->get_where("course",array('id'=>$course_id,'status'=>1))->row();
                                if(!empty($checkname))
                                {   
                                        $idarr = $arrr = array();
                                        $count = count($chapter);
                                        $questionarr = array();
                                        if($json_array->total_no_of_q >20 && $json_array->total_no_of_q <= 30)
                                        {
                                            $comphcount = 1;
                                        }elseif($json_array->total_no_of_q >30 && $json_array->total_no_of_q <= 40)
                                        {
                                            $comphcount = 2;
                                        }elseif($json_array->total_no_of_q >40 && $json_array->total_no_of_q <= 50)
                                        {
                                            $comphcount = 3;
                                        }else
                                        {
                                            $comphcount = 4;    
                                        }
                                            $total_q=0;
                                            for ($j=0; $j < $count; $j++) { 
                                                $ch_q_count = $chapter[$j]->no_of_q * 2;
                                                $total_q = $total_q+ $chapter[$j]->no_of_q * 2;
                                                if($comphcount>0)
                                                {
                                                    $selecomph = $this->db->query("SELECT questions.comprehensive_id,questions.question_type,questions_comprehensive.question FROM `questions` INNER JOIN questions_comprehensive ON questions.comprehensive_id = questions_comprehensive.id WHERE questions_comprehensive.no_of_question <= ".$chapter[$j]->no_of_q*'2'." AND questions.chapter_id = ".$chapter[$j]->chapter_id." AND questions.question_type = 10 ORDER BY RAND() LIMIT 1")->row();
                                                    if(!empty($selecomph))
                                                    {
                                                        $checkdata = $this->db->select('id,subject_id,question')->get_where("questions",array('chapter_id'=>$chapter[$j]->chapter_id,'comprehensive_id'=>$selecomph->comprehensive_id))->result();
                                                        if(!empty($checkdata))
                                                        {
                                                            $newar = $compreques = array();
                                                            foreach ($checkdata as $key) {
                                                             $idarr[] = $key->subject_id;
                                                             $compreques[]= array('id'=>$key->id,'question'=>$key->question);
                                                             //$iddar[] = $key->id;
                                                             $newar[] = $key->id;   
                                                            }
                                                            $questionarr[] = array('comp_que'=>$selecomph->question,'que_type'=>$selecomph->question_type,'que'=>$compreques);
                                                            $cqcount = count($newar);
                                                            $compcountt[] = 1;
                                                            $ch_q_count = $ch_q_count - $cqcount;
                                                            $comphcount = $comphcount-1;
                                                        }
                                                    }else
                                                    {
                                                        $comphcount = $comphcount-1;
                                                    }
                                                }
                                                $this->db->order_by('id','RANDOM');
                                                $this->db->limit($ch_q_count);
                                                $checkdata = $this->db->select('id,subject_id,question,question_type')->get_where("questions",array('chapter_id'=>$chapter[$j]->chapter_id,'question_type !='=>10))->result();
                                                if(!empty($checkdata))
                                                {
                                                    foreach ($checkdata as $key) {
                                                        $questionarr[]= array('que_type'=>$key->question_type,'id'=>$key->id,'question'=>$key->question);
                                                        $idarr[] = $key->subject_id;
                                                    }

                                                }
                                            }
                                            
                                            if(!empty($questionarr))
                                            {
                                                $qcount = count($questionarr); 
                                                $requcount = $qcount + count($compcountt);
                                                $selsubject = $this->db->query("SELECT id,title as subject_name  FROM course_subject WHERE id in ($subject_id)")->result();
                                                if(!empty($selsubject))
                                                {    
                                                    foreach ($selsubject as $value) {

                                                        $subarr =array();
                                                        for ($i=0; $i < $qcount ; $i++) { 
                                                            if($value->id ==$idarr[$i])
                                                            {
                                                                $subarr[] = $questionarr[$i];
                                                            }
                                                        }
                                                        $arr[] = array(
                                                                'subject_name'=>$value->subject_name,
                                                                'subject_id'=>$value->id,
                                                                'required_question'=>$total_q/2,
                                                                'question'=>$subarr
                                                                );
                                                    }
                                                    $arrr = array('que'=>$arr);
                                                }
                                            }
                                        if($arrr!='')
                                        {   
                                            $final_output['statusText'] = 'success';
                                            $final_output['message'] = 'question successfully get.';
                                            $final_output['status'] = 200;   
                                            $final_output['data']=$arrr;
                                        }else
                                        {
                                            $final_output['statusText'] = 'failed';
                                            $final_output['message'] = 'Questions not available.';    
                                            $final_output['status'] = 200;       
                                        }
                                }else
                                {
                                    $final_output['statusText'] = 'failed';
                                    $final_output['message'] = 'Currently this course not available.';    
                                    $final_output['status'] = 200;
                                }
                            }else
                            {
                                $final_output['statusText'] = 'false';
                                $final_output['message'] = 'Unauthorized access';
                                $final_output['status'] = 401;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }
    //moke test question for coaching

    function get_question_for_mock_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);
            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->course_id) && !empty($json_array->question_pattern))
                        {
                            if($tokencheck['data']->user_type == 2)
                            {
                                $currdate = date('Y-m-d 00:00:00');
                                $userid = $tokencheck['data']->id;
                                $course_id = $json_array->course_id;
                                $subject_id = $json_array->subject_id;
                                $chapter = $json_array->question_pattern;
                                //$organise_date = $json_array->organised_date;       
                                //$time = $json_array->time;          
                                $checkname = $this->db->select('course_name')->get_where("course",array('id'=>$course_id,'status'=>1))->row();
                                if(!empty($checkname))
                                {   
                                        $idarr = $arrr = array();
                                        $count = count($chapter);
                                        $questionarr = array();
                                            for ($j=0; $j < $count; $j++) { 
                                               
                                                $this->db->order_by('id','RANDOM');
                                                $this->db->limit($chapter[$j]->no_of_q*2);
                                                $checkdata = $this->db->select('id,subject_id,question')->get_where("questions",array('chapter_id'=>$chapter[$j]->chapter_id))->result();
                                                if(!empty($checkdata))
                                                {
                                                    foreach ($checkdata as $key) {
                                                        $questionarr[]= array('id'=>$key->id,'question'=>$key->question);
                                                        $idarr[] = $key->subject_id;
                                                    }

                                                }
                                            }
                                            if(!empty($questionarr))
                                            {
                                                $qcount = count($questionarr);
                                                $selsubject = $this->db->query("SELECT id,title as subject_name  FROM course_subject WHERE id in ($subject_id)")->result();
                                                if(!empty($selsubject))
                                                {    
                                                    foreach ($selsubject as $value) {

                                                        $subarr =array();
                                                        for ($i=0; $i < $qcount ; $i++) { 
                                                            if($value->id ==$idarr[$i])
                                                            {
                                                                $subarr[] = $questionarr[$i];
                                                            }
                                                        }

                                                        $arr[] = array(
                                                                'subject_name'=>$value->subject_name,
                                                                'subject_id'=>$value->id,
                                                                'required_question'=>count($subarr)/2,
                                                                'question'=>$subarr
                                                                );
                                                    }
                                                    $arrr = array('que'=>$arr);
                                                }
                                            }
                                        if($arrr!='')
                                        {   
                                            $final_output['statusText'] = 'success';
                                            $final_output['message'] = 'questoin successfully get.';
                                            $final_output['status'] = 200;   
                                            $final_output['data']=$arrr;
                                        }else
                                        {
                                            $final_output['statusText'] = 'failed';
                                            $final_output['message'] = 'Questions not available.';    
                                            $final_output['status'] = 200;       
                                        }
                                }else
                                {
                                    $final_output['statusText'] = 'failed';
                                    $final_output['message'] = 'Currently this course not available.';    
                                    $final_output['status'] = 200;
                                }
                            }else
                            {
                                $final_output['statusText'] = 'false';
                                $final_output['message'] = 'Unauthorized access';
                                $final_output['status'] = 401;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }

    function create_mocktest_coaching_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->course_id) && !empty($json_array->subject_id) && !empty($json_array->question_pattern))
                        {
                            $currdate = date('Y-m-d 00:00:00');
                            $userid = $tokencheck['data']->id;
                            $checkname = $this->db->select('course_name')->get_where("course",array('id'=>$json_array->course_id,'status'=>1))->row();
                            if(!empty($checkname))
                            {   
                                   //$test_name = $checkname->course_name.'_'.$this->common_model->randomuniqueCode();
                                   //json_array->test_name = $test_name;
                                   //$json_array->questions_id = $imp_q_id;
                                   $json_array->subject_pettern = json_encode($json_array->subject_pettern);
                                   $json_array->question_pattern = json_encode($json_array->question_pattern);
                                   $json_array->create_at = date('Y-m-d h:i:s');
                                   //$json_array->no_of_que = $qcount;
                                   //$json_array->time = $time;
                                   $json_array->coaching_id = $userid;
                                   $json_array->type = 2;
                                   $token = md5($this->common_model->uniquetoken().$header['secret_key'].militime);
                                   $json_array->token =  $token;
                                  //print_r($json_array);
                                    $insert = $this->common_model->common_insert("mock_test",$json_array);
                                    if($insert!=false)
                                    {   
                                        // $seltoken = $this->db->query("SELECT id,device_token FROM user WHERE admin_status=1 AND FIND_IN_SET($userid,follow_coaching)")->result_array();
                                        // if(!empty($seltoken))
                                        // {
                                        //     $gcmRegIds = array();
                                        //     $i = 0;
                                        //     foreach ($seltoken as $query_row) 
                                        //     {
                                        //         $i++;
                                        //         if($query_row['device_token'] == '')
                                        //         {
                                        //             $user[] = $query_row['id'];
                                        //         }else
                                        //         {
                                        //             $user[] = $query_row['id'];
                                        //             $gcmRegIds[floor($i/1000)][] = $query_row['device_token'];
                                        //             //$insert_data[] = '("","'.$aa['data']->user_id.'","'.$query_row['user_id'].'",6,0,0,"'.datetime.'","","")';
                                        //         }
                                        //     }
                                        //     if(isset($gcmRegIds)) 
                                        //     {
                                        //         $msg = "Mock test create test by ".$tokencheck['data']->name."";
                                        //         $arr = array('title'=>'New mock test created','msg'=>$msg,'type'=>1);
                                        //         $message = $arr;
                                        //         $pushStatus = array();
                                        //         foreach($gcmRegIds as $val) $pushStatus[] = $this->common_model->sendNotification($val, $message);
                                        //     }
                                            
                                        //     if(!empty($user))
                                        //     {
                                        //         $notification_data = implode(',',$user);
                                        //         $insertt = $this->db->query("INSERT INTO notification (sender_id,receiver_id,id,type,msg,create_date) VALUES ('$userid','$notification_data','$insert',1,'$msg','".date('Y-m-d H:i:s')."')");
                                        //     }               
                                        // }

                                        $final_output['statusText'] = 'success';
                                        $final_output['data']=$token;
                                        $final_output['message'] = 'Mock paper successfully created.';
                                        $final_output['status'] = 200;   
                                    }else
                                    {
                                        $final_output['statusText'] = 'failed';
                                        $final_output['message'] = 'Something went wrong! please try again later.';    
                                        $final_output['status'] = 500;       
                                    }
                            }else
                            {
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'Currently this course not available.';    
                                $final_output['status'] = 200;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }
    //create mock test coaching 02-02-18

    function auto_create_mock_test_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->course_id) && !empty($json_array->subject_id) && !empty($json_array->question_pattern))
                        {
                            $currdate = date('Y-m-d 00:00:00');
                            $userid = $tokencheck['data']->id;
                            $course_id = $json_array->course_id;
                            $subject_id = $json_array->subject_id;
                            $chapter = $json_array->question_pattern;
                            $organise_date = $json_array->organised_date;
                            $end_date = $json_array->end_date;       

                            $time = $json_array->time;          
                            $checkname = $this->db->select('course_name,time_per_question,sectional_price')->get_where("course",array('id'=>$course_id,'status'=>1))->row();
                            if(!empty($checkname))
                            {   
                                if($json_array->total_no_of_q >20 && $json_array->total_no_of_q <= 30)
                                    {
                                        $comphcount = 1;
                                    }elseif($json_array->total_no_of_q >30 && $json_array->total_no_of_q <= 40)
                                    {
                                        $comphcount = 2;
                                    }elseif($json_array->total_no_of_q >40 && $json_array->total_no_of_q <= 50)
                                    {
                                        $comphcount = 3;
                                    }else
                                    {
                                        $comphcount = 4;    
                                    }
                                    $idarr =array();
                                    $count = count($chapter);
                                    for ($i=0; $i < $count; $i++) { 
                                        $ch_q_count = $chapter[$i]->no_of_q;
                                        if($comphcount>0)
                                        {
                                            $selecomph = $this->db->query("SELECT questions.comprehensive_id FROM `questions` INNER JOIN questions_comprehensive ON questions.comprehensive_id = questions_comprehensive.id WHERE questions_comprehensive.no_of_question <= ".$chapter[$i]->no_of_q." AND questions.chapter_id = ".$chapter[$i]->chapter_id." AND questions.question_type = 10 LIMIT 1")->row();
                                          //print_r($this->db->last_query());
                                            if(!empty($selecomph))
                                            {
                                                $checkdata = $this->db->select('id,subject_id')->get_where("questions",array('chapter_id'=>$chapter[$i]->chapter_id,'comprehensive_id'=>$selecomph->comprehensive_id))->result();
                                                if(!empty($checkdata))
                                                {
                                                    $newar =array();
                                                    foreach ($checkdata as $key) {
                                                     $idarr[] = $key;
                                                     $iddar[] = $key->id;
                                                     $newar[] = $key->id;   
                                                    }
                                                    $cqcount = count($newar);
                                                    $ch_q_count = $ch_q_count - $cqcount;
                                                }
                                                $comphcount = $comphcount-1;
                                                
                                            }else
                                            {
                                                $comphcount = $comphcount-1;
                                            }
                                        }
                                        $this->db->order_by('id','RANDOM');
                                        $this->db->limit($ch_q_count);
                                        $checkdata = $this->db->select('id,subject_id')->get_where("questions",array('chapter_id'=>$chapter[$i]->chapter_id,'question_type !='=>10))->result();
                                        if(!empty($checkdata))
                                        {
                                            foreach ($checkdata as $key) {
                                             $idarr[] = $key;
                                             $iddar[] = $key->id;
                                            }
                                        }
                                    }
                                if(!empty($idarr))
                                {
                                   $qcount = count($idarr); $arrr =array();
                                   $imp_q_id = implode(',', $iddar);
                                   $selsubject = $this->db->query("SELECT id,title as subject_name  FROM course_subject WHERE id in ($subject_id)")->result();
                                   if(!empty($selsubject))
                                   {    
                                        foreach ($selsubject as $value) {

                                            $subarr =array();
                                            for ($i=0; $i < $qcount ; $i++) { 
                                                if($value->id ==$idarr[$i]->subject_id)
                                                {
                                                    $subarr[] = $idarr[$i]->id;
                                                }
                                            }
                                                $arr[] = array(
                                                        'subject_name'=>$value->subject_name,
                                                        'question'=>$subarr
                                                            );
                                        }
                                    $arrr = array('que'=>$arr);
                                   }
                                   $json_array->questions_id = $imp_q_id;
                                   $json_array->subject_pettern = json_encode($arrr);
                                   $json_array->create_at = date('Y-m-d h:i:s');
                                   $json_array->no_of_que = $qcount;
                                   
                                   $json_array->coaching_id = $userid;
                                   $json_array->type = 2;
                                   $json_array->question_pattern = json_encode($chapter);
                                   $json_array->token = md5($this->common_model->uniquetoken().$header['secret_key'].militime); 
                                    unset($json_array->total_no_of_q);
                                   //print_r($json_array);
                                   $insert = $this->common_model->common_insert("mock_test",$json_array);
                                    if($insert!=false)
                                    {   
                                        // $seltoken = $this->db->query("SELECT id,device_token FROM user WHERE admin_status=1 AND FIND_IN_SET($userid,follow_coaching)")->result_array();
                                        // if(!empty($seltoken))
                                        // {
                                        //     $gcmRegIds = array();
                                        //     $i = 0;
                                        //     foreach ($seltoken as $query_row) 
                                        //     {
                                        //         $i++;
                                        //         if($query_row['device_token'] == '')
                                        //         {
                                        //             $user[] = $query_row['id'];
                                        //         }else
                                        //         {
                                        //             $user[] = $query_row['id'];
                                        //             $gcmRegIds[floor($i/1000)][] = $query_row['device_token'];
                                        //             //$insert_data[] = '("","'.$aa['data']->user_id.'","'.$query_row['user_id'].'",6,0,0,"'.datetime.'","","")';
                                        //         }
                                        //     }
                                        //     if(isset($gcmRegIds)) 
                                        //     {
                                        //         $msg = "Mock test create test by ".$tokencheck['data']->name."";
                                        //         $arr = array('title'=>'New mock test created','msg'=>$msg,'type'=>2);
                                        //         $message = $arr;
                                        //         $pushStatus = array();
                                        //         foreach($gcmRegIds as $val) $pushStatus[] = $this->common_model->sendNotification($val, $message);
                                        //     }
                                            
                                        //     if(!empty($user))
                                        //     {
                                        //         $notification_data = implode(',',$user);
                                        //         $insertt = $this->db->query("INSERT INTO notification (sender_id,receiver_id,id,type,msg,create_date) VALUES ('$userid','$notification_data','$insert',1,'$msg','".date('Y-m-d H:i:s')."')");
                                        //     }               
                                        // }
                                        $final_output['statusText'] = 'success';
                                        $final_output['data']=$json_array->token;
                                        $final_output['message'] = 'Mock paper successfully created.';
                                        $final_output['status'] = 200;   
                                    }else
                                    {
                                        $final_output['statusText'] = 'failed';
                                        $final_output['message'] = 'Something went wrong! please try again later.';    
                                        $final_output['status'] = 500;       
                                    }
                                }else
                                {
                                    $final_output['statusText'] = 'failed';
                                    $final_output['message'] = 'Something went wrong! please try again later.';    
                                    $final_output['status'] = 500;
                                }
                            }else
                            {
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'Currently this course not available.';    
                                $final_output['status'] = 200;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }
    //create mock test

    function submit_full_test_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {   
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output = $test_result_detail = array();
                    if(!empty($json_array))
                    {
                        if(!empty($json_array->que))
                        {
                            $this->db->join("course",'course.id=musp.course_id');
                            $testcourse = $this->db->select('course.id,course.total_course_time as test_time,musp.id as testid')->get_where("musp",array('musp.token'=>$json_array->reference_t_id,'musp.status'=>1))->row();
                            if(!empty($testcourse))
                            {
                                $checktestsubmit = $this->db->select('id')->get_where("submit_test",array('user_id'=>$tokencheck['data']->id,'test_id'=>$testcourse->testid))->row();
                                if(empty($checktestsubmit))
                                {
                                    $quecount = count($json_array->que);
                                    //$positivemark = $testcourse->sectional_positive_mark;
                                    //$nagativemark = $testcourse->sectional_negative_mark;
                                    $test_total_time = $testcourse->test_time;
                                    $total_time_taken_in_all_sub = $total_score =0;
                                    for ($i=0; $i < $quecount; $i++) {
                                        $total_subject_score = $json_array->que[$i]->subject_total_mark;
                                        $correctmark = $worngmark = $partialgmark = $positivemark =0;
                                        $attempt_quearr = $not_attempt_quearr = $attempt_ansarr = $correctans = $wrongans = $partans = array();
                                        $quest_count = count($json_array->que[$i]->question);
                                        $questions= $json_array->que[$i]->question;
                                        $total_takentime = 0;
                                        for ($j=0; $j < $quest_count; $j++) { 
                                            if($questions[$j]->status==1)
                                            {
                                                $attempt_quearr[] = $questions[$j]->id;
                                                $attempt_ansarr[] = $questions[$j]->response_id;
                                                $total_takentime += $questions[$j]->taken_time;
                                            }else
                                            {
                                                $not_attempt_quearr[] = $questions[$j]->id;
                                                $total_takentime += $questions[$j]->taken_time;
                                            }
                                        }
                                        $correct_ans_cnt = $wrong_ans_cnt = array();
                                        if(!empty($attempt_quearr))
                                        {
                                            $implodearr= implode(',', $attempt_quearr);
                                            $quecountt = count($attempt_quearr);
                                           // print_r($attempt_ansarr);exit;
                                            $que_ans = $this->db->query("SELECT id,question_type,answer,marking_scheme FROM questions WHERE id IN ($implodearr)")->result();
                                            if(!empty($que_ans))
                                            {
                                                
                                                $anscount = count($que_ans);   
                                                for ($k=0; $k < $anscount; $k++) { 
                                                   
                                                    for ($l=0; $l < $quecountt; $l++) { 
                                                        if($que_ans[$k]->id==$attempt_quearr[$l])
                                                        {
                                                            if($que_ans[$k]->question_type==5)
                                                            {
                                                                $fid = explode(',', $que_ans[$k]->answer);
                                                                $secid = explode(',', $attempt_ansarr[$l]);
                                                                if(count($secid) > count($fid) )
                                                                {
                                                                    $answerr = 'wrong';
                                                                }elseif(count($secid) == count($fid))
                                                                {
                                                                    $result = array_merge(array_diff($fid, $secid), array_diff($secid, $fid));
                                                                    if(!empty($result))
                                                                    {
                                                                        $answerr = 'wrong';
                                                                    }else
                                                                    {
                                                                        $answerr = 'correct';
                                                                    }
                                                                }else
                                                                {
                                                                    $answerr = 'wrong';
                                                                    $parcial_correct =0;
                                                                    for ($m=0; $m < count($fid) ; $m++) { 
                                                                        if(isset($secid[$m]))
                                                                        {
                                                                            if($fid[$m] == $secid[$m])
                                                                            {
                                                                                $answerr = 'part';
                                                                                $parcial_correct += 1;
                                                                            }  
                                                                        }
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                if($que_ans[$k]->answer == $attempt_ansarr[$l])
                                                                {
                                                                    $answerr = 'correct';
                                                                }else
                                                                {
                                                                    $answerr = 'wrong';
                                                                }
                                                            }
                                                            $getmarkingscheme = $this->common_model->common_getRow("marking",array('id'=>$que_ans[$k]->marking_scheme,'status'=>1));
                                                            if(!empty($getmarkingscheme))
                                                            {
                                                                if($answerr=='correct')
                                                                {
                                                                    $correctmark += $getmarkingscheme->positive_mark;
                                                                    $correctans[] = $attempt_ansarr[$l];
                                                                }elseif($answerr=='wrong')
                                                                {
                                                                    $worngmark += $getmarkingscheme->negative_mark;
                                                                    $wrongans[] = $attempt_ansarr[$l];
                                                                }else
                                                                {
                                                                    $partialgmark += $getmarkingscheme->partial_mark * $parcial_correct;
                                                                    $partans[] = $attempt_ansarr[$l];
                                                                } 
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        $total_question = $quest_count;
                                        $total_attempt_question = count($attempt_quearr);
                                        $total_correct_answer = count($correctans);
                                        $total_wrong_answer = count($wrongans);
                                        $total_partial_answer = count($partans); 
                                        $total_notvisited = count($not_attempt_quearr);
                                        $total_time_taken = gmdate("i:s", $total_takentime);
                                        if(count($attempt_quearr)>0)
                                        {
                                            $accuracy = $total_correct_answer * 100 / $total_attempt_question;
                                        }else
                                        {
                                            $accuracy = 0;
                                        }
                                        //$positivemarkss = $positivemark*$total_correct_answer;
                                        //$nagativemarkss = $nagativemark*$total_wrong_answer;
                                        $total_get_score_subj = $correctmark+$partialgmark - $worngmark;
                                        $total_score += $total_get_score_subj; 
                                        
                                        $total_time_taken_in_all_sub += $total_takentime;
                                        $test_result_detail[] = array('subject_name'=>$json_array->que[$i]->subject_name,'total_question'=>$quest_count,'total_attempt_question'=>$total_attempt_question,'total_correct_answer'=>$total_correct_answer,'total_wrong_answer'=>$total_wrong_answer,'total_partial_answer'=>$total_partial_answer,'not_visited'=>$total_notvisited,'total_time_taken'=>$total_time_taken,'accuracy'=>round($accuracy,2),'total_score'=>$total_get_score_subj,'total_subject_score'=>$total_subject_score); 
                                       // echo $accuracy;
                                       //  echo '<pre>';
                                        // echo $correctmark;
                                        // echo '<pre>';
                                        // echo $partialgmark;
                                        // echo '<pre>';
                                        // echo $worngmark;
                                        // echo '<pre>';
                                        // echo $total_attempt_question;
                                    }
                                    if(!empty($test_result_detail))
                                    {
                                        $insert = $this->common_model->common_insert("submit_test",array('user_id'=>$tokencheck['data']->id,'test_type'=>1,'test_token'=>$json_array->reference_t_id,'taken_time'=>gmdate("i:s", $total_time_taken_in_all_sub),'test_total_time'=>$test_total_time,'response'=>json_encode($json_array->que),'test_id'=>$testcourse->testid,'score'=>$total_score,'test_detail'=>json_encode($test_result_detail),'create_at'=>date('Y-m-d H:i:s')));
                                        if($insert!=false)
                                        {
                                            $final_output['statusText'] = 'success';
                                            $final_output['message'] = 'Full test successfully submited.';    
                                            $final_output['status'] = 200;   
                                        }else
                                        {
                                            $final_output['statusText'] = 'failed';
                                            $final_output['message'] = 'Something went wrong! please try again later.';    
                                            $final_output['status'] = 500;       
                                        }
                                    }
                                }else
                                {
                                    $final_output['statusText'] = 'failed';
                                    $final_output['message'] = 'Test already done.';    
                                    $final_output['status'] = 200;       
                                }
                            }else
                            {
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'Test not found.';    
                                $final_output['status'] = 200;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        http_response_code($final_output['status']);
        echo json_encode($final_output);
    }
    //submit full test

    function upload_image_file_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {   
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $type = $this->uri->segment(3);
                    $user_id = $tokencheck['data']->id;
                    $img57 = $img48 = '';
              		if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
	        		{  
			            	$files = $_FILES;	
			        	    //$filesCount1 = count($_FILES['image']['name']);
	        	            $_FILES['image']['name'] =  $files['image']['name'];
	        	            $_FILES['image']['type'] =   $files['image']['type'];
			                $_FILES['image']['tmp_name'] =  $files['image']['tmp_name'];
			                $_FILES['image']['error'] =  $files['image']['error'];
			                $_FILES['image']['size'] =  $files['image']['size'];

			                $subFileName = explode('.',$_FILES['image']['name']);
	                        $ExtFileName = end($subFileName);
	                        
			                if($type==1)
	        	            {
	        	             	$uploadPath = 'uploads/user_image/';
	        	            	$config['width'] = 150;
	        	            	$config['height'] = 150;
	        	            	$config['overwrite'] = TRUE;
	        	            	$keyname = 'user_image';
	        	            	$table_name= 'user';
	        	            	$img = 'user_image_'.$user_id.'_'.militime.'.'.$ExtFileName;
	        	            }elseif($type==2)
	        	            {    
                                $course_id = $this->uri->segment(4);
                                $content_title = urldecode($this->uri->segment(5));
                                
	        	             	$uploadPath = 'uploads/content_file/';
	        	             	$config['max_size'] = 2048;
	        	             	$keyname = 'content';
	        	             	$data['name'] = $content_title;
	        	             	$data['course_id'] = $course_id;
	        	             	$data['create_at'] = date('Y-m-d H:i:s');
	        	             	$table_name= 'coaching_content';
	        	            	$img = 'content_'.$user_id.'_'.militime.'.'.$ExtFileName;
	        	             }else
	        	             {
	        	             	$uploadPath = 'uploads/brochure/';
	        	             	$config['max_size'] = 2048;
	        	             	$config['overwrite'] = TRUE;
	        	             	$keyname = 'brochure_file';
	        	             	$table_name= 'user';
	        	            	$img = 'user_brochure_'.$user_id.'.'.$ExtFileName;
	        	            }
	        	            $config['upload_path'] = $uploadPath;
	                        $config['allowed_types'] = '*';
	                        
							$_FILES['image']['name'] = $img;
                  	    	$config['file_name'] = $_FILES['image']['name'];

                           	$fileName = $config['file_name'];
                            	//$fileName11[] = $config['file_name'];
                       		$this->load->library('upload', $config);
                       		$this->upload->initialize($config);
                       		//print_r($this->upload->do_upload('image'));exit;
                   			if($this->upload->do_upload('image'))
                       		{
                          	 	$this->load->library('image_lib');	
                        	 	$fileData = $this->upload->data();
                              	
                              	if($type==1)
                              	{
	                              	//your desired config for the resize() function
								    $config = array(
								    'source_image'      => $fileData['full_path'], //path to the uploaded image
								    'new_image'         => 'uploads/user_image/thumb_57/'.$fileData['file_name'], //path to
								    'maintain_ratio'    => true,
								    'width'             => 57,
								    'height'            => 57
								    );
									 //$this->load->library('image_lib',$config);	
								     $this->image_lib->initialize($config);
								     $this->image_lib->resize();
 									 // if ( ! $this->image_lib->resize()){
									 //  echo $this->image_lib->display_errors(); exit;
									 // }
								 	//print_r($config);exit;

								    $config = array(
								    'source_image'      => $fileData['full_path'],
								    'new_image'         => 'uploads/user_image/thumb_48/'.$fileData['file_name'],
								    'maintain_ratio'    => true,
								    'width'             => 48,
								    'height'            => 48
								    );
								    //here is the second thumbnail, notice the call for the initialize() function again
								    $this->image_lib->initialize($config);
								    $this->image_lib->resize();
									$img57 = base_url().'uploads/user_image/thumb_57/'.$fileData['file_name'];
									$img48 = base_url().'uploads/user_image/thumb_48/'.$fileData['file_name'];

								}
                              	$data[$keyname] = $fileData['file_name'];
    	        				if($type==1 || $type==3)
    	        				{
									$insertt = $this->common_model->updateData($table_name,$data,array('id'=>$user_id));
    	        				}else
    	        				{
    	        					$data['coaching_id'] = $user_id;
    	        					$insertt = $this->common_model->common_insert($table_name,$data);
    	        				}
    	        				if($insertt!=false)
    	        				{
    	        					if($type==1){
                                        //$arr = array('image_url'=>$fileData['full_path'],'image_57'=>$img57,'image_48'=>$img48);
                                        unlink('uploads/user_image/thumb_57/'.$tokencheck['data']->user_image);
                                        unlink('uploads/user_image/thumb_48/'.$tokencheck['data']->user_image);
                                        unlink('uploads/user_image/'.$tokencheck['data']->user_image);
                                        $arr = $fileData['file_name'];
                                    }else
                                    {
                                        $arr= '';
                                    }
	        					    $seltoken = $this->db->query("SELECT id,device_token FROM user WHERE admin_status=1 AND FIND_IN_SET($user_id,follow_coaching)")->result_array();
                                    if(!empty($seltoken))
                                    {
                                        $gcmRegIds = array();
                                        $i = 0;
                                        foreach ($seltoken as $query_row) 
                                        {
                                            $i++;
                                            if($query_row['device_token'] == '')
                                            {
                                                $user[] = $query_row['id'];
                                            }else
                                            {
                                                $user[] = $query_row['id'];
                                                $gcmRegIds[floor($i/1000)][] = $query_row['device_token'];
                                                //$insert_data[] = '("","'.$aa['data']->user_id.'","'.$query_row['user_id'].'",6,0,0,"'.datetime.'","","")';
                                            }
                                        }
                                        if(isset($gcmRegIds)) 
                                        {
                                            $msg = "Content upload by ".$tokencheck['data']->name."";
                                            $arrr = array('title'=>'New content uploaded','msg'=>$msg,'type'=>1);
                                            $message = $arrr;
                                            $pushStatus = array();
                                            foreach($gcmRegIds as $val) $pushStatus[] = $this->common_model->sendNotification($val, $message);
                                        }
                                        if(!empty($user))
                                        {
                                            $notification_data = implode(',',$user);
                                            $insertt = $this->db->query("INSERT INTO notification (sender_id,receiver_id,id,type,msg,create_date) VALUES ('$user_id','$notification_data','',1,'$msg','".date('Y-m-d H:i:s')."')");
                                        }               
                                    }
                                    $final_output['statusText'] = 'success';
				                    $final_output['message'] = 'Successfully uploaded';
				                    $final_output['status'] = 200;
				                    $final_output['data'] = $arr;
    	        				}else
    	        				{
    	        					$final_output['statusText'] = 'failed';
				                    $final_output['message'] = 'Something went wrong! please try again later.';
				                    $final_output['status'] = 200;
    	        				}
                        	}
                        	else
                        	{
                        		
				 				$final_output['statusText'] = 'failed';
			                    $final_output['message'] = $this->upload->display_errors();
			                    $final_output['status'] = 200;	
			 	 						
                        	}
	              	}else
	              	{
	              		$final_output['statusText'] = 'failed';
	                    $final_output['message'] = 'File required.';
	                    $final_output['status'] = 200;
	              	}
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        http_response_code($final_output['status']);
        echo json_encode($final_output);
    }
    
    function delete_coaching_content_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->id))
                        {
                            $userid = $tokencheck['data']->id;
                            $checkname = $this->db->select('id')->get_where("coaching_content",array('coaching_id'=>$userid,'id'=>$json_array->id))->row();
                            if(!empty($checkname))
                            {   
                                $insert = $this->common_model->deleteData("coaching_content",array('id'=>$json_array->id));
                                if($insert!=false)
                                {   
                                    $final_output['statusText'] = 'success';
                                    $final_output['message'] = 'Content successfully deleted.';
                                    $final_output['status'] = 200;   
                                }else
                                {
                                    $final_output['statusText'] = 'failed';
                                    $final_output['message'] = 'Something went wrong! please try again later.';    
                                    $final_output['status'] = 500;       
                                }
                            }else
                            {
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'Content not found.';    
                                $final_output['status'] = 200;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }
    //delete coaching content 27-3-18


function download_content_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->filename))
                        {
                            
                            $path = $json_array->filename;
                            $file_type = $json_array->type;
                            $full_path = base_url().'uploads/'.$file_type.'/'.$path;
                            $this->load->helper('download');

                            // make sure it's a file before doing anything!
                              if(is_file('uploads/'.$file_type.'/'.$path))
                              {

                                header('Content-Type: application/pdf');
                                header("Content-Disposition: attachment; filename='http://13.127.94.52:8282/witcircle_admin/uploads/brochure/user_brochure_10.pdf");

                                $file = fopen('http://13.127.94.52:8282/witcircle_admin/uploads/brochure/user_brochure_10.pdf',"r");
                                
                                echo json_encode($file);
                                    exit;                                
                                // $pth    =   file_get_contents(base_url()."uploads/brochure/user_brochure_10.pdf");
                                // $nme    =   "user_brochure_10.pdf";
                                // echo force_download($nme, $pth); 
                                // exit(0);
                   
                    
                            }else{
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'file not found';    
                                $final_output['status'] = 500; 
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }

    function card_date_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->card_section))
                        {
                            $course_id = $json_array->course_id;
                            $subject_id = $json_array->subject_id;
                            $chapter_id = $json_array->chapter_id;
                            $card_section = $json_array->card_section;
                            $data =array();
                            $this->db->group_by('update_at');
                            $this->db->order_by('update_at','DESC');
                            $selectcard = $this->db->select('date')->get_where('in_short',array('card_section'=>$card_section))->result();
                        
                            if(!empty($selectcard))
                            {
                                $data = $selectcard;
                                $final_output['statusText'] = 'success';
                                $final_output['message'] = 'Card dates';    
                                $final_output['status'] = 200;
                                $final_output['data'] = $data;
                            }else
                            {
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'Cards not available.';    
                                $final_output['status'] = 200;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }
    //10-04-18
    function get_card_by_date_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $json1 = file_get_contents('php://input');
                    $json_array = json_decode($json1);
                    $final_output =array();
                    if($json_array)
                    {
                        if(!empty($json_array->card_section))
                        {
                            $course_id = $json_array->course_id;
                            $subject_id = $json_array->subject_id;
                            $chapter_id = $json_array->chapter_id;
                            $card_section = $json_array->card_section;
                            $card_date = $json_array->card_date;
                            $user_id = $tokencheck['data']->id;
                            if($card_section == 2)
                            {
                                $where = array('in_short.card_section'=>$card_section,'in_short.course_id'=>$course_id,'in_short.subject_id'=>$subject_id,'in_short.chapter_id'=>$chapter_id,'date'=>$card_date);
                            }else
                            {
                                $where = array('in_short.card_section'=>$card_section,'in_short.date'=>$card_date);
                            }
                            $arr =array();
                            $this->db->join("bookmark_card","in_short.id=bookmark_card.card_id AND bookmark_card.user_id = '$user_id'","left");
                            $this->db->order_by('in_short.id','DESC');
                            $selectcard = $this->db->select('in_short.id,in_short.card_section,in_short.course_id,in_short.subject_id,in_short.chapter_id,in_short.image,
										in_short.heading,in_short.text,in_short.short_type,in_short.question,in_short.date,in_short.answer,in_short.option_1,in_short.option_2,in_short.option_3,in_short.option_4,COALESCE(bookmark_card.status,0) as is_bookmark')->get_where('in_short',$where)->result();
                            if(!empty($selectcard))
                            {
                                foreach ($selectcard as $key) {

                                	if($key->image!='')
                                	{
                                		$image = base_url().'uploads/in_short/'.$key->image;
                                	}else
                                	{
                                		$image = '';
                                	}
                                	$ques = (object)array(); $op_1 = $op_2 = $op_3 = $op_4 = 0;
                                	if(!empty($key->question))
                                	{
                                	  	${"op_".$key->answer} = 1;

                                		$option[] = array('option_id'=>1,'option_text'=>$key->option_1,'options_status'=>$op_1);
                                		$option[] = array('option_id'=>2,'option_text'=>$key->option_2,'options_status'=>$op_2);
                                		$option[] = array('option_id'=>3,'option_text'=>$key->option_3,'options_status'=>$op_3);
                                		$option[] = array('option_id'=>4,'option_text'=>$key->option_4,'options_status'=>$op_4);

	                                	$ques = array(
	                                			'question'=>$key->question,
	                                			'options'=>$option
	                                		);
                                	}
                                	$arr[] = array(
                                			'id'=>$key->id,
                                			'card_section'=>$key->card_section,
                                			'course_id'=>$key->course_id,
                                			'subject_id'=>$key->subject_id,
                                			'chapter_id'=>$key->chapter_id,
                                			'image'=>$image,
                                			'heading'=>$key->heading,
                                			'text'=>$key->text,
                                			'card_type'=>$key->short_type,
                                			'question'=>$ques,
                                			'date'=>$key->date,
                                			'is_bookmark'=>$key->is_bookmark
                                			);
                                }  
                                $final_output['statusText'] = 'success';
                                $final_output['message'] = 'Card detail';    
                                $final_output['status'] = 200;
                                $final_output['data'] = $arr;
                            }else
                            {
                                $final_output['statusText'] = 'failed';
                                $final_output['message'] = 'Cards not available.';    
                                $final_output['status'] = 200;
                            }
                        }else
                        {
                            $final_output['statusText'] = 'failed';
                            $final_output['message'] = 'Invalid request parameter';    
                            $final_output['status'] = 412;
                        }
                    }
                    else{
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'No request parameter';
                        $final_output['status'] = 412;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }
    //10-04-18
    function bookmark_card_list_post()
    {
        $header  = apache_request_headers();
        if(!empty($header))
        {
            $tokencheck = $this->ChechAuth($header['secret_key']);

            if($tokencheck['status'] == 'true')
            {
                if($tokencheck['data']->admin_status==1)
                {
                    $user_id = $tokencheck['data']->id;
                    // if($card_section == 2)
                    // {
                    //     $where = array('in_short.card_section'=>$card_section,'in_short.course_id'=>$course_id,'in_short.subject_id'=>$subject_id,'in_short.chapter_id'=>$chapter_id,'date'=>$card_date);
                    // }else
                    // {
                    //     $where = array('in_short.card_section'=>$card_section,'in_short.date'=>$card_date);
                    // }
                    $arr =array();
                    $this->db->join("bookmark_card","in_short.id=bookmark_card.card_id AND bookmark_card.user_id = '$user_id'","left");
                    $this->db->order_by('bookmark_card.bookmark_id','DESC');
                    $selectcard = $this->db->select('in_short.id,in_short.card_section,in_short.course_id,in_short.subject_id,in_short.chapter_id,in_short.image,
                                in_short.heading,in_short.text,in_short.short_type,in_short.question,in_short.date,in_short.answer,in_short.option_1,in_short.option_2,in_short.option_3,in_short.option_4')->get_where('in_short',array('bookmark_card.user_id'=>$user_id,'bookmark_card.status'=>1))->result();
                    if(!empty($selectcard))
                    {
                        foreach ($selectcard as $key) {

                            if($key->image!='')
                            {
                                $image = base_url().'uploads/in_short/'.$key->image;
                            }else
                            {
                                $image = '';
                            }
                            $ques = (object)array(); $op_1 = $op_2 = $op_3 = $op_4 = 0;
                            if(!empty($key->question))
                            {
                                ${"op_".$key->answer} = 1;

                                $option[] = array('option_id'=>1,'option_text'=>$key->option_1,'options_status'=>$op_1);
                                $option[] = array('option_id'=>2,'option_text'=>$key->option_2,'options_status'=>$op_2);
                                $option[] = array('option_id'=>3,'option_text'=>$key->option_3,'options_status'=>$op_3);
                                $option[] = array('option_id'=>4,'option_text'=>$key->option_4,'options_status'=>$op_4);

                                $ques = array(
                                        'question'=>$key->question,
                                        'options'=>$option
                                    );
                            }
                            $arr[] = array(
                                    'id'=>$key->id,
                                    'card_section'=>$key->card_section,
                                    'course_id'=>$key->course_id,
                                    'subject_id'=>$key->subject_id,
                                    'chapter_id'=>$key->chapter_id,
                                    'image'=>$image,
                                    'heading'=>$key->heading,
                                    'text'=>$key->text,
                                    'card_type'=>$key->short_type,
                                    'question'=>$ques,
                                    'date'=>$key->date
                                    );
                        }  
                        $final_output['statusText'] = 'success';
                        $final_output['message'] = 'Bookmark list detail';    
                        $final_output['status'] = 200;
                        $final_output['data'] = $arr;
                    }else
                    {
                        $final_output['statusText'] = 'failed';
                        $final_output['message'] = 'Empty bookmark list.';    
                        $final_output['status'] = 200;
                    }
                }else
                {
                    $final_output['statusText'] = 'failed';
                    $final_output['message'] = 'Your account temporary suspended for security reason.';
                    $final_output['status'] = 200;
                }
            }else
            {
                $final_output['statusText'] = 'false';
                $final_output['message'] = 'Invalid token';
                $final_output['status'] = 401;
            }
        }else
        {
            $final_output['statusText'] = 'false';
            $final_output['message'] = 'Unauthorized access';
            $final_output['status'] = 401;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }
    //10-04-18
    function ChechAuth($token)
    {
        //$token=str_replace("Bearer","",$token);
        $auth = $this->common_model->getDataField('id,user_status,admin_status,name,user_subscription_status,user_subscription_valid_till,wallet,user_type,user_image','user',array('user_token'=>$token));
        if(!empty($auth))
        {
            $abc['status'] = "true";
            $abc['data'] = $auth[0];
            return $abc;
        }else
        {
            $abc['status'] = "false";
            return $abc;
        }
    }
}
