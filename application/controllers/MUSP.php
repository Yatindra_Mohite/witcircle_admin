<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MUSP extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}

		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				//echo $result;exit;
				redirect(base_url($result));
			}
		}
		//$this->session->userdata->userdata('subject_id_sess');
	    //$this->session->userdata->unset_userdata('question_type_sess');
	    //$this->session->userdata->unset_userdata('marking_scheme_sess');
	    //unset($this->session->userdata->userdata('subject_id_sess'));		
	}

	 public function isJSON($string){
		   return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
		} 

	public function index(){

        $currentdate = date("Y-m-d H:i:s");
        $subjects = [];
		if(isset($_POST['submit']))
		{   
		    $course_id =$this->input->post ('course_id');
		    $section_name = $this->input->post('section_name');
		    $sectional_times = $this->input->post('sectional_times');
		    $subject_id =$this->input->post ('subject_id');
            $check_time = 0;
		    // print_r($sectional_times);
		    // exit;

		    for($i=0; $i<count($section_name); $i++){
		    	$data = [];
		    	$data["section_name"] = $section_name[$i];
		    	$data["subject_id"] = $subject_id[$i];
		    	$data["sectional_times"] = $sectional_times[$i];
		    	if (!empty($sectional_times[$i])) {
		    		$check_time = 1;
		    	}
		    	$subjects[] = $data;
		    }

		    $total = $check_time;

		    if ($total == 0) {
		      $is_section_time = 0;
		    }else{
		      $is_section_time =1;
		    }	

		    //print_r($subjects);
		    $subjects = json_encode($subjects);
		  	//print_r($subjects);
		  	//exit; 		

		    
		    $text_desc =$this->input->post ('text_desc');
		    $a = implode(",",$subject_id);
		    $test_name =$this->input->post ('test_name');
		    $no_of_question =$this->input->post ('no_of_question');
		    $milisecond = round(microtime(true)*1000);
		    $token = md5($milisecond).$milisecond;
		    //exit;
				 
			$insertid = $this->common_model->common_insert('musp',array('added_by'=>'admin','added_id'=>$this->session->userdata('admin_id'),'course_id'=>$course_id,'subject_id'=>$subjects,'test_name'=>$test_name,'test_desc'=>$text_desc,'is_sectional_time'=>$is_section_time,'no_of_question'=>$no_of_question,'token'=>$token,'create_at'=>$currentdate));
			 
			if($insertid == '' && $insertid == '0')
			{
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('MUSP'));
			}
			else
			{
               $this->db->query("UPDATE global_counters SET full_test_count = full_test_count + 1 WHERE id = 1");

               $this->session->set_flashdata('success' ,'MUSP added successfully');	
			   redirect(base_url('MUSP/musp_list'));
			}	
		}


      $this->load->view('admin/musp/add_musp');
	 }

	public function get_subject()
	{
		//echo "dfdf";exit;
	  $course_id = $this->input->post('course_id');

	  $sub = $this->db->query("SELECT * FROM `course_subject` WHERE FIND_IN_SET($course_id,course_id)")->result();
	 
 
	  if (empty($sub)) {?>
		  <option disabled="" >No subject added for this course</option>
		   <?php }else{?>
		   <label class="control-label col-md-3">Select Subject<span class="required">*</span></label>
                    
	      <option disabled="" >Select Subject</option>
	  <?php }
	  
	  foreach ($sub as $key) 
	  { 
	  ?>
	    <option value="<?php echo $key->id; ?>"><?php echo $key->subject_name;?></option>
	   <?php
	  }
	}

	public function get_subject_new()
	{
	  $course_id = $this->input->post('course_id');

	 // $sub = $sub = $this->db->query("SELECT * FROM `course_subject` WHERE `course_id` LIKE '%-$course_id,%'")->result();
	  $sub = $this->db->query("SELECT * FROM `course_subject` WHERE FIND_IN_SET($course_id,course_id)")->result();
 
	  if (empty($sub)) {?><option value="" >No subject added for this course</option>
		   <?php }else{?><option value="" >Select Subject</option>
	  <?php }
	  
	  foreach ($sub as $key) 
	  { 
	  ?><option value="<?php echo $key->id; ?>"><?php echo $key->subject_name;?></option>
	   <?php
	  }
	}
    
    public function musp_list(){

		$data['musp_list'] = $this->common_model->getData('musp',array('added_by'=>'admin','type'=>''),'id','DESC');
				 
       $this->load->view('admin/musp/musp_list', $data);
	}

	public function change_status(){
		//echo "hihih";
		//exit;
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
        
        $update = $this->common_model->updateData("musp",array('status'=>$status),array('id'=>$user_id));
        if($update)
	    {
	        echo 1000;exit; 
	    }
    }

    public function generate_musp_test(){
		
        $id = $this->input->post('id');
        
    	$finalarr = $quearr = array(); $impquest = '';
    	$musp_list = $this->db->select('subject_id,no_of_question')->get_where('musp',array('id'=>$id))->row();
    	
    	if(!empty($musp_list))
      	{
        	$sub_pattern = json_decode($musp_list->subject_id);
          	for ($i=0; $i < count($sub_pattern); $i++) { 
      
            	$quest_list = $this->db->query("SELECT GROUP_CONCAT(questions.id) as question,GROUP_CONCAT(questions.marking_scheme) as markingsch,GROUP_CONCAT(questions.question_type) as qtype,questions.subject_id,course_subject.title as subject_name from questions INNER JOIN course_subject ON questions.subject_id = course_subject.id WHERE questions.test_id = '$id' AND questions.subject_id = '".$sub_pattern[$i]->subject_id."'")->row();
            	
            	if(!empty($quest_list))
            	{ 
	              	if(isset($quest_list->question) && isset($quest_list->markingsch) )
	              	{
		              	$ques = $quescomp = $quesother = array();
		                $question = explode(',',$quest_list->question);
		                $questiontype = explode(',',$quest_list->qtype);
		                $markschesum = array_count_values(explode(',', $quest_list->markingsch));
		                $implodnum = implode(',', array_keys($markschesum));
		                $totalsubjmark = 0;
	                	$markingsel = $this->db->query("SELECT id,positive_mark FROM marking WHERE id IN ($implodnum)")->result();
	                	if(!empty($markingsel))
	                	{
	                		foreach ($markingsel as $value) {
	                			$totalsubjmark += $value->positive_mark*$markschesum[$value->id];
	                		}
	                	}
	                    for ($j=0; $j < count($question); $j++) { 
			                if($questiontype[$j] == 10)
			                {
			                    $quescomp[] = $question[$j];
			                }else
			                {
			                    $quesother[] = $question[$j];
			                }
		                }
	                	$ques = array_merge($quescomp,$quesother);
	                	$quearr[] = array('subject_id'=>$sub_pattern[$i]->subject_id,'subject_name'=>$quest_list->subject_name,'subject_total_mark'=>$totalsubjmark,'question'=>$ques,'sectional_time'=>$sub_pattern[$i]->sectional_times);
	                	$questionarr[] = implode(',', $ques); 
	              	}else
	              	{
	              		echo 2001;exit;
	              	}
                }else
                {
                	echo 2001;exit;	
                }
      		}
            if(!empty($questionarr))
            {
                $impquest = implode(',', $questionarr);
              	$quescount = substr_count($impquest,",")+1;
	            if($quescount == $musp_list->no_of_question)
	            {
	                $finalarr = array('que'=>$quearr);
	                $this->common_model->updateData("musp",array('questions_id'=>$impquest,'subject_pattern'=>json_encode($finalarr)),array('id'=>$id));
      				echo 1000;exit;
	            }else
	            {
	                echo 5001;exit;
	            }
        	}else
        	{
        		echo 4001; exit;
        	}
    	}else{
        	echo 3001; exit;
    	} 
    }

    public function edit($id = false){
       
     $id = $this->common_model->id_decrypt($id);

		if(isset($_POST['submit']))
		{   
		    
		    $text_desc =$this->input->post ('text_desc');
		    $test_name =$this->input->post ('test_name');
		    $no_of_question =$this->input->post ('no_of_question');
		    //exit;
			 
			$updateid = $this->common_model->updateData('musp',array('test_name'=>$test_name,'test_desc'=>$text_desc,'no_of_question'=>$no_of_question),array('id'=>$id));
			 
			if($updateid == '' || $updateid == '0') {
			 	
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('MUSP'));
			}
			else
			{
            $this->session->set_flashdata('success' ,'MUSP added successfully');	
			   redirect(base_url('MUSP/musp_list'));
			}	
		}
    $data['musp_edit'] = $this->common_model->common_getRow('musp',array('id'=>$id));

    $this->load->view('admin/musp/edit_musp', $data);
	 }

   public function add_question($id = false)
   {  
    $id = $this->common_model->id_decrypt($id);

    $data['que_active'] = $this->common_model->common_getRow('musp',array('id'=>$id));
    
    $data1 = $this->db->query("SELECT COUNT(test_id) as que_count FROM questions WHERE test_id = $id")->result();
    //print_r($data1);
    //exit;

    $data['question_type'] = $this->common_model->getData('question_type',array(),'id','DESC');

    $data['marking'] = $this->common_model->getData('marking',array('status'=>1),'id','DESC');  

        if(isset($_POST['submit']))
		{   
          if ($data['que_active']->no_of_question >  $data1[0]->que_count)
          {

			$question_id = $this->input->post ('question_type');

			if ($question_id != 10)// only for comprehensive
			{

				if ($question_id == 2) {
			        $ids = $this->input->post ('answer_id2');
				} 
				 
				elseif ($question_id == 4) {
				   $ids = $this->input->post ('answer_id1');
				} 
				 
				elseif ($question_id == 5) {
				   $ids = implode(",", $this->input->post ('answer_id3'));
				}

				$option = array();        
	            $j = 0;
	            for ($i = 1; $i < 7; $i++) { 
	                $a=str_replace('"', "'",$this->input->post('text_desc'.$i));
	                if ($a!='') {
	                    $j++;
	                   $data1["id"] = $j;
	                    $data1["option"] = $a;
	                    $option[] = $data1;    
	                    $data1= [];
	               }
	            } 
	            $op = stripslashes(json_encode($option));

	            if($this->isJSON($op)){
				 //echo "It's JSON";
				}else{
					//echo "ihihihihih";
					$this->session->set_flashdata('failed' ,'Something went wrong in your options please check.');	
			    	redirect(base_url('MUSP/add_question/'.$this->common_model->id_encrypt($id)));
				}

			    $u_data = array(
			    'guide_line'=> $this->input->post('guide_line'),
			    'added_by' => 'admin',
			    'added_id' => $this->session->userdata('admin_id'),
			    'test_id' => $id,
			    'subject_id' => $this->input->post ('subject_id'),
			    'question_type' => $question_id,
			    'marking_scheme' => $this->input->post ('marking_scheme'),
			    'chapter_id' => '0',
			    'question' => $this->input->post('ques_desc'),
			    'options' => $op,
			    'answer'=> $ids,
			    'create_at'=>date("Y-m-d H:i:s"),
			    );

			    $insertid = $this->common_model->common_insert('questions',$u_data);
				
			}else
			{   
                $u_data = array(
                'question_type' => $question_id,
                'chapter_id'=> '0',
			    'question' => $this->input->post ('ques_desc'),
			    'subject_id' => $this->input->post ('subject_id'),
			    'test_id' => $id,
			    'create_at'=>date("Y-m-d H:i:s"),
			    );
                
                $insertid = $this->common_model->common_insert('questions_comprehensive',$u_data);
			}
			 
			if($insertid == '' || $insertid == '0') {
			 	
			 	$this->session->set_flashdata('error' ,'Something went wrong');	
			    redirect(base_url('MUSP/add_question/'.$this->common_model->id_encrypt($id)));
			}
			else
			{
               $this->session->set_userdata ( array (
               	                'guide_line_sess'=> $this->input->post('guide_line'),
								'subject_id_sess'   => $this->input->post ('subject_id'),
								'question_type_sess'   => $this->input->post ('question_type'),
								'marking_scheme_sess'   => $this->input->post ('marking_scheme'),								
								));

               $this->db->query("UPDATE global_counters SET question_count = question_count + 1 WHERE id = 1");

            $this->session->set_flashdata('success' ,'question added successfully');	
			   redirect(base_url('MUSP/add_question/'.$this->common_model->id_encrypt($id)));
	        }

	     }else
		 {
            $this->session->set_flashdata('error' ,'Your limit is over for this test');	
		    redirect(base_url('MUSP/musp_list'));
		 }    

	   }
	  
	   $this->load->view('admin/musp/add_question',$data);
    }

   public function question_list($id = false)
   {   
   	    $id = $this->common_model->id_decrypt($id);
       //exit;
       $data['que_active'] = $this->common_model->common_getRow('musp',array('id'=>$id));

       $data['que_list'] = $this->db->query(" SELECT * FROM questions WHERE test_id = $id AND added_by = 'admin' AND comprehensive_id = 0 ORDER BY id DESC ")->result();
       //echo " SELECT  DISTINCT questions.subject_id, course_subject.subject_name FROM questions LEFT JOIN course_subject ON questions.subject_id = course_subject.id WHERE questions.test_id = $id AND added_by = 'admin' ";
       //exit;
       $data['sub_list'] = $this->db->query(" SELECT  DISTINCT questions.subject_id, course_subject.subject_name FROM questions LEFT JOIN course_subject ON questions.subject_id = course_subject.id WHERE questions.test_id = '$id' AND added_by = 'admin' ")->result();
       
 
       $this->load->view('admin/musp/question_list', $data);
   }
   
    public function according_to_subject_list($id = false, $subject_id = false)
	{
	    $id = $this->common_model->id_decrypt($id);

	    $data['sub_list'] = $this->db->query(" SELECT  DISTINCT questions.subject_id, course_subject.subject_name FROM questions LEFT JOIN course_subject ON questions.subject_id = course_subject.id WHERE questions.test_id = '$id' AND added_by = 'admin' ")->result();
        
        if($subject_id)
	    {
	      $where = "AND subject_id = $subject_id";
	    }
	    else
	    {
	      $where = '';
	    }
	   
        $data['que_list'] = $this->db->query(" SELECT * FROM questions WHERE test_id = '$id' $where AND added_by = 'admin' ")->result();
	    $this->load->view('admin/musp/question_list', $data);
	}

	public function edit_question($id = false, $question_id  = false)
	{   
		//echo "iam here";
		//exit;
		$id = $this->common_model->id_decrypt($id);

		$question_ids = $this->common_model->id_decrypt($question_id);


		if(isset($_POST['submit']))
		{   
			$question_id = $this->input->post ('question_type');

			if ($question_id == 2) {
			   $ids = $this->input->post ('answer_id2');
			} 
			 
			elseif ($question_id == 4) {
			   $ids = $this->input->post ('answer_id1');
			} 
			 
			elseif ($question_id == 5) {
			   $ids = implode(",", $this->input->post ('answer_id3'));
			}

			$option = array();        
            $j = 0;
            for ($i = 1; $i < 7; $i++) { 
                $a=str_replace('"', "'",$this->input->post('text_desc'.$i));
                if ($a!='') {
                    $j++;
                   $data1["id"] = $j;
                    $data1["option"] = $a;
                    $option[] = $data1;    
                    $data1= [];
               }
            } 
            $op = stripslashes(json_encode($option));

            if($this->isJSON($op)){
				 //echo "It's JSON";
			}else{
				//echo "ihihihihih";
				$this->session->set_flashdata('failed' ,'Something went wrong in your options please check.');	
		    	redirect(base_url('MUSP/edit_question/'.$this->common_model->id_encrypt($id).'/'.$this->common_model->id_encrypt($question_ids)));
			}

		    $u_data = array(
		    	'guide_line'=> $this->input->post('guide_line'),
			    'added_by' => 'admin',
			    'added_id' => $this->session->userdata('admin_id'),
			    'test_id' => $id,
			    'subject_id' => $this->input->post ('subject_id'),
			    'question_type' => $question_id,
			    'marking_scheme' => $this->input->post ('marking_scheme'),
			    'chapter_id' => '0',
			    'question' => $this->input->post('ques_desc'),
			    'options' => $op,
			    'answer'=> $ids,
			    'create_at'=>date("Y-m-d H:i:s"),
		        );

		    $updateid = $this->common_model->updateData('questions',$u_data,array('id'=>$question_ids),array());
			 
			if($updateid == '' || $updateid == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('MUSP/question_list/'.$this->common_model->id_encrypt($id)));
			}
			else
			{
               $this->session->set_flashdata('success' ,'question Update successfully');	
			   redirect(base_url('MUSP/question_list/'.$this->common_model->id_encrypt($id)));
	        }
	   }
		//exit;
		$data['question_type'] = $this->common_model->getData('question_type',array(),'id','DESC');

        $data['marking'] = $this->common_model->getData('marking',array('status'=>1),'id','DESC');    

		$data['question_edit'] = $this->common_model->common_getRow('questions',array('id'=>$question_ids));

		$data['que_active'] = $this->common_model->common_getRow('musp',array('id'=>$data['question_edit']->test_id));
		//print_r($data['que_active']);
		//exit;

		$this->load->view('admin/musp/edit_question',$data);
	}

	public function comprehension_question_list($id = false)
    {   
   	    $id = $this->common_model->id_decrypt($id);
        //exit;
        // $data['que_active'] = $this->common_model->common_getRow('musp',array('id'=>$id));

       $data['que_list'] = $this->db->query(" SELECT * FROM questions_comprehensive WHERE test_id = $id ORDER BY id DESC ")->result();
       //print_r($data['que_list']);
       //exit;
       //echo " SELECT  DISTINCT questions.subject_id, course_subject.subject_name FROM questions LEFT JOIN course_subject ON questions.subject_id = course_subject.id WHERE questions.test_id = $id AND added_by = 'admin' ";
       //exit;
       //$data['sub_list'] = $this->db->query(" SELECT  DISTINCT questions.subject_id, course_subject.subject_name FROM questions LEFT JOIN course_subject ON questions.subject_id = course_subject.id WHERE questions.test_id = '$id' AND added_by = 'admin' ")->result();
       $this->load->view('admin/musp/comprehension_question_list', $data);
    }

   public function edit_comprehension($test_id = false,$com_id = false)
   {
        $id2 = $this->common_model->id_decrypt($test_id);
        $id = $this->common_model->id_decrypt($com_id);
        //echo "hihih";
        //exit; 
        if(isset($_POST['submit']))
		{   
            $sub_id = $this->input->post('subject_id');
            //$guide_line = $this->input->post('guide_line');

			$update = $this->common_model->updateData('questions_comprehensive',array('subject_id'=> $sub_id),array('id' => $id),array());
			//print_r($update);

			//exit; 
			if($update == '' || $update == '0') {
			 	
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('MUSP/comprehension_question_list/'.$this->common_model->id_encrypt($id2)));
			}
			else
			{
               $update = $this->common_model->updateData('questions',array('subject_id'=> $sub_id),array('comprehensive_id' => $id,'chapter_id'=>'0'),array());
               //exit;

               $this->session->set_flashdata('success' ,'Comprehension question update successfully');	
			   redirect(base_url('MUSP/comprehension_question_list/'.$this->common_model->id_encrypt($id2)));
	        }
		    
		}

        $data['que_active'] = $this->common_model->common_getRow('musp',array('id'=>$id2));

        $data['question_edit'] = $this->common_model->common_getRow('questions_comprehensive',array('id'=>$id));
        
        $this->load->view('admin/musp/edit_comprehension', $data);
   }

   public function add_comprehension_question($id = false, $com_id = false)
   {   
   	    $id = $this->common_model->id_decrypt($id);

   	    $com_id = $this->common_model->id_decrypt($com_id);

   	    $data2 = $this->db->query(" SELECT subject_id,question_type FROM questions_comprehensive WHERE id = $com_id ORDER BY id DESC ")->result();
   	    //print_r($data1);
   	    //exit;

   	    if(isset($_POST['submit']))
		{   
			$type = $this->input->post ('type_id');

			if ($type == 1) {
			   $ids = $this->input->post ('answer_id1');
			} 
			 
			elseif ($type == 2) {
			   $ids = implode(",", $this->input->post ('answer_id3'));
			}

			$option = array();        
            $j = 0;
            for ($i = 1; $i < 7; $i++) { 
                $a=str_replace('"', "'",$this->input->post('text_desc'.$i));
                if ($a!='') {
                    $j++;
                   $data1["id"] = $j;
                    $data1["option"] = $a;
                    $option[] = $data1;    
                    $data1= [];
               }
            } 
            $op = stripslashes(json_encode($option));

            if($this->isJSON($op)){
				 //echo "It's JSON";
			}else{
				//echo "ihihihihih";
				$this->session->set_flashdata('failed' ,'Something went wrong in your options please check.');	
		    	redirect(base_url('subadmin/MUSP/add_comprehension_question/'.$this->common_model->id_encrypt($id).'/'.$this->common_model->id_encrypt($com_id)));
			}

		    $u_data = array(
			    'added_by' => 'admin',
			    'added_id' => $this->session->userdata('admin_id'),
			    'test_id' => $id,
			    'subject_id' => $data2[0]->subject_id,
			    'question_type' =>$data2[0]->question_type,
			    'marking_scheme' => $this->input->post ('marking_scheme'),
			    'comprehensive_id'=> $com_id,
			    'comprehensive_q_type'=>$type,
			    'chapter_id' => '0',
			    'question' => $this->input->post('ques_desc'),
			    'options' => $op,
			    'answer'=> $ids,
			    'create_at'=>date("Y-m-d H:i:s"),
		        );

		    $insertid = $this->common_model->common_insert('questions',$u_data);
			 
			if($insertid == '' || $insertid == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('MUSP/comprehension_question_list/'.$this->common_model->id_encrypt($id)));
			}
			else
			{  
               $this->db->query("UPDATE global_counters SET question_count = question_count + 1 WHERE id = 1");

               $this->session->set_flashdata('success' ,'comprehension Update successfully');	
			   redirect(base_url('MUSP/comprehension_question_list/'.$this->common_model->id_encrypt($id)));
	        }
	   }
        

        $data['marking'] = $this->common_model->getData('marking',array('status'=>1),'id','DESC'); 

        
 
       $this->load->view('admin/musp/add_comprehension_question', $data);
   }

   public function comp_question_list($id = false, $com_id = false)
   {   
   	   $id = $this->common_model->id_decrypt($id);
   	   $c_id = $this->common_model->id_decrypt($com_id);

   	   $data['que_list'] = $this->common_model->getData('questions',array('test_id' => $id,'comprehensive_id'=>$c_id),'id','DESC');
   
       $this->load->view('admin/musp/comp_question_list',$data);
   }

   public function edit_comprehension_question($com_id = false, $que_id = false)
   {   
   	    $com_id = $this->common_model->id_decrypt($com_id);

   	    $que_id = $this->common_model->id_decrypt($que_id);
        
        //$data2 = $this->db->query(" SELECT subject_id,question_type FROM questions_comprehensive WHERE id = $com_id ORDER BY id DESC ")->result();
   	    //print_r($data1);
   	    //exit;

   	    if(isset($_POST['submit']))
		{   
			$type = $this->input->post ('type_id');
			$aa = $this->input->post ('get_test');
            $bb = $this->input->post ('get_com_id');


			if ($type == 1) {
			   $ids = $this->input->post ('answer_id1');
			} 
			 
			elseif ($type == 2) {
			   $ids = implode(",", $this->input->post ('answer_id3'));
			}

			$option = array();        
            $j = 0;
            for ($i = 1; $i < 7; $i++) { 
                $a=str_replace('"', "'",$this->input->post('text_desc'.$i));
                if ($a!='') {
                    $j++;
                   $data1["id"] = $j;
                    $data1["option"] = $a;
                    $option[] = $data1;    
                    $data1= [];
               }
            } 
            $op = stripslashes(json_encode($option));

            if($this->isJSON($op)){
				 //echo "It's JSON";
			}else{
				//echo "ihihihihih";
				$this->session->set_flashdata('failed' ,'Something went wrong in your options please check.');	
		    	redirect(base_url('MUSP/edit_comprehension_question/'.$this->common_model->id_encrypt($com_id).'/'.$this->common_model->id_encrypt($que_id)));
			}

		    $u_data = array(
			    //'added_by' => 'admin',
			    //'added_id' => $this->session->userdata('admin_id'),
			    //'test_id' => $id,
			    //'subject_id' => $data2[0]->subject_id,
			    //'question_type' =>$data2[0]->question_type,
			    'marking_scheme' => $this->input->post ('marking_scheme'),
			    //'comprehensive_id'=> $com_id,
			    'comprehensive_q_type'=>$type,
			    'chapter_id' => '0',
			    'question' => $this->input->post('ques_desc'),
			    'options' => $op,
			    'answer'=> $ids,
			    'create_at'=>date("Y-m-d H:i:s"),
		        );

		    $update = $this->common_model->updateData('questions',$u_data,array('id' =>$que_id ),array());
			 
			if($update == '' || $update == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			     redirect(base_url('MUSP/comp_question_list/'.$this->common_model->id_encrypt($aa).'/'.$this->common_model->id_encrypt($bb)));
			}
			else
			{
              
             $this->session->set_flashdata('success' ,'Comprehension question update successfully');	
			   redirect(base_url('MUSP/comp_question_list/'.$this->common_model->id_encrypt($aa).'/'.$this->common_model->id_encrypt($bb)));
	        }
	   } 

	    $data['question_edit'] = $this->common_model->common_getRow('questions',array('id'=>$que_id));

        $data['marking'] = $this->common_model->getData('marking',array('status'=>1),'id','DESC'); 
 
       $this->load->view('admin/musp/edit_comprehension_question',$data);
   }

   public function delete_comp($id = false)
    {   
    	//$data_querry = $this->db->query("SELECT * FROM mock_test where FIND_IN_SET ('".$id."',questions_id) ")->result();
        //exit;
    	//$data_querry = $this->db->query("SELECT * FROM questions WHERE comprehensive_id = $id ")->result();
    	$data_querry = $this->common_model->deleteData("questions",array('comprehensive_id'=>$id));
       // print_r($data_querry);
        //exit;

        // if (count($data_querry) > 0) {
        // 	echo '100';exit; 
        // }else{
            $delete = $this->common_model->deleteData("questions_comprehensive",array('id'=>$id));
            if($delete)
		    {
		         echo '1000';exit; 
		    }            
        // }
    }

     public function delete_comp_que($id = false){
        //echo $id;
        //exit;
     	$delete = $this->common_model->deleteData("questions",array('id'=>$id));
            if($delete)
		    {
		         echo '1000';exit; 
		    } 
     }

    // public function checking(){
         
    //     $data = $this->common_model->getData('questions',array()); 
    //     print_r($data);
    //     exit;
     	 
    // }


}