<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_ws extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
     
			// $currentdate = date("Y-m-d H:i:s");
			// echo $currentdate;
			// exit;
			// define('currentdate', $currentdate);
		
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
		
	}

    public function add_courses()
	{
        if(isset($_POST['submit']))
		{   
		    $course_name = strtolower($this->input->post ('course_name'));
		    $subjective = $this->input->post ('type_id1');
		    $wit_card = $this->input->post ('type_id2');
		    $sub_coin = $this->input->post ('sub_coin');
		    //$witcard_coin = $this->input->post ('witcard_coin');
		    
		    if (empty($subjective)) {
		    	$subjective = '0';
		    	$sub_coin = '0';
	 	    }
		    if (empty($wit_card)) {
		    	$wit_card = '0';
		    	//$witcard_coin = '0';
		    }

            $insertid = $this->common_model->common_insert('subjective_and_cards_course',array('name'=>$course_name,'sub_coin'=>$sub_coin,'subjective_id'=>$subjective,'witcard_id'=>$wit_card));

            if($insertid == '' || $insertid == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('Add_ws/add_courses'));
			}
			else
			{
	            $this->session->set_flashdata('success' ,'Course added successfully');	
			    redirect(base_url('Add_ws/add_courses'));
			}
		}
        $this->load->view('admin/add_courses_for_both/add_courses'); 
	}

    public function courses_list()
	{
        $data['both_course'] = $this->common_model->getData('subjective_and_cards_course',array(),'id','DESC');
        //print_r($data['both_course']);
        //exit;
        $this->load->view('admin/add_courses_for_both/courses_list',$data); 

	}

	public function change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("subjective_and_cards_course",array('status'=>$status),array('id'=>$user_id));

          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit($id = false){
    	$id = $this->common_model->id_decrypt($id);
        
		if(isset($_POST['submit']))
		{   
		    $course_name = strtolower($this->input->post ('course_name'));
		    $subjective = strtolower($this->input->post ('type_id1'));
		    $wit_card = strtolower($this->input->post ('type_id2'));
		    
		    if (empty($subjective)) {
		    	$subjective = '0';
	 	    }
		    if (empty($wit_card)) {
		    	$wit_card = '0';
		    }

            $insertid = $this->common_model->updateData('subjective_and_cards_course',array('name'=>$course_name,'subjective_id'=>$subjective,'witcard_id'=>$wit_card),array('id'=>$id),array());

            if($insertid == '' || $insertid == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('Add_ws/courses_list'));
			}
			else
			{
	            $this->session->set_flashdata('success' ,'Course updated successfully');	
			    redirect(base_url('Add_ws/courses_list'));
			}
		}

       $data['course_edit'] = $this->common_model->common_getRow('subjective_and_cards_course',array('id'=>$id));
      // print_r($data['course_edit']);
       //exit;

       $this->load->view('admin/add_courses_for_both/edit_courses', $data);
    }

}
