<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test_type extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		
	}

	public function index(){
       
        $currentdate = date("Y-m-d H:i:s");

		if(isset($_POST['submit']))
		{   
			     $test_type = strtolower($this->input->post ('test_type'));
				 //exit;
				 $insertid = $this->common_model->common_insert('test_type',array('type_name'=>$test_type,'create_at'=>$currentdate));
				 
				 if($insertid == '' && $insertid == '0') {
				 	
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('test_type'));
				 }
				 else
				 {
                    $this->session->set_flashdata('success' ,'Test added successfully');	
				    redirect(base_url('test_type/test_type_list'));
				 }	
		}
   $this->load->view('admin/test_type/add_test_type');
	}

	public function test_type_list(){
       
		$data['test_type_list'] = $this->common_model->getData('test_type',array(),'id','DESC');
				 
       $this->load->view('admin/test_type/test_type_list', $data);

	}

	public function change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("test_type",array('status'=>$status),array('id'=>$user_id));
        /*print_r($update);
        exit;*/
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit($id = false){
       
       $id = $this->common_model->id_decrypt($id);

		if(isset($_POST['submit']))
		{   
			     $test_type = strtolower($this->input->post ('test_type'));
				 //exit;
				 $updateid = $this->common_model->updateData('test_type',array('type_name'=>$test_type),array('id'=>$id));
				 
				 if($updateid == '' && $updateid == '0') {
				 	
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('test_type'));
				 }
				 else
				 {
                    $this->session->set_flashdata('success' ,'Test added successfully');	
				    redirect(base_url('test_type/test_type_list'));
				 }	
		}
		
		$data['test_type_edit'] = $this->common_model->common_getRow('test_type',array('id'=>$id));
       
       $this->load->view('admin/test_type/edit_test_type',$data);
	}
}