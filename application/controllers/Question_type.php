<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_type extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
		
	}

	public function index()
	{
		$currentdate = date("Y-m-d H:i:s");

		if(isset($_POST['submit']))
		{   
			     $question_type_name = strtoupper($this->input->post ('question_type_name'));
	 
		         $insertid = $this->common_model->common_insert('question_type',array('name'=>$question_type_name,'status'=>'1','create_at'=>$currentdate));
				 
				 if($insertid == '' || $insertid == '0') {
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('question_type/question_type_list'));
				 }
				 else
				 {
                    $this->session->set_flashdata('success' ,'Question Type added successfully');	
				    redirect(base_url('question_type/question_type_list'));
				 }	
		}
       $this->load->view('admin/question_type/add_question_type');
	}

	public function question_type_list(){

	$data['question_type_list'] = $this->common_model->getData('question_type',array(),'id','DESC');
				 
       $this->load->view('admin/question_type/question_type_list', $data);
	}

	public function edit($id = false){

    	$id = $this->common_model->id_decrypt($id);
        
		if(isset($_POST['submit']))
		{   
			     $question_type_name = strtoupper($this->input->post ('question_type_name'));
	 
		         $insertid = $this->common_model->updateData('question_type',array('name'=>$question_type_name),array('id'=>$id));
				 
				 if($insertid == '' || $insertid == '0') {
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('question_type/question_type_list'));
				 }
				 else
				 {
                    $this->session->set_flashdata('success' ,'Question Type Updated successfully');	
				    redirect(base_url('question_type/question_type_list'));
				 }	
		}
       
        
       $data['edit_question_type'] = $this->common_model->common_getRow('question_type',array('id'=>$id));
       
       $this->load->view('admin/question_type/edit_question_type', $data);
	}
	
}