<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//print_r($this->session->userdata);
		//exit;
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
		date_default_timezone_set('Asia/Kolkata');
	}
	
	public function index()
    {
	   $data['musp_list'] = $this->common_model->getData('musp',array('added_by'=>'coaching','added_id'=>$this->session->userdata('admin_id')),'id','DESC');
				 
       $this->load->view('admin/subadmin/musp/musp_list', $data);
	}
}
