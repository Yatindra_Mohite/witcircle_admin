<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subjective extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
     
	    }
	    if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
    } 

	//public function index(){
//		echo "sdsds";
//	}
	
	public function exam_paper_list(){
        
        $data['subjective'] = $this->common_model->getData('subjective',array('sub_type'=>1),'id','DESC');

		$this->load->view('admin/subjective/exam_paper_list',$data);
	}

	public function add_exam_paper(){

	  if(isset($_POST['submit']))
	  { 
	    $currentdate = date("Y-m-d H:i:s");	 
	  	$course_ids =$this->input->post ('course_id');
	  	$title = $this->input->post('title');

	       if(isset($_FILES['image']['name'][0]) && $_FILES['image']['name'][0] != '')
		   {  
		          //$files = $_FILES; 
		          //$filesCount1 = count($_FILES['image']['name']);
		                
		          // for($i = 0; $i < $filesCount1; $i++)
		          // {
		                 // $_FILES['image']['name'] =  $files['image']['name'][$i];
		                 // $_FILES['image']['type'] =   $files['image']['type'][$i];
		                //  $_FILES['image']['tmp_name'] =  $files['image']['tmp_name'][$i];
		                //  $_FILES['image']['error'] =  $files['image']['error'][$i];
		               //   $_FILES['image']['size'] =  $files['image']['size'][$i];

		                      $date = date("ymdhis");  
		                      $uploadPath = 'uploads/subjective/exam_paper';
		                      $config['upload_path'] = $uploadPath;
		                      $config['allowed_types'] = 'pdf';
		                      $config['max_size'] = '5120';
		                      $config['file_name'] = md5($_FILES['image']['name']);
                             //exit;
		                      $fileName = $config['file_name'];
		                      $fileName11[] = $config['file_name'];
		                      
		                      $this->load->library('upload', $config);
		                      $this->upload->initialize($config);
		                      if($this->upload->do_upload('image'))
					          { //die('upload');
					            $image_data = $this->upload->data();
					            echo $rest_image =  $image_data['file_name'];

					            $arr = array('chapter_coin'=>'','course_id'=>$course_ids,'sub_type'=>1,'title'=>$title,'file_name'=>$rest_image,'status'=>0,'create_at'=>$currentdate);
					           // print_r($arr);
					           // exit;

		                        $insertid = $this->common_model->common_insert('subjective',$arr);
					          }else
					          {  
					            $err = $this->upload->display_errors();
					            $final_output = $this->session->set_flashdata('error_pic', $err);
					            redirect(base_url('subjective/add_exam_paper'));
					          }
		          //} 
		    }

		    
	           
		    if($insertid == '' || $insertid == '0') {
		 	
		 		$this->session->set_flashdata('failed' ,'Something went wrong');	
		    	redirect(base_url('subjective/add_exam_paper'));
			}
			else
			{
				//$this->db->query("UPDATE global_counters SET subject_count = subject_count + 1 WHERE id = 1");
	            $this->session->set_flashdata('success' ,'Exam paper added successfully');	
			    redirect(base_url('subjective/add_exam_paper'));
			}
	  }    
		$this->load->view('admin/subjective/add_exam_paper');
	}

	public function change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("subjective",array('status'=>$status),array('id'=>$user_id));
        //print_r($update);
        //exit;
          if($update)
          {
             echo '1000';exit; 
          }
    }

    // public function add_content(){
    //     $this->load->view('admin/subjective/add_content');
    // }

    public function content_list(){
        
        $data['subjective'] = $this->common_model->getData('subjective',array('sub_type'=>2),'id','DESC');

		$this->load->view('admin/subjective/content_list',$data);
	}

	public function get_subject()
	{
		$course_id = $this->input->post('course_id');
		$subject=$this->db->query("select * from in_short_subject where course_id=".$course_id)->result();
		 if (empty($subject)) {?>
		  <option value="" selected="selected">No subject in this course</option>
		   <?php }else{?>
	      <option value="" disabled selected="selected">Select subject</option>
	  <?php }
	  
	  foreach ($subject as $key) 
	  { 
	  ?>
	    <option value="<?php echo $key->id;?>" <?php if($key->id == $this->session->userdata('chapter_id_sess2')){ echo "selected";} ?>><?php echo ucwords($key->subject_name);?></option>
	   <?php
	  }
	}

	public function get_chapter()
	{
		$subject_id = $this->input->post('subject_id');
		$chapter=$this->db->query("select * from in_short_chapter where subject_id=".$subject_id)->result();
		 if (empty($chapter)) {?>
		  <option value="" selected="selected">No chapters in this subject</option>
		   <?php }else{?>
	      <option value="" disabled selected="selected">Select chapter</option>
	  <?php }
	  
	  foreach ($chapter as $key) 
	  { 
	  ?>
	    <option value="<?php echo $key->id;?>" <?php if($key->id == $this->session->userdata('chapter_id_sess2')){ echo "selected";} ?>><?php echo $key->chapter_name;?></option>
	   <?php
	  }
	}

	public function add_content(){

	  if(isset($_POST['submit']))
	  { 
	    $currentdate = date("Y-m-d H:i:s");	 
	  	$course_ids =$this->input->post ('course_id');

	       if(isset($_FILES['image']['name'][0]) && $_FILES['image']['name'][0] != '')
		   {  
		          $files = $_FILES; 
		          $filesCount1 = count($_FILES['image']['name']);
		                
		          for($i = 0; $i < $filesCount1; $i++)
		          {
		                  $_FILES['image']['name'] =  $files['image']['name'][$i];
		                  $_FILES['image']['type'] =   $files['image']['type'][$i];
		                  $_FILES['image']['tmp_name'] =  $files['image']['tmp_name'][$i];
		                  $_FILES['image']['error'] =  $files['image']['error'][$i];
		                  $_FILES['image']['size'] =  $files['image']['size'][$i];

		                      $date = date("ymdhis");  
		                      $uploadPath = 'uploads/subjective/content';
		                      $config['upload_path'] = $uploadPath;
		                      $config['allowed_types'] = 'pdf';
		                      $config['max_size'] = '5120';
		                      $config['file_name'] = md5($date.$_FILES['image']['name']);

		                      $fileName = $config['file_name'];
		                      $fileName11[] = $config['file_name'];
		                      
		                      $this->load->library('upload', $config);
		                      $this->upload->initialize($config);
		                      if($this->upload->do_upload('image'))
					          { //die('upload');
					            $image_data = $this->upload->data();
					            $rest_image =  $image_data['file_name'];

				            	   $arr = array('chapter_coin'=>'',
										    	'course_id'=>$course_ids,
										    	'subject_id' =>$this->input->post('subject_id'),
					              				'chapter_id'=> $this->input->post('chapter_id'),
					              				'chapter_coin'=>$this->input->post('chapter_coin'),
					              				'title'=>$this->input->post('title'),
										    	'sub_type'=>2,
										    	'file_name'=>$rest_image,
										    	'status'=>0,
										    	'create_at'=>$currentdate
							    			);

		                         $insertid = $this->common_model->common_insert('subjective',$arr);
	           
					          }else
					          {  
					            $err = $this->upload->display_errors();
					            $final_output = $this->session->set_flashdata('failed', $err);
					            redirect(base_url('subjective/add_content'));
					          }
		          } 
		    }

		    
		    if($insertid == '' || $insertid == '0') {
		 	
		 		$this->session->set_flashdata('failed' ,'Something went wrong');	
		    	redirect(base_url('subjective/add_content'));
			}
			else
			{
				//$this->db->query("UPDATE global_counters SET subject_count = subject_count + 1 WHERE id = 1");
	            $this->session->set_flashdata('success' ,'Content added successfully');	
			    redirect(base_url('subjective/add_content'));
			}
	  }    
		$this->load->view('admin/subjective/add_content');
	}
	
	public function delete($id = false){
		$delete = $this->common_model->deleteData("subjective",array('id'=>$id));
		    if($delete)
		    {
		         echo '1000';exit; 
		    }
	}
	
		
}