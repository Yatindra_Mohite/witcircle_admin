<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gk_chapter extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
	}


	// public function index(){
       
	// 	if(isset($_POST['submit']))
	// 	{   

	// 	    $chapter_name = strtolower($this->input->post ('name'));
	// 	    $title = strtolower($this->input->post ('title'));

			 
	// 		$check = $this->common_model->getData('gk_chapter',array('name'=>$chapter_name,'title'=>$title));
 //            if( count($check) > 0){

 //                $this->session->set_flashdata('failed' ,'This chapter is alredy added in your list.');	
	// 		    redirect(base_url('gk_chapter'));
 //            }
 //            else
 //            {
 //            	$insertid = $this->common_model->common_insert('gk_chapter',array('name'=>$chapter_name,'title'=>$title));
			 
	// 			if($insertid == '' || $insertid == '0')
	// 			{
	// 			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
	// 			    redirect(base_url('gk_chapter'));
	// 			}
	// 			else
	// 			{   

	//                 $this->session->set_flashdata('success' ,'GK Chapter added successfully');	
	// 			    redirect(base_url('Gk_chapter'));
	// 			}
 //            }		
	// 	}
		
 //    	$this->load->view('admin/in_short/gk_chapter/add_chapter');
	//  }

	//  public function gk_chapter_list(){

	// 	$data['chapter_list'] = $this->common_model->getData('gk_chapter',array(),'id','DESC');
				 
 //       $this->load->view('admin/in_short/gk_chapter/chapter_list', $data);
	// }

	// public function change_status(){
 //        $user_id = $this->input->post('user_id');
 //        $status = $this->input->post('admin_status');
              
 //        $update = $this->common_model->updateData("gk_chapter",array('status'=>$status),array('id'=>$user_id));
 //        //print_r($update);
 //        //exit;
 //          if($update)
 //          {
 //             echo '1000';exit; 
 //          }
 //    }

 //    public function edit($id = false){
       
	// 	if(isset($_POST['submit']))
	// 	{   

	// 	    $chapter_name = strtolower($this->input->post ('name'));
	// 	    $title = strtolower($this->input->post ('title'));

			 
	// 		$check = $this->common_model->getData('gk_chapter',array('name'=>$chapter_name,'title'=>$title));
 //            if( count($check) > 0){

 //                $this->session->set_flashdata('failed' ,'This chapter is alredy added in your list.');	
	// 		    redirect(base_url('gk_chapter/gk_chapter_list'));
 //            }
 //            else
 //            {
 //            	$insertid = $this->common_model->updateData('gk_chapter',array('name'=>$chapter_name,'title'=>$title),array('id'=>$id));
			 
	// 			if($insertid == '' || $insertid == '0')
	// 			{
	// 			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
	// 			    redirect(base_url('gk_chapter/gk_chapter_list'));
	// 			}
	// 			else
	// 			{   

	//                 $this->session->set_flashdata('success' ,'GK Chapter updated successfully');	
	// 			    redirect(base_url('Gk_chapter/gk_chapter_list'));
	// 			}
 //            }		
	// 	}
		
	// 	$data['chapter_edit'] = $this->common_model->common_getRow('gk_chapter',array('id'=>$id));

 //    	$this->load->view('admin/in_short/gk_chapter/edit_chapter',$data);
	//  }
}	
