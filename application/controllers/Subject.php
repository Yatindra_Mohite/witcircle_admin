<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
	}

	public function index(){
       
     $currentdate = date("Y-m-d H:i:s");

		if(isset($_POST['submit']))
		{   

	        $subject_name = strtolower($this->input->post ('subject_name'));
            
            $check = $this->common_model->getData('course_subject',array('subject_name'=>$subject_name,));
            if( count($check) > 0) {
                
                $this->session->set_flashdata('failed' ,'This subject is alredy added in your list.');	
			    redirect(base_url('subject'));

            }else{
	            
	            $course_ids =$this->input->post ('course_id');

	            $title =$this->input->post ('title_name');
                
                $course_id = implode(",",$course_ids);
            
			 	$insertid = $this->common_model->common_insert('course_subject',array('course_id'=>$course_id,'subject_name'=>$subject_name,'title'=>$title,'create_at'=>$currentdate));
	           
			    if($insertid == '' && $insertid == '0') {
			 	
			 		$this->session->set_flashdata('failed' ,'Something went wrong');	
			    	redirect(base_url('subject'));
				}
				else
				{
					$this->db->query("UPDATE global_counters SET subject_count = subject_count + 1 WHERE id = 1");

		            $this->session->set_flashdata('success' ,'Subject added successfully');	
				    redirect(base_url('subject/subject_list'));
				}	
            }
		}
    $this->load->view('admin/subject/add_subject');
	 }

	 public function subject_list(){
		$data['course_subject'] = $this->common_model->getData('course_subject',array(),'id','DESC');
			
		for($i=0;$i<count($data['course_subject']); $i++){
			$str = $data['course_subject'][$i]->course_id;
			$ids = explode(",", $str);
			//print_r($ids);
			//exit;
			$course_name = array();
			for($j=0; $j<count($ids); $j++){
				$course_name[$j]=$this->db->query("select * from course where id = ".$ids[$j])->row()->course_name;
			}
			$course_name=implode(",", $course_name);
			$data['course_subject'][$i]->course_name = $course_name;
			//print_r($data['course_subject']);
		}	//exit;	

    	$this->load->view('admin/subject/subject_list', $data);
	}

	public function change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("course_subject",array('status'=>$status),array('id'=>$user_id));
        /*print_r($update);
        exit;*/
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit($id = flase){
       
    $id = $this->common_model->id_decrypt($id);

		if(isset($_POST['submit']))
		{   
			     $course_ids =$this->input->post ('course_id');

                 $title =$this->input->post ('title_name');

			     $course_id = implode(",",$course_ids);
			     
			     $subject_name = strtolower($this->input->post ('subject_name'));
				 //exit;
				 $insertid = $this->common_model->updateData('course_subject',array('course_id'=>$course_id,'title'=>$title,'subject_name'=>$subject_name),array('id'=>$id));
				 
				 if($insertid == '' || $insertid == '0') {
				 	
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('subject'));
				 }
				 else
				 {
                    $this->session->set_flashdata('success' ,'Subject updated successfully');	
				    redirect(base_url('subject/subject_list'));
				 }	
		}

		$data['course_subject_edit'] = $this->common_model->common_getRow('course_subject',array('id'=>$id));

    $this->load->view('admin/subject/edit_subject', $data);
	 }
}