<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advertisement extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$militime=round(microtime(true) * 1000);
		define('militime', $militime);	
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
	}

	 public function add_advertisement(){

        if(isset($_POST['submit']))
		{   

			$type = $this->input->post('type');
            $text = $this->input->post('text');
            $heading = $this->input->post('heading');
            
            $rest_image = '';

            if ($this->input->post('type') == 5) {
            	
                $questions  = $this->input->post ('questions');
              	$opt_1 = $this->input->post('opt_1');
              	$opt_2 = $this->input->post('opt_2');
              	$opt_3  = $this->input->post ('opt_3');
              	$opt_4 = $this->input->post('opt_4');
              	$answer = $this->input->post('answer');

            }
            else
            {
              	$questions  = '';
              	$opt_1 = '';
              	$opt_2 = '';
              	$opt_3  = '';
              	$opt_4 = '';
              	$answer = '';
            }

            	if($type == 2 || $type == 1 || $type == 4 ) 
            	{

            		if ($type == 1 ) {
            			$text = '';
            			

            		}
            		if ( $type == 4 ) {
            			$heading = '';
            		}

		            if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		            {
		                  $_FILES['image']['name']= $_FILES['image']['name'];
		                  $_FILES['image']['type']= $_FILES['image']['type'];
		                  $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		                  $_FILES['image']['error']= $_FILES['image']['error'];
		                  $_FILES['image']['size']= $_FILES['image']['size'];

		                  $config = array();
		                  $config['upload_path']   = 'uploads/advertisement/';
		                  $config['allowed_types'] = 'jpg|jpeg|png|gif';

		                  $config['file_name'] = militime.$_FILES['image']['name'];
		                        
		                  $this->load->library('upload',$config);
		                  $this->upload->initialize($config);
		                  if($this->upload->do_upload('image'))
		                  { 
		                    $image_data = $this->upload->data();
		                    $rest_image =  $image_data['file_name'];

		                  }else
		                  {  
		                    $err= $this->upload->display_errors();
		                    $final_output = $this->session->set_flashdata('error_pic', $err);
		                  }
		                    // move_uploaded_file($image,"../../uploads/subcategory_image/".$image_name);
		                    // //$u_image1 = base_url."uploads/user_image/".$image_name;
		             }
            	
            	}

              $arr = array(
              	'card_section' => $this->input->post('card_section'),
              	'short_type' => $this->input->post('type'),
              	'heading'  => $heading,
              	'text' => $text,
              	'image' => $rest_image,
              	'question'=>$questions,
              	'option_1'=>$opt_1,
              	'option_2'=>$opt_2,
              	'option_3'=>$opt_3,
              	'option_4'=>$opt_4,
              	'answer'=>$answer,
              	'create_at'=>militime
              	);

              $insertid = $this->common_model->common_insert('advertisement',$arr);
              //print_r($this->db->last_query());
              //exit;
	           
			    if($insertid == '' || $insertid == '0') {
			 	
			 		$this->session->set_flashdata('failed' ,'Something went wrong');	
			    	redirect(base_url('advertisement/add_advertisement'));
				}
				else
				{
					//$this->db->query("UPDATE global_counters SET subject_count = subject_count + 1 WHERE id = 1");
     				//       $this->session->set_userdata ( array (
				 	//       'course_id_sess'=> $this->input->post('course_id'),
					// 		 'subject_id_sess'   => $this->input->post ('subject_id'),
					// 		 'chapter_id_sess'   => $this->input->post ('chapter_id'),
					// 		 'type_sess'   => $this->input->post ('type'),								
					// ));


		            $this->session->set_flashdata('success' ,'Advertisement added successfully');	
				    redirect(base_url('advertisement/add_advertisement'));
				}
		}

        $this->load->view('admin/advertisement/add_advertisement');
	 }

	public function advertisement_list(){

       $data['advertisement_list'] = $this->common_model->getData('advertisement',array(),'id','DESC');
       $this->load->view('admin/advertisement/advertisement_list',$data);
	 
	 }
    
    public function delete($id = false){
        
        $delete = $this->common_model->deleteData("advertisement",array('id'=>$id));
	    if($delete)
	    {
	         echo '1000';exit; 
	    }

    }

	public function edit_advertisement($id)
	{   
		if(isset($_POST['submit']))
		{

			$dat = $this->common_model->common_getRow("advertisement",array('id'=>$id));
             if ($this->input->post('type') == 1) {

             	$rest_image = '';
             	 if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		            {
		                  $_FILES['image']['name']= $_FILES['image']['name'];
		                  $_FILES['image']['type']= $_FILES['image']['type'];
		                  $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		                  $_FILES['image']['error']= $_FILES['image']['error'];
		                  $_FILES['image']['size']= $_FILES['image']['size'];

		                  $config = array();
		                  $config['upload_path']   = 'uploads/advertisement/';
		                  $config['allowed_types'] = 'jpg|jpeg|png|gif';

		                  $config['file_name'] = militime.$_FILES['image']['name'];
		                        
		                  $this->load->library('upload',$config);
		                  $this->upload->initialize($config);
		                  if($this->upload->do_upload('image'))
		                  { 
		                    $image_data = $this->upload->data();
		                    $rest_image =  $image_data['file_name'];

		                  }else
		                  {  
		                    $err= $this->upload->display_errors();
		                    $final_output = $this->session->set_flashdata('error_pic', $err);
		                  }
		                    // move_uploaded_file($image,"../../uploads/subcategory_image/".$image_name);
		                    // //$u_image1 = base_url."uploads/user_image/".$image_name;
		             }
		             if ($rest_image == '') {
		             	 $rest_image = $dat->image;
		             }

             		$arr = array('heading'=>$this->input->post ('heading'),
				              	 'image'=>$rest_image,
				              	);

              $insertid = $this->common_model->updateData('in_short',$arr,array('id'=> $id),array());
             	
             }

             if ($this->input->post('type') == 2) {

             	 $rest_image = '';
             	 if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		            {
		                  $_FILES['image']['name']= $_FILES['image']['name'];
		                  $_FILES['image']['type']= $_FILES['image']['type'];
		                  $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		                  $_FILES['image']['error']= $_FILES['image']['error'];
		                  $_FILES['image']['size']= $_FILES['image']['size'];

		                  $config = array();
		                  $config['upload_path']   = 'uploads/advertisement/';
		                  $config['allowed_types'] = 'jpg|jpeg|png|gif';

		                  $config['file_name'] = militime.$_FILES['image']['name'];
		                        
		                  $this->load->library('upload',$config);
		                  $this->upload->initialize($config);
		                  if($this->upload->do_upload('image'))
		                  { 
		                    $image_data = $this->upload->data();
		                    $rest_image =  $image_data['file_name'];

		                  }else
		                  {  
		                    $err= $this->upload->display_errors();
		                    $final_output = $this->session->set_flashdata('error_pic', $err);
		                  }
		                    // move_uploaded_file($image,"../../uploads/subcategory_image/".$image_name);
		                    // //$u_image1 = base_url."uploads/user_image/".$image_name;
		             }
		             if ($rest_image == '') {
		             	 $rest_image = $dat->image;
		             }

             	$arr = array('heading'=>$this->input->post ('heading'),
				              	'text'=>$this->input->post('text'),
				              	'image'=>$rest_image,
				              	);

              $insertid = $this->common_model->updateData('advertisement',$arr,array('id'=> $id),array());
             	
             }

             if ($this->input->post('type') == 3) {


             	$arr = array('heading'=>$this->input->post ('heading'),
				              	'text'=>$this->input->post('text'),
				              	
				              	);

              $insertid = $this->common_model->updateData('advertisement',$arr,array('id'=> $id),array());
             }
             
             if ($this->input->post('type') == 4) {
                 $rest_image = '';
             	 if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		            {
		                  $_FILES['image']['name']= $_FILES['image']['name'];
		                  $_FILES['image']['type']= $_FILES['image']['type'];
		                  $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		                  $_FILES['image']['error']= $_FILES['image']['error'];
		                  $_FILES['image']['size']= $_FILES['image']['size'];

		                  $config = array();
		                  $config['upload_path']   = 'uploads/advertisement/';
		                  $config['allowed_types'] = 'jpg|jpeg|png|gif';

		                  $config['file_name'] = militime.$_FILES['image']['name'];
		                        
		                  $this->load->library('upload',$config);
		                  $this->upload->initialize($config);
		                  if($this->upload->do_upload('image'))
		                  { 
		                    $image_data = $this->upload->data();
		                    $rest_image =  $image_data['file_name'];

		                  }else
		                  {  
		                    $err= $this->upload->display_errors();
		                    $final_output = $this->session->set_flashdata('error_pic', $err);
		                  }
		                    // move_uploaded_file($image,"../../uploads/subcategory_image/".$image_name);
		                    // //$u_image1 = base_url."uploads/user_image/".$image_name;
		             }
		             if ($rest_image == '') {
		             	 $rest_image = $dat->image;
		             }
                
                $arr = array('image'=>$rest_image,
				              	);

              $insertid = $this->common_model->updateData('advertisement',$arr,array('id'=> $id),array());

             }
             
             if ($this->input->post('type') == 5) {		

             	   $arr = array('question'=>$this->input->post ('questions'),
				              	'option_1'=>$this->input->post('opt_1'),
				              	'option_2'=>$this->input->post('opt_2'),
				              	'option_3'=>$this->input->post('opt_3'),
				              	'option_4'=>$this->input->post('opt_4'),
				              	'answer'=>$this->input->post('answer'),
				              	);

              $insertid = $this->common_model->updateData('advertisement',$arr,array('id'=> $id),array());
              
             }

             if($insertid == '' || $insertid == '0') {
			 	
			 		$this->session->set_flashdata('failed' ,'Something went wrong');	
			    	redirect(base_url('advertisement/advertisement_list'));
				}
				else
				{
		            $this->session->set_flashdata('success' ,'Advertisement update successfully');	
				    redirect(base_url('advertisement/advertisement_list'));
				}

		}

        $data['advertisement'] = $this->common_model->common_getRow("advertisement",array('id'=>$id));
        $this->load->view('admin/advertisement/edit_advertisement', $data);
	}

}