<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class In_short extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$militime=round(microtime(true) * 1000);
		define('militime', $militime);	
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			//echo $result;exit;
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
		
	}

	public function add_subject(){
       
     $currentdate = date("Y-m-d H:i:s");

		if(isset($_POST['submit']))
		{   

	        $subject_name = strtolower($this->input->post ('subject_name'));
            
            $check = $this->common_model->getData('course_subject',array('subject_name'=>$subject_name,));
            if( count($check) > 0) {
                
                $this->session->set_flashdata('failed' ,'This subject is alredy added in your list.');	
			    redirect(base_url('subject'));

            }else{
	            
	            $course_ids =$this->input->post ('course_id');

	            $title =$this->input->post ('title_name');
                 
			 	$insertid = $this->common_model->common_insert('in_short_subject',array('course_id'=>$course_ids,'subject_name'=>$subject_name,'title'=>$title,'status'=>1,'create_at'=>$currentdate));
	           
			    if($insertid == '' || $insertid == '0') {
			 	
			 		$this->session->set_flashdata('failed' ,'Something went wrong');	
			    	redirect(base_url('In_short/add_subject'));
				}
				else
				{
					//$this->db->query("UPDATE global_counters SET subject_count = subject_count + 1 WHERE id = 1");

		            $this->session->set_flashdata('success' ,'Subject added successfully');	
				    redirect(base_url('In_short/add_subject'));
				}	
            }
		}

	    $this->load->view('admin/in_short/subject/add_subject');
	 }



	 public function subject_list(){

	 	$data['subject_data'] = $this->common_model->getData('in_short_subject',array(),'id','DESC');
        
	    $this->load->view('admin/in_short/subject/subject_list', $data);
	 }

	 public function edit_subject($id = flase){
       
	    $id = $this->common_model->id_decrypt($id);

			if(isset($_POST['submit']))
			{   
				     $course_ids =$this->input->post ('course_id');

	                 $title =$this->input->post ('title_name');
				     
				     $subject_name = strtolower($this->input->post ('subject_name'));
					 
					 $insertid = $this->common_model->updateData('in_short_subject',array('course_id'=>$course_ids,'title'=>$title,'subject_name'=>$subject_name),array('id'=>$id));

					 if($insertid == '' || $insertid == '0') {
					 	
					 	$this->session->set_flashdata('failed' ,'Something went wrong');	
					    redirect(base_url('In_short/subject_list'));
					 }
					 else
					 {
	                    $this->session->set_flashdata('success' ,'Subject added successfully');	
					    redirect(base_url('In_short/subject_list'));
					 }	
			}

			$data['course_subject_edit'] = $this->common_model->common_getRow('in_short_subject',array('id'=>$id));

	    $this->load->view('admin/in_short/subject/edit_subject', $data);
	 }

	 public function change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("in_short_subject",array('status'=>$status),array('id'=>$user_id));
        //print_r($update);
        //exit;
          if($update)
          {
             echo '1000';exit; 
          }
    }

	 public function add_chapter(){
       
     $currentdate = date("Y-m-d H:i:s");

		if(isset($_POST['submit']))
		{   
		    $course_id =0;
		    $subject_id =$this->input->post ('subject_id');
		    $chapter_name = strtolower($this->input->post ('chapter_name'));
			 
			$check = $this->common_model->getData('chapter',array('chapter_name'=>$chapter_name,'subject_id'=>$subject_id));
            if( count($check) > 0){

                $this->session->set_flashdata('failed' ,'This chapter is alredy added in your list.');	
			    redirect(base_url('chapter'));
            }
            else
            {
            	$insertid = $this->common_model->common_insert('in_short_chapter',array('course_id'=>$course_id,'subject_id'=>$subject_id,'chapter_name'=>$chapter_name,'status'=>1,'create_at'=>$currentdate));
			 
				if($insertid == '' || $insertid == '0')
				{
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('in_short/add_chapter'));
				}
				else
				{
					$this->db->query("UPDATE global_counters SET chapter_count = chapter_count + 1 WHERE id = 1");

	                $this->session->set_flashdata('success' ,'Chapter added successfully');	
				    redirect(base_url('in_short/add_chapter'));
				}
            }		
		}
		$data['subjects'] = $this->db->query("select * from in_short_subject")->result();		
    	$this->load->view('admin/in_short/chapter/add_chapter',$data);
	 }

	 public function chapter_list(){

	    $data['chapter_list'] = $this->common_model->getData('in_short_chapter',array(),'id','DESC');
				 
        $this->load->view('admin/in_short/chapter/chapter_list', $data);
	}

	public function change_status1(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("in_short_chapter",array('status'=>$status),array('id'=>$user_id));
        
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit_chapter($id = false){
       
     $id = $this->common_model->id_decrypt($id);

		if(isset($_POST['submit']))
		{   
			     $course_id =0;
			     $subject_id =$this->input->post ('subject_id');
			     $chapter_name = strtolower($this->input->post ('chapter_name'));
				 
				 $updateid = $this->common_model->updateData('in_short_chapter',array('course_id'=>$course_id,'subject_id'=>$subject_id,'chapter_name'=>$chapter_name),array('id'=>$id));
				 
				 if($updateid == '' || $updateid == '0') {
				 	
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('in_short/chapter_list'));
				 }
				 else
				 {
                    $this->session->set_flashdata('success' ,'Chapter added successfully');	
				    redirect(base_url('in_short/chapter_list'));
				 }	
		}
		
    $data['chapter_edit'] = $this->common_model->common_getRow('in_short_chapter',array('id'=>$id));

    $this->load->view('admin/in_short/chapter/edit_chapter', $data);
	 }

	 public function add_inshort(){
        if(isset($_POST['submit']))
		{   
            $date = date('Y-m-d');

			$type = $this->input->post('type');
            $text = $this->input->post('text');
            $heading = $this->input->post('heading');
            $gk_id = $this->input->post('gk_id');
            $rest_image = ''; 

            if($this->input->post('card_section') == 2)
            {  	
                $course  = $this->input->post ('course_id');
              	$subject = $this->input->post('subject_id');
              	$chapter = $this->input->post('chapter_id');
              	
            }else{
                $course = '';
              	$subject = '';
              	$chapter = '';

            }

            if($this->input->post('card_section') == 1 || $this->input->post('card_section') == 2 || $this->input->post('card_section') == 3)
            {
            	$gk_id = '';
            }



            if ($this->input->post('type') == 5) {
            	
                $questions  = $this->input->post ('questions');
              	$opt_1 = $this->input->post('opt_1');
              	$opt_2 = $this->input->post('opt_2');
              	$opt_3  = $this->input->post ('opt_3');
              	$opt_4 = $this->input->post('opt_4');
              	$answer = $this->input->post('answer');

            }
            else
            {
              	$questions  = '';
              	$opt_1 = '';
              	$opt_2 = '';
              	$opt_3  = '';
              	$opt_4 = '';
              	$answer = '';
            }

            	if($type == 2 || $type == 1 || $type == 4 ) 
            	{

            		if ($type == 1 ) {
            			$text = '';
            			

            		}
            		if ( $type == 4 ) {
            			$heading = '';
            		}

		            if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		            {
		                  $_FILES['image']['name']= $_FILES['image']['name'];
		                  $_FILES['image']['type']= $_FILES['image']['type'];
		                  $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		                  $_FILES['image']['error']= $_FILES['image']['error'];
		                  $_FILES['image']['size']= $_FILES['image']['size'];

		                  $config = array();
		                  $config['upload_path']   = 'uploads/in_short/';
		                  $config['allowed_types'] = 'jpg|jpeg|png';

		                  $config['file_name'] = militime.$_FILES['image']['name'];
		                        
		                  $this->load->library('upload',$config);
		                  $this->upload->initialize($config);
		                  if($this->upload->do_upload('image'))
		                  { 
		                    $image_data = $this->upload->data();
		                    $rest_image =  $image_data['file_name'];

		                  }else
		                  {  
		                    $err= $this->upload->display_errors();
		                    $final_output = $this->session->set_flashdata('failed', $err);
		                  }
		                    // move_uploaded_file($image,"../../uploads/subcategory_image/".$image_name);
		                    // //$u_image1 = base_url."uploads/user_image/".$image_name;
		             }
            	
            	}

              $arr = array(
              	'card_section' => $this->input->post('card_section'),
              	'course_id'  => $course, 
              	'subject_id' => $subject,
              	'chapter_id' => $chapter,
              	'short_type' => $this->input->post('type'),
              	'heading'  => $heading,
              	'text' => $text,
              	'image' => $rest_image,
              	'gk_chapter' => $gk_id,
              	'question'=>$questions,
              	'option_1'=>$opt_1,
              	'option_2'=>$opt_2,
              	'option_3'=>$opt_3,
              	'option_4'=>$opt_4,
              	'answer'=>$answer,
              	'create_at'=>militime,
              	'date'=>$date
              	);

              $insertid = $this->common_model->common_insert('in_short',$arr);
	           
			    if($insertid == '' || $insertid == '0') {
			 	
			 		$this->session->set_flashdata('failed' ,'Something went wrong');	
			    	redirect(base_url('In_short/add_inshort'));
				}
				else
				{
					//$this->db->query("UPDATE global_counters SET subject_count = subject_count + 1 WHERE id = 1");
     				 $this->session->set_userdata ( array (
				 	        'card_section' => $this->input->post('card_section'),
							'type' => $this->input->post('type'),
							'course' => $this->input->post ('course_id'),
			              	'subject' => $this->input->post('subject_id'),
			              	'chapter' => $this->input->post('chapter_id'),
			              	'gk_chapter' => $this->input->post('gk_id'),								
					));


		            $this->session->set_flashdata('success' ,'Witcard added successfully');	
				    redirect(base_url('In_short/add_inshort'));
				}
		}

        $this->load->view('admin/in_short/add_inshort');
	 }

	public function inshort_list(){

       $data['inshort_list'] = $this->common_model->getData('in_short',array(),'id','DESC');
       $this->load->view('admin/in_short/inshort_list',$data);
	 
	 }

	 public function news_list(){

       $data['inshort_list'] = $this->common_model->getData('in_short',array('card_section'=>1),'id','DESC');
       $this->load->view('admin/in_short/news_list',$data);
	 
	 }

	 public function current_affairs_list(){

       $data['inshort_list'] = $this->common_model->getData('in_short',array('card_section'=>3),'id','DESC');
       $this->load->view('admin/in_short/current_affairs_list',$data);
	 
	 }

	 public function study_zone_list(){

       //$data['inshort_list'] = $this->common_model->getData('in_short',array('card_section'=>2),'id','DESC');
	 	// echo "SELECT DISTINCT subject_id, chapter_id,count(chapter_id) as no_of_q FROM questions WHERE chapter_id!= 0 GROUP BY chapter_id,subject_id";
	 	// exit;
	 	$data['inshort_list'] = $this->db->query("SELECT DISTINCT subject_id, chapter_id,count(chapter_id) as no_of_q FROM in_short WHERE chapter_id!= 0 GROUP BY chapter_id,subject_id")->result();
       $this->load->view('admin/in_short/study_zone_list',$data);
	 
	 }

	 public function all_card_study_zone($subject_id =false , $chapter_id = false)
    {

	   $s_id = $this->common_model->id_decrypt($subject_id);

	   $c_id = $this->common_model->id_decrypt($chapter_id);

       $data['inshort_list'] = $this->common_model->getData('in_short',array('subject_id'=>$s_id,'chapter_id'=>$c_id));
       //print_r($data['subject']);
       $this->load->view('admin/in_short/study_card_list', $data);
	}

	 public function gk_card_list(){

       //$data['inshort_list'] = $this->common_model->getData('in_short',array('card_section'=>2),'id','DESC');
	 	// echo "SELECT DISTINCT subject_id, chapter_id,count(chapter_id) as no_of_q FROM questions WHERE chapter_id!= 0 GROUP BY chapter_id,subject_id";
	 	// exit;
	 	$data['inshort_list'] = $this->db->query("SELECT DISTINCT gk_chapter,count(gk_chapter) as no_of_q FROM in_short WHERE card_section= 4 AND gk_chapter!= 0 GROUP BY gk_chapter")->result();
       $this->load->view('admin/in_short/gk_list',$data);
	 
	 }

	  public function all_card_gk($chapter_id = false)
    {

	   $c_id = $this->common_model->id_decrypt($chapter_id);

       $data['inshort_list'] = $this->common_model->getData('in_short',array('gk_chapter'=>$c_id));
       //print_r($data['subject']);
       $this->load->view('admin/in_short/gk_card_list', $data);
	}
    
    public function delete($id = false){
        
        $delete = $this->common_model->deleteData("in_short",array('id'=>$id));
	    if($delete)
	    {
	         echo '1000';exit; 
	    }

    }


	public function get_subject()
	{
		$course_id = $this->input->post('course_id');
		$subject=$this->db->query("select * from in_short_subject where course_id=".$course_id)->result();
		 if (empty($subject)) {?>
		  <option value="" selected="selected">No subject in this course</option>
		   <?php }else{?>
	      <option value="" disabled selected="selected">Select subject</option>
	  <?php }
	  
	  foreach ($subject as $key) 
	  { 
	  ?>
	    <option value="<?php echo $key->id;?>" <?php if($key->id == $this->session->userdata('subject')){ echo "selected";} ?>><?php echo ucwords($key->subject_name);?></option>
	   <?php
	  }
	}

	public function get_chapter()
	{
		$subject_id = $this->input->post('subject_id');
		$chapter=$this->db->query("select * from in_short_chapter where subject_id=".$subject_id)->result();
		 if (empty($chapter)) {?>
		  <option value="" selected="selected">No chapters in this subject</option>
		   <?php }else{?>
	      <option value="" disabled selected="selected">Select chapter</option>
	  <?php }
	  
	  foreach ($chapter as $key) 
	  { 
	  ?>
	    <option value="<?php echo $key->id;?>" <?php if($key->id == $this->session->userdata('chapter')){ echo "selected";} ?>><?php echo $key->chapter_name;?></option>
	   <?php
	  }
	}

	public function edit_inshort($id)
	{   
		if(isset($_POST['submit']))
		{
           
			$dat = $this->common_model->common_getRow("in_short",array('id'=>$id));

             if ($this->input->post('type') == 1) {

             	$rest_image = '';
             	 if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		            {
		                  $_FILES['image']['name']= $_FILES['image']['name'];
		                  $_FILES['image']['type']= $_FILES['image']['type'];
		                  $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		                  $_FILES['image']['error']= $_FILES['image']['error'];
		                  $_FILES['image']['size']= $_FILES['image']['size'];

		                  $config = array();
		                  $config['upload_path']   = 'uploads/in_short/';
		                  $config['allowed_types'] = 'jpg|jpeg|png';

		                  $config['file_name'] = militime.$_FILES['image']['name'];
		                        
		                  $this->load->library('upload',$config);
		                  $this->upload->initialize($config);
		                  if($this->upload->do_upload('image'))
		                  { 
		                    $image_data = $this->upload->data();
		                    $rest_image =  $image_data['file_name'];

		                  }else
		                  {  
		                    $err= $this->upload->display_errors();
		                    $final_output = $this->session->set_flashdata('failed', $err);
		                  }
		                    // move_uploaded_file($image,"../../uploads/subcategory_image/".$image_name);
		                    // //$u_image1 = base_url."uploads/user_image/".$image_name;
		             }
		             if ($rest_image == '') {
		             	 $rest_image = $dat->image;
		             }

             		$arr = array('heading'=>$this->input->post ('heading'),
				              	 'image'=>$rest_image,
				              	);

              $insertid = $this->common_model->updateData('in_short',$arr,array('id'=> $id),array());
             	
             }

             if ($this->input->post('type') == 2) {

             	 $rest_image = '';
             	 if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		            {
		                  $_FILES['image']['name']= $_FILES['image']['name'];
		                  $_FILES['image']['type']= $_FILES['image']['type'];
		                  $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		                  $_FILES['image']['error']= $_FILES['image']['error'];
		                  $_FILES['image']['size']= $_FILES['image']['size'];

		                  $config = array();
		                  $config['upload_path']   = 'uploads/in_short/';
		                  $config['allowed_types'] = 'jpg|jpeg|png';

		                  $config['file_name'] = militime.$_FILES['image']['name'];
		                        
		                  $this->load->library('upload',$config);
		                  $this->upload->initialize($config);
		                  if($this->upload->do_upload('image'))
		                  { 
		                    $image_data = $this->upload->data();
		                    $rest_image =  $image_data['file_name'];

		                  }else
		                  {  
		                    $err= $this->upload->display_errors();
		                    $final_output = $this->session->set_flashdata('failed', $err);
		                  }
		                    // move_uploaded_file($image,"../../uploads/subcategory_image/".$image_name);
		                    // //$u_image1 = base_url."uploads/user_image/".$image_name;
		             }
		             if ($rest_image == '') {
		             	 $rest_image = $dat->image;
		             }

             	$arr = array('heading'=>$this->input->post ('heading'),
				              	'text'=>$this->input->post('text'),
				              	'image'=>$rest_image,
				              	);

              $insertid = $this->common_model->updateData('in_short',$arr,array('id'=> $id),array());
             	
             }

             if ($this->input->post('type') == 3) {


             	$arr = array('heading'=>$this->input->post ('heading'),
				              	'text'=>$this->input->post('text'),
				              	
				              	);

              $insertid = $this->common_model->updateData('in_short',$arr,array('id'=> $id),array());
             }
             
             if ($this->input->post('type') == 4) {
                 $rest_image = '';
             	 if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		            {
		                  $_FILES['image']['name']= $_FILES['image']['name'];
		                  $_FILES['image']['type']= $_FILES['image']['type'];
		                  $_FILES['image']['tmp_name']= $_FILES['image']['tmp_name'];
		                  $_FILES['image']['error']= $_FILES['image']['error'];
		                  $_FILES['image']['size']= $_FILES['image']['size'];

		                  $config = array();
		                  $config['upload_path']   = 'uploads/in_short/';
		                  $config['allowed_types'] = 'jpg|jpeg|png';

		                  $config['file_name'] = militime.$_FILES['image']['name'];
		                        
		                  $this->load->library('upload',$config);
		                  $this->upload->initialize($config);
		                  if($this->upload->do_upload('image'))
		                  { 
		                    $image_data = $this->upload->data();
		                    $rest_image =  $image_data['file_name'];

		                  }else
		                  {  
		                    $err= $this->upload->display_errors();
		                    $final_output = $this->session->set_flashdata('failed', $err);
		                  }
		                    // move_uploaded_file($image,"../../uploads/subcategory_image/".$image_name);
		                    // //$u_image1 = base_url."uploads/user_image/".$image_name;
		             }
		             if ($rest_image == '') {
		             	 $rest_image = $dat->image;
		             }
                
                $arr = array('image'=>$rest_image,
				              	);

              $insertid = $this->common_model->updateData('in_short',$arr,array('id'=> $id),array());

             }
             
             if ($this->input->post('type') == 5) {		

             	   $arr = array('question'=>$this->input->post ('questions'),
				              	'option_1'=>$this->input->post('opt_1'),
				              	'option_2'=>$this->input->post('opt_2'),
				              	'option_3'=>$this->input->post('opt_3'),
				              	'option_4'=>$this->input->post('opt_4'),
				              	'answer'=>$this->input->post('answer'),
				              	);

              $insertid = $this->common_model->updateData('in_short',$arr,array('id'=> $id),array());
              
             }

             if ($dat->card_section == 1) {

             	  if($insertid == '' || $insertid == '0') {
			 	
		 		 $this->session->set_flashdata('failed' ,'Something went wrong');	
		    	 redirect(base_url('in_short/news_list'));
				 }
				 else
				 {
		             $this->session->set_flashdata('success' ,'Witcard update successfully');	
				     redirect(base_url('in_short/news_list'));
				 }
             	
             }

             elseif ($dat->card_section == 3) {

             	 if($insertid == '' || $insertid == '0') {
			 	
		 		 $this->session->set_flashdata('failed' ,'Something went wrong');	
		    	 redirect(base_url('in_short/current_affairs_list'));
				 }
				 else
				 {
		             $this->session->set_flashdata('success' ,'Witcard update successfully');	
				     redirect(base_url('in_short/current_affairs_list'));
				 }
             	
             }

             elseif ($dat->card_section == 2) {

             	 if($insertid == '' || $insertid == '0') {
			 	
		 		 $this->session->set_flashdata('failed' ,'Something went wrong');	
		    	 redirect(base_url('in_short/study_zone_list'));
				 }
				 else
				 {
		             $this->session->set_flashdata('success' ,'Witcard update successfully');	
				     redirect(base_url('in_short/study_zone_list'));
				 }
             	
             }

             elseif ($dat->card_section == 4) {

             	 if($insertid == '' || $insertid == '0') {
			 	
		 		 $this->session->set_flashdata('failed' ,'Something went wrong');	
		    	 redirect(base_url('in_short/gk_card_list'));
				 }
				 else
				 {
		             $this->session->set_flashdata('success' ,'Witcard update successfully');	
				     redirect(base_url('in_short/gk_card_list'));
				 }
                
             }

            

		}

        $data['inshort'] = $this->common_model->common_getRow("in_short",array('id'=>$id));
        $this->load->view('admin/in_short/edit_inshort', $data);
	}

	public function witcard_price(){

	    if(isset($_POST['submit']))
		{ 
            		
		    $arr = array('price'=>$this->input->post('price'));

		    $insertid = $this->common_model->updateData('witcard_price',$arr,array('id'=> 1),array());
		    if($insertid == '' || $insertid == '0')
		    {
			 	
		 		$this->session->set_flashdata('failed' ,'Something went wrong');	
		    	redirect(base_url('In_short/witcard_price'));
			}
			else
			{
		        $this->session->set_flashdata('success' ,'Witcard Price update successfully');	
			    redirect(base_url('In_short/witcard_price'));
			}             
		}
	   $data['price'] = $this->common_model->common_getRow("witcard_price",array('id'=>1));
       $this->load->view('admin/in_short/witcard_price', $data);   
    }


    public function add_gk_chapter(){
       
		if(isset($_POST['submit']))
		{   

		    $chapter_name = $this->input->post ('name');
		    $title = $this->input->post ('title');
			 
			$check = $this->common_model->getData('gk_chapter',array('name'=>$chapter_name,'title'=>$title));
            if( count($check) > 0){

                $this->session->set_flashdata('failed' ,'This chapter is alraedy added in your list.');	
			    redirect(base_url('gk_chapter'));
            }
            else
            {
            	$insertid = $this->common_model->common_insert('gk_chapter',array('name'=>$chapter_name,'title'=>$title));
			 
				if($insertid == '' || $insertid == '0')
				{
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('in_short/add_gk_chapter'));
				}
				else
				{   

	                $this->session->set_flashdata('success' ,'GK Chapter added successfully');	
				    redirect(base_url('in_short/add_gk_chapter'));
				}
            }		
		}
		
    	$this->load->view('admin/in_short/gk_chapter/add_chapter');
	 }

	 public function gk_chapter_list(){

		$data['chapter_list'] = $this->common_model->getData('gk_chapter',array(),'id','DESC');
				 
       $this->load->view('admin/in_short/gk_chapter/chapter_list', $data);
	}

	public function gk_chapter_change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("gk_chapter",array('status'=>$status),array('id'=>$user_id));
        //print_r($update);
        //exit;
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function gk_chapter_edit($id = false){
       
		if(isset($_POST['submit']))
		{   

		    $chapter_name = $this->input->post ('name');
		    $title = $this->input->post ('title');

			 
			$check = $this->common_model->getData('gk_chapter',array('name'=>$chapter_name,'title'=>$title));
            if( count($check) > 0){

                $this->session->set_flashdata('failed' ,'This chapter is already added in your list.');	
			    redirect(base_url('in_short/gk_chapter_list'));
            }
            else
            {
            	$insertid = $this->common_model->updateData('gk_chapter',array('name'=>$chapter_name,'title'=>$title),array('id'=>$id));
			 
				if($insertid == '' || $insertid == '0')
				{
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('in_short/gk_chapter_list'));
				}
				else
				{   

	                $this->session->set_flashdata('success' ,'GK Chapter updated successfully');	
				    redirect(base_url('in_short/gk_chapter_list'));
				}
            }		
		}
		
		$data['chapter_edit'] = $this->common_model->common_getRow('gk_chapter',array('id'=>$id));

    	$this->load->view('admin/in_short/gk_chapter/edit_chapter',$data);
	 }

}