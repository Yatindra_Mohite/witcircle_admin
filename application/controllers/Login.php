<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if($userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		
	}

	public function index()
	{ 
	   if($_SERVER['REQUEST_METHOD'] == 'POST')
		{   
			     $username = $this->input->post ('username');
				 $password = $this->input->post('password');
				 //exit;
				 $AdminData = $this->common_model->common_getRow('admin',array('user_email'=>$username));
				 //print_r($AdminData);
				 //exit;
				if (!empty($AdminData)) 
				{
					if($AdminData->user_status==1)
					{

						if($AdminData->role == 'admin')
						{
							$pass = md5($password);
						}else
						{
							$pass = sha1($password);
						}
						if($pass == $AdminData->user_password)
						{
							if($AdminData->role!='admin')
							{
								$this->session->set_userdata ( array (
									'admin_id'   => $AdminData->user_id,
									'role'   => $AdminData->role,
									'user_id'   => $AdminData->user_id,
									'user_name'   => $AdminData->user_name,
									'user_email'   => $AdminData->user_email,
									'menu'   => $AdminData->menu_id,
									'sub_menu'   => $AdminData->submenu_id,
									'temp_menu'   => $AdminData->temp_menu,
									'temp_sub_menu'   => $AdminData->temp_submenu
									));
								$this->common_model->updateData("admin",array('role'=>'subadmin'),array('user_id'=>$AdminData->user_id));
							}else
							{
								$this->session->set_userdata ( array (
									'admin_id'   => $AdminData->user_id,
									'role'   => $AdminData->role,
									'user_id'   => $AdminData->user_id,
									'user_name'   => $AdminData->user_name,
									'user_email'   => $AdminData->user_email,
									'menu'   => '',
									'sub_menu'   => '',
									'temp_menu'   => '',
									'temp_sub_menu'   => ''
									));							
							}

							redirect(base_url().'dashboard');
						}else
						{ 
							
							$this->session->set_flashdata('msg' ,'Email or Password Not Matched.');	
					  		redirect(base_url('login'));
						}
					}else
					{
						$this->session->set_flashdata('msg' ,'Your witcircle account has been suspended from administration.');	
					  	redirect(base_url('login'));
					}
				}
				else
				{
	
				  $this->session->set_flashdata('msg' ,'Email or Password Not Matched.');	
				  redirect(base_url('login'));	
				}	
		}
       $this->load->view('admin/login1');
	}
	
	// public function index()
	// { 
	// 	if(isset($_POST['submit']))
	// 	{   
	// 		     $username = $this->input->post ('username');
	// 			 $password = $this->input->post('password');
	// 			 //exit;
	// 			 $AdminData = $this->common_model->common_getRow('admin',array('user_email'=>$username));
	// 			 //print_r($AdminData);
	// 			 //exit;
	// 			if (!empty($AdminData)) 
	// 			{
	// 				if($AdminData->role == 'admin')
	// 				{
	// 					$pass = md5($password);
	// 				}else
	// 				{
	// 					$pass = sha1($password);
	// 				}
	// 				if($pass == $AdminData->user_password)
	// 				{

	// 					$this->session->set_userdata ( array (
	// 							'admin_id'   => $AdminData->user_id,
	// 							'role'   => $AdminData->role,
	// 							'user_id'   => $AdminData->user_id,
	// 							'user_name'   => $AdminData->user_name,
	// 							'user_email'   => $AdminData->user_email,
	// 							'menu'   => $AdminData->menu_id,
	// 							'sub_menu'   => $AdminData->submenu_id,
	// 							));

	// 						redirect(base_url().'dashboard');
	// 				}else
	// 				{
	// 					$this->session->set_flashdata('msg' ,'Email or Password Not Matched.');	
	// 			  		redirect(base_url('login'));
	// 				}
					
	// 			}
	// 			else
	// 			{
	// 			  $this->session->set_flashdata('msg' ,'Email or Password Not Matched.');	
	// 			  redirect(base_url('login'));	
	// 			}	
	// 	}
	// 	$this->load->view('admin/index');
	// }
	
}
