<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rank extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
	}

    public function index()
	{ 
		 $data['rank_predictions'] = $this->common_model->getData('rank_predictions',array(),'id','DESC');
         $this->load->view('admin/rank/list_rank',$data);
	}

	public function Add_rank()
	{		
	    if (isset($_POST['submit'])) 
	    {

	    	$arr = array(
            	'course_id'=>$this->input->post('course_id'),
            	'starting_no'=>$this->input->post('starting_number'),
            	'end_no'=>$this->input->post('end_number'),
            	'start_rank'=>$this->input->post('start_rank'),
            	'end_rank'=>$this->input->post('end_rank'),
            	'create_at'=>date("Y-m-d H:i:s"),	
            	);

		    $insertid = $this->common_model->common_insert('rank_predictions',$arr);
			 
			if($insertid == '' || $insertid == '0') {

			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('rank/add_rank'));
			}
			else
			{
               $this->session->set_flashdata('success' ,'Rnak added successfully');	
			   redirect(base_url('rank/add_rank'));
			}
	    }

	$this->load->view('admin/rank/add_rank');
    }

    public function edit_rank($id)
	{	
	   $id = $this->common_model->id_decrypt($id);

	    if (isset($_POST['submit'])) 
	    {
	    	$arr = array(
            	'course_id'=>$this->input->post('course_id'),
            	'starting_no'=>$this->input->post('starting_number'),
            	'end_no'=>$this->input->post('end_number'),
            	'start_rank'=>$this->input->post('start_rank'),
            	'end_rank'=>$this->input->post('end_rank'),
            	'create_at'=>date("Y-m-d H:i:s"),	
            	);

		    $updateid = $this->common_model->updateData('rank_predictions',$arr,array('id' =>$id));
			 
			if($updateid == '' || $updateid == '0') {

			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('rank'));
			}
			else
			{
               $this->session->set_flashdata('success' ,'Rank update successfully');	
			   redirect(base_url('rank'));
			}
	    }
    $data['rank_predictions'] = $this->common_model->common_getRow('rank_predictions',array('id'=>$id));
	$this->load->view('admin/rank/edit_rank',$data);
    }

    public function delete_record($id = false){
	$delete = $this->common_model->deleteData("rank_predictions",array('id'=>$id));
	    //print_r($delete);
	    //exit;
	if($delete)
	{ 
	   echo '1000';exit; 
	}
 }
}	