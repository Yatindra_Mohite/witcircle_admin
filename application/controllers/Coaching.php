<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coaching extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
		
	}

	public function index(){
		   $data['coaching_list'] = $this->db->query("select user.*,region.Region,city.City from user LEFT JOIN region ON region.RegionId= user.state LEFT JOIN city ON city.CityId=user.city where user.user_type=2")->result();
		   $this->load->view('admin/coaching/coaching_list', $data);

     
	 }

   

	public function change_status(){
		//echo "hihih";
		//exit;
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("user",array('admin_status'=>$status,'user_token'=>'','device_token'=>''),array('id'=>$user_id));
       
	    if($update)
	    {   
            $base = base_url('uploads/email-template/images/white-logo.png');
            $base2 = base_url('uploads/email-template/images/phone-call.png');
            $base3 = base_url('uploads/email-template/images/mail.png');
            
            $n = $this->common_model->common_getRow("user",array('id'=>$user_id));
            $name = $n->name;
            $email_id = $n->email_id;
            if ($n->admin_status == 1) 
            {
		    	require 'send/vendor/autoload.php';
				$request_body = json_decode('{
				  "personalizations": [
				    {
				      "to": [
				        {
				          "email": "'.$email_id.'"
				        }
				      ],
				      "subject": "Account activation",
				      "substitutions": {  
				      	                  "{name}": "'.$name.'",
					                      "{src}": "'.$base.'",
					                      "{phone_img}": "'.$base2.'",
					                      "{mail_img}": "'.$base3.'"
				                        }
				    }
				  ],
				  "from": {
				    "email": "noreply@witcircle.com"
				  },
				  
				  "template_id": "5a3964bd-6c05-4346-aab7-0713c89964ad",
				  "content": [
				    {
				      "type": "text/html",
				      "value": "and easy to do anywhere, even with PHP"
				    }
				  ]
				}');

				$apiKey = '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';
				$sg = new \SendGrid($apiKey);

				$response = $sg->client->mail()->send()->post($request_body);
				$response->statusCode();
				$response->body();
				$response->headers();
		    }
		    
	        echo '1000';exit; 
	    }
    }

    public function edit($id = false){
     $id = $this->common_model->id_decrypt($id);
		if(isset($_POST['submit']))
		{   
		     	$s = $this->input->post ('section_name');
			    
			     $s1 = $this->input->post ('positive_mark');
			    

			     $s2 = $this->input->post ('partial_mark');
			    

			     $s3 = $this->input->post ('negative_mark');

			$updateid = $this->common_model->updateData('marking',array('name'=>$s,'positive_mark'=>$s1,'partial_mark'=>$s2,'negative_mark'=>$s3),array('id'=>$id));
						if($updateid == '' || $updateid == '0') {
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    	redirect(base_url('Marking_scheme'));
			}
			else
			{
        	    $this->session->set_flashdata('success' ,'Scheme updated successfully');	
			   redirect(base_url('Marking_scheme/scheme_list'));
			}	
		}

    $data['scheme_edit'] = $this->common_model->common_getRow('marking',array('id'=>$id));
    $this->load->view('admin/marking_scheme/edit_scheme', $data);
	 }

   public function add_question($id = false){
    
    $id = $this->common_model->id_decrypt($id);

    $data['que_active'] = $this->common_model->common_getRow('musp',array('id'=>$id));
    //print_r($data);
    //exit;

    $this->load->view('admin/musp/add_question',$data);
   
   }
}