<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_code extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
    	if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
		
	}	
   
	public function add_promo_code(){

   if(isset($_POST['submit']))
	 { 
      $a = strtoupper($this->input->post ('promo_code'));
      $promo = $this->common_model->getData('promo_code',array('promo_code_name'=>$a),'id','DESC');

        if( count($promo) > 0){
            $this->session->set_flashdata('failed' ,'This promo code is alredy added in your list.');  
            redirect(base_url('promo_code/add_promo_code'));
        }{

                $currentdate = date("Y-m-d H:i:s");
                if($this->input->post ('type_id') == 2){
                
                    $coin = $this->input->post ('coin');
                    $sub = '';
                }else{
                    $sub = $this->input->post ('subscription_id');
                    $coin = '';
                }

                if($this->input->post ('coaching_name') == ''){
                    $coaching = 0;
                }else{
                	 $a = $this->input->post ('coaching_name');
                	 $coaching  =  implode(',', $a);
                	
                }

                $arr  = array(
                      'title'=>$this->input->post ('title'),
                      'promo_code_name'=> strtoupper($this->input->post ('promo_code')),
                      'usage_limit'=>$this->input->post('usage_limit'),
                      'curr_limit'=>$this->input->post('usage_limit'),
                      'apply_on'=>$this->input->post ('type_id'),
                      'coaching_id'=>$coaching,
                      'coin'=>$coin,
                      'subscription_id'=>$sub,
                      'description'=>$this->input->post ('desc'),
                      'start_date'=>$this->input->post ('start_date'),
                      'end_date'=>$this->input->post ('end_date'),
                      'status'=>0,
                      'create_at'=>$currentdate,

                	);

     			$insertid = $this->common_model->common_insert('promo_code',$arr);
	           
			    if($insertid == '' || $insertid == '0') {
			 	
			 		    $this->session->set_flashdata('failed' ,'Something went wrong');	
			    	  redirect(base_url('promo_code/add_promo_code'));
  				}
  				else
  				{
  					//$this->db->query("UPDATE global_counters SET subject_count = subject_count + 1 WHERE id = 1");

  		        $this->session->set_flashdata('success' ,'Promo code added successfully');	
  				    redirect(base_url('promo_code/add_promo_code'));
  				}
        }  
		}		
    	$this->load->view('admin/promo_code/promo_code');
	 }
    
    public function promo_code_list(){

		$data['promo_code'] = $this->common_model->getData('promo_code',array(),'id','DESC');
				 
       $this->load->view('admin/promo_code/promo_code_list', $data);
	  }

    public function delete($id = false){
        
        $delete = $this->common_model->deleteData("promo_code",array('id'=>$id));
      if($delete)
      {
           echo '1000';exit; 
      }

    }

    public function promocode_change_status(){
    //echo "hihih";
    //exit;
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
        
        $update = $this->common_model->updateData("promo_code",array('status'=>$status),array('id'=>$user_id));
        if($update)
      {
          echo 1000;exit; 
      }
    }

   public function edit_promo_code($id = false){
   
   $data['user_limit'] = $this->common_model->common_getRow('promo_code',array('id'=>$id));
   //echo $data['user_limit']->curr_limit;
   //exit; 

   if(isset($_POST['submit']))
   { 
        $user_limit = $this->input->post ('usage_limit');

        if( $user_limit < $data['user_limit']->curr_limit ){
            $this->session->set_flashdata('error' ,'User limit is over');  
            redirect(base_url('promo_code/promo_code_list'));
        } 

        $currentdate = date("Y-m-d H:i:s");
                if($this->input->post ('type_id') == 2){
                
                    $coin = $this->input->post ('coin');
                    $sub = '';
                }else{
                    $sub = $this->input->post ('subscription_id');
                    $coin = '';
                }

                if($this->input->post ('coaching_name') == ''){
                    $coaching = 0;
                }else{
                    $a = $this->input->post ('coaching_name');
                    $coaching  =  implode(',', $a);
                  
                }

                if ( $this->input->post ('all_user') == 'ha' ) {
                  $coaching = 0;
                }

                $arr  = array(
                      'title'=>$this->input->post ('title'),
                      'promo_code_name'=>$this->input->post ('promo_code'),
                      'usage_limit'=>$this->input->post ('usage_limit'),
                      'apply_on'=>$this->input->post ('type_id'),
                      'coaching_id'=>$coaching,
                      'coin'=>$coin,
                      'subscription_id'=>$sub,
                      'description'=>$this->input->post ('desc'),
                      'start_date'=>$this->input->post ('start_date'),
                      'end_date'=>$this->input->post ('end_date'),
                      'status'=>0,
                      'create_at'=>$currentdate,

                  );

          $insertid = $this->common_model->updateData('promo_code',$arr,array('id'=>$id));
             
          if($insertid == '' || $insertid == '0') {
        
          $this->session->set_flashdata('failed' ,'Something went wrong');  
            redirect(base_url('promo_code/promo_code_list'));
        }
        else
        {
          //$this->db->query("UPDATE global_counters SET subject_count = subject_count + 1 WHERE id = 1");

                $this->session->set_flashdata('success' ,'Promo code updated successfully'); 
            redirect(base_url('promo_code/promo_code_list'));
        }
    }   
      $data['promo_code'] = $this->common_model->common_getRow('promo_code',array('id'=>$id));

      $this->load->view('admin/promo_code/edit_promo_code',$data);
   }
}