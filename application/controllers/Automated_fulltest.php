<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Automated_fulltest extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
			$currentdate = date("Y-m-d H:i:s");
			define('currentdate', $currentdate);
			$militime=round(microtime(true) * 1000);
			define('militime', $militime);
		
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
		
	}

	public function add_automated_full_test()
	{   
		if(isset($_POST['submit']))
		{   
			$i=0;
			$course = $this->input->post('course_id');
			$subject = $this->input->post('subject_id');
			$no_of_question = $this->input->post('no_of_question');
			$no_of_question_course = $this->input->post('no_of_question_course');
			$sub_time = $this->input->post('subject_time');
            //print_r($sub_time);
            //print_r($subject);
            //exit;
            // if ($sub_time == '') {
            // 	$sub = '';
            // }else{
            //     $sub = $sub_time[$i];
            // }

            $check = $this->common_model->getData('automated_course',array('course_id'=>$course));
            if( count($check) > 0){

                $this->session->set_flashdata('failed' ,'This Course is alraedy added in your list.');	
			    redirect(base_url('Automated_fulltest/add_automated_full_test'));
            }else
            {   
            	$arr2 = array(
						    'course_id'=>$course,
						    'no_of_question'=>$no_of_question_course,
						   );
                $insertid = $this->common_model->common_insert('automated_course',$arr2);

				$a = $subject;

				for ($i=0;  $i < count($a); $i++) { 
				    
				    $arr = array(
						    'course_id'=>$course,
						    'subject_id'=>$a[$i],
						    'no_of_question'=>$no_of_question[$i],
						    'subject_time'=>$sub_time[$i],
						   );

					$insertid = $this->common_model->common_insert('automated_subject',$arr);

				}
				
			    if($insertid == '' || $insertid == '0') {
			 	
			 	   $this->session->set_flashdata('failed' ,'Something went wrong');	
			       redirect(base_url('automated_fulltest/add_automated_full_test'));
			    }
			    else
			    {
                   $this->session->set_flashdata('success' ,'automated Test added successfully');	
			       redirect(base_url('automated_fulltest/add_automated_full_test'));
			    }
			}	 			
		}
        $this->load->view('admin/automated_fulltest/add_automated_full_test');
	}
    
    // public function getsubject()
    // {
    //       $sub = $this->db->query("SELECT * FROM `course_subject` WHERE FIND_IN_SET($course_id,course_id)")->result();
    // }



    public function get_subject()
	{
		$course_id = $this->input->post('course_id');
		$subject = $this->db->query("SELECT * FROM `course_subject` WHERE FIND_IN_SET($course_id,course_id)")->result();
        $i = 0;
		foreach ($subject as $key) {
          $i++;
            ?>
		 	<span class="areaCheckbox"><input type="checkbox" name="subject_id[]" class="new_class from-control <?php echo "check".$i?>" required value="<?php echo $key->id;?>"> <?php echo $key->subject_name;?></span><span class="noq"><input type="number" placeholder="number of question" name="no_of_question[]" class=" from-control <?php echo "check".$i?> <?php echo "check_total".$i?>" ></span><span class="subTime"><input type="number" min="1" placeholder="Subject time" name="subject_time[]" class="from-control check_req <?php echo "req".$i?>" ></span>
		<?php }
	}

	public function automated_test_list(){
		
	    $data['auomated_test'] = $this->common_model->getData('automated_course',array(),'id','DESC');		
		//$data['auomated_test'] = $this->db->query("SELECT distinct course_id, subject_id FROM automated_course_and_subject")->result();
		//print_r($data);
		//exit;
       
       $this->load->view('admin/automated_fulltest/automated_full_test_list', $data);
	}

	public function automated_subject_list($id = false){
		
	    $data['auomated_test'] = $this->common_model->getData('automated_subject',array('course_id'=>$id));		
		//$data['auomated_test'] = $this->db->query("SELECT distinct course_id, subject_id FROM automated_course_and_subject")->result();
		//print_r($data);
		//exit;
       
       $this->load->view('admin/automated_fulltest/automated_subject_list', $data);
	}

	public function add_section(){

       if(isset($_POST['submit']))
	   { 
	        $section_name = $this->input->post('section_name');
			$marking = $this->input->post('marking');
			$question_type = $this->input->post('question_type');
			$section_time = $this->input->post('section_time');
			if($section_time == '') {
				$section_time = '';
			}
			
			$starting_number = $this->input->post('starting_number');
			$no_of_question = $this->input->post('no_of_question');
			$end_number = $this->input->post('end_number');
			$course_id = $this->input->post('course_id');
			$subject_id = $this->input->post('subject_id');

        	$question_type_name = $this->common_model->common_getRow('question_type',array('id'=>$question_type));

        
        $check = $this->common_model->getData('automated_section',array('course_id'=>$course_id,'subject_id'=>$subject_id,'section_name'=>$section_name));
        if( count($check) > 0){
               
               $this->session->set_flashdata('failed' ,'This section name is alraedy exists');
		       redirect(base_url('Automated_fulltest/add_section/'.$course_id.'/'.$subject_id));

        }else
        {   

            if ($question_type == 10) { ////// thisi only for COMPREHENSION
            	//echo "SELECT count(DISTINCT id ) as com_count FROM questions WHERE subject_id = '".$subject_id."' AND question_type = '".$question_type."' group by comprehensive_id ";
            	//exit;
            	$type_check2 = $this->db->query("SELECT count(DISTINCT id ) as com_count FROM questions WHERE subject_id = '".$subject_id."' AND question_type = '".$question_type."' group by comprehensive_id ")->result();
            	//$no_of_question = 4;
            	//print_r($type_check2[0]->com_count);
            	//exit;
            	foreach ($type_check2 as $key) {
                   if ($key->com_count == $no_of_question) {
                   	  $found = 1;
                   }
            	}
                if ($found != 1) {
                	$this->session->set_flashdata('failed' ,'Insufficient no. of question in ( '.ucfirst($question_type_name->name).' ).');	
					redirect(base_url('automated_fulltest/add_section/'.$course_id.'/'.$subject_id));
                }

            }else{ //// thib si only for multiple,single,interger
            	
            	$type_check = $this->db->query("SELECT count(id) as ques_count FROM questions WHERE subject_id = '".$subject_id."' AND question_type = '".$question_type."' ")->result();
            	//$type_check[0]->ques_count;
            	//exit;
                //echo "$type_check[0]->ques_count <= $no_of_question";
                //exit;
            	if ($type_check[0]->ques_count <= $no_of_question) {
            			$this->session->set_flashdata('failed' ,'Insufficient no. of question in ( '.ucfirst($question_type_name->name).' ).');	
						redirect(base_url('automated_fulltest/add_section/'.$course_id.'/'.$subject_id));
            		}	
                
            } 
            //exit;

            $arr = array(
            	        'course_id'=>$course_id,
			            'subject_id'=>$subject_id,
					    'section_name'=>$section_name,
					    'marking'=>$marking,
					    'question_type'=>$question_type,
					    'no_of_question'=>$no_of_question,
					    'section_time'=>$section_time,
					    'start_to'=>$starting_number,
					    'end_to'=>$end_number,
					    'created_at'=>date('Y-m-d h:i:s'),
					   );

				    $insertid = $this->common_model->common_insert('automated_section',$arr);
					 
					 if($insertid == '' || $insertid == '0') {
					 	
					 	$this->session->set_flashdata('failed' ,'Something went wrong');	
					    redirect(base_url('automated_fulltest/add_section'.$course_id.'/'.$subject_id));
					 }
					 else
					 {
	                    $this->session->set_flashdata('success' ,'Section added successfully');	
					    redirect(base_url('automated_fulltest/add_section/'.$course_id.'/'.$subject_id));
					 }
        }

	   }	   
       $this->load->view('admin/automated_fulltest/add_section');
	}

	// public function add_number_of_question_for_subject(){
    //       if(isset($_POST['submit']))
	//    { 
	//    }
	// }


	
	function automated_test_generate()
	{   
	    $courseid = $this->input->post('course');
        $this->db->join("course","course.id = automated_course.course_id");
        $sele_course = $this->db->select('automated_course.id as autoid,course.course_name,automated_course.course_id,automated_course.no_of_question')->get_where("automated_course",array('automated_course.course_id'=>$courseid,'automated_course.status'=>1))->row();
        if(!empty($sele_course))
        {
            $arr = $auto  = $total_question =array();  $status = '';
            $test_name = $sele_course->course_name.'_'.$this->common_model->randomuniqueCode();
            $sel = $this->db->query("SELECT automated_subject.*,course_subject.title as subject_name FROM automated_subject INNER JOIN course_subject ON automated_subject.subject_id = course_subject.id WHERE automated_subject.course_id = $sele_course->course_id")->result(); //status column not in table

            if(!empty($sel))
            {
                foreach ($sel as $key) {
                    $q_id = $totalsubjmark =0; $not_in = ''; $section_arr = $que_automated  = array(); 
                    $section = $this->db->query("SELECT automated_section.*,marking.positive_mark,marking.partial_mark,marking.negative_mark FROM automated_section LEFT JOIN marking ON automated_section.marking = marking.id WHERE automated_section.course_id = $sele_course->course_id AND automated_section.subject_id = ".$key->subject_id." ORDER BY automated_section.start_to ASC")->result();
                    
                    if(!empty($section))
                    {
                        foreach ($section as $keyvalue) {
                            
                            $section_arr[] = array('section_name'=>$keyvalue->section_name,'sectional_times'=>$keyvalue->section_time,'marking_scheme'=>$keyvalue->marking);
                            if($keyvalue->question_type!=10)
                            {
                                if(!empty($q_id))
                                {
                                    $not_in = "AND id NOT IN ($q_id)";
                                }
                                 
                                // $question = $this->db->query("SELECT substring_index(group_concat(id SEPARATOR ','), ',', ".$keyvalue->no_of_question.") as questions_str FROM questions WHERE subject_id = ".$keyvalue->subject_id." AND question_type = ".$keyvalue->question_type." ".$not_in."  ORDER BY rand()")->result();
                                $question = $this->db->query("Select GROUP_CONCAT(id SEPARATOR ',') as questions_str from (SELECT id FROM questions WHERE subject_id = ".$keyvalue->subject_id." AND question_type = ".$keyvalue->question_type." ".$not_in." AND questions.status = 1 ORDER BY RAND() limit ".$keyvalue->no_of_question." ) tbl")->result();
                                if(isset($question[0]->questions_str) && !empty($question[0]->questions_str) )
                                {
                                    foreach ($question as $question_value) {
                                        
                                        $exp = explode(',', $question_value->questions_str);

                                        for ($i=0; $i < count($exp) ; $i++) { 
                                            $markingsel = $this->db->query("SELECT id,positive_mark,partial_mark,negative_mark FROM marking WHERE id = ".$keyvalue->marking."")->row();
                                            if(!empty($markingsel))
                                            {
                                                $totalsubjmark += $markingsel->positive_mark;
                                            }
                                            $marking_string = "Positive - ".$keyvalue->positive_mark.", Negative - ".$keyvalue->negative_mark.", Partial - ".$keyvalue->partial_mark."";
                                            $que_automated[] = array('id'=>$exp[$i],'marking_scheme'=>$marking_string,"marking_positive"=>$keyvalue->positive_mark,"marking_negative"=>$keyvalue->negative_mark,"marking_partial"=>$keyvalue->partial_mark);
                                        }
                                        if($q_id!=0 && !empty($q_id)) //check quest type 
                                        {
                                            $q_id = $question_value->questions_str.','.$q_id; 
                                        }else
                                        {
                                            $q_id = $question_value->questions_str;
                                        }
                                    }
                                }else
                                {
                                    //$this->session->set_flashdata('failed' ,'Insufficient questions.');
                                    $status = 1000; //INSUFFICENT QUESTION
                                }
                            }else
                            {
                                // $question = $this->db->query("SELECT group_concat(questions.id) as questions_str FROM questions INNER JOIN questions_comprehensive as qc ON questions.comprehensive_id = qc.id WHERE questions.subject_id = ".$keyvalue->subject_id." AND questions.question_type = ".$keyvalue->question_type." AND qc.no_of_question = ".$keyvalue->no_of_question." ORDER BY RAND()")->result();
                                //$question = $this->db->query("SELECT group_concat(tbl.id) as questions_str FROM (SELECT questions.id FROM questions INNER JOIN questions_comprehensive as qc ON questions.comprehensive_id = qc.id WHERE questions.subject_id = ".$keyvalue->subject_id." AND questions.question_type = ".$keyvalue->question_type." AND qc.no_of_question = ".$keyvalue->no_of_question." ORDER BY RAND() limit ".$keyvalue->no_of_question." ) tbl")->result();
                                $question = $this->db->query("SELECT substring_index(group_concat(questions.id order by rand()) , ',', ".$keyvalue->no_of_question.") as questions_str FROM questions INNER JOIN questions_comprehensive ON questions.comprehensive_id = questions_comprehensive.id WHERE questions.subject_id = ".$keyvalue->subject_id." AND questions.question_type = ".$keyvalue->question_type."  AND questions_comprehensive.no_of_question = ".$keyvalue->no_of_question." AND  questions.status = 1 AND questions_comprehensive.status = 1")->result();
                                if(isset($question[0]->questions_str) && !empty($question[0]->questions_str) )
                                {
                                    foreach ($question as $question_value) {
                                        //$arr[] = $question_value;
                                        if($q_id!=0) //check quest type 
                                        {
                                            $q_id = $question_value->questions_str.','.$q_id; 
                                        }else
                                        {
                                            $q_id = $question_value->questions_str;
                                        }

                                        $exp1 = explode(',', $question_value->questions_str);
                                        for ($i=0; $i < count($exp1) ; $i++) { 
                                            $markingsel = $this->db->query("SELECT id,positive_mark,partial_mark,negative_mark FROM marking WHERE id = ".$keyvalue->marking."")->row();
                                            if(!empty($markingsel))
                                            {
                                                $totalsubjmark += $markingsel->positive_mark;
                                            }
                                            $marking_string = "Positive - ".$keyvalue->positive_mark.", Negative - ".$keyvalue->negative_mark.", Partial - ".$keyvalue->partial_mark."";
                                            $que_automated[] = array('id'=>$exp1[$i],'marking_scheme'=>$marking_string,"marking_positive"=>$keyvalue->positive_mark,"marking_negative"=>$keyvalue->negative_mark,"marking_partial"=>$keyvalue->partial_mark);
                                        }
                                    }
                                }else
                                {
                                  //$this->session->set_flashdata('failed' ,'Insufficient questions.');
                                    $status = 1000;  //INSUFFICENT QUESTION
                                }
                            } 
                        }
                    }else
                    {
                         $status = 2000; //SECTION NOT FOUND
                    }
                    $total_question[] = $q_id;
                    $sub_id[] = array('subject_id'=>$key->subject_id,'section'=>$section_arr);    
                    $quearr[] = array('subject_id'=>$key->subject_id,'subject_name'=>$key->subject_name,'subject_total_mark'=>$totalsubjmark,'question'=>explode(',',$q_id),'sectional_time'=>$key->subject_time);

                    $nee[] = $que_automated;
                
                }
                if($status == '')
                {
                    $markingarr =array();
                    for ($i=0; $i < count($nee) ; $i++) { 
                        foreach ($nee[$i] as $value) {
                            $markingarr[] = $value;
                        }
                    }
                    $impquest = implode(',', $total_question);
                    $quescount = substr_count($impquest,",")+1;
                    if($quescount==$sele_course->no_of_question)
                    {
    	                $arr = array(
    	                    'added_by'=>'admin',
    	                    'added_id'=>'1',
                            'type'=>'automated',
    	                    'test_name'=>$test_name,
    	                    'course_id'=>$courseid,
    	                    'subject_id'=>json_encode($sub_id),
    	                    'questions_id'=>$impquest,
    	                    'subject_pattern'=>json_encode(array('que'=>$quearr)),
                            'automated_question_pattern'=>json_encode($markingarr),
    	                    'no_of_question'=>$sele_course->no_of_question,
    	                    'token'=>md5($this->common_model->uniquetoken().$sele_course->autoid.time()),
    	                    'create_at'=>date('Y-m-d H:i:s')
    	                    );
                    
    	                $insert = $this->common_model->common_insert("musp",$arr);
    					if($insert!=false)
    					{
    						echo 200; 
    					}else
    					{
    						echo 201;
    					}
                    }else
                    {
                    	echo 3000;  //Question not match with total no. of questions
                    }
                }else
                {
                    echo $status;
                }
            }else
            {
                echo 4000;  //Subject not found
            }
        }else
        {
            echo 5000;  //course not found

        }
   		//create by YM 07-06-18
	}

    // function automated_test_generate()
    // {
    //     $courseid = $this->input->post('course');
    //     $this->db->join("course","course.id = automated_course.course_id");
    //     $sele_course = $this->db->select('automated_course.id as autoid,course.course_name,automated_course.course_id,automated_course.no_of_question')->get_where("automated_course",array('automated_course.course_id'=>$courseid,'automated_course.status'=>1))->row();
    //     if(!empty($sele_course))
    //     {
    //         $arr = $total_question =array(); 
    //         $test_name = $sele_course->course_name.'_'.$this->common_model->randomuniqueCode();
    //         $sel = $this->db->query("SELECT automated_subject.*,course_subject.title as subject_name FROM automated_subject INNER JOIN course_subject ON automated_subject.subject_id = course_subject.id WHERE automated_subject.course_id = $sele_course->course_id")->result(); //status column not in table
    //         if(!empty($sel))
    //         {
    //             foreach ($sel as $key) {
    //                 $q_id = $totalsubjmark =0; $not_in = ''; $section_arr = array(); 
    //                 $section = $this->db->query("SELECT * FROM automated_section WHERE course_id = $sele_course->course_id AND subject_id = ".$key->subject_id." ORDER BY start_to ASC")->result();
    //                 if(!empty($section))
    //                 {
    //                     foreach ($section as $keyvalue) {
                            
    //                         $section_arr[] = array('section_name'=>$keyvalue->section_name,'sectional_times'=>$keyvalue->section_time,'marking_scheme'=>$keyvalue->marking);
    //                         if($keyvalue->question_type!=10)
    //                         {
    //                             if(!empty($q_id))
    //                             {
    //                                 $not_in = "AND id NOT IN ($q_id)";
    //                             }
    //                             // $question = $this->db->query("SELECT substring_index(group_concat(id SEPARATOR ','), ',', ".$keyvalue->no_of_question.") as questions_str FROM questions WHERE subject_id = ".$keyvalue->subject_id." AND question_type = ".$keyvalue->question_type." ".$not_in."  ORDER BY rand()")->result();
    //                             $question = $this->db->query("Select GROUP_CONCAT(id SEPARATOR ',') as questions_str from (SELECT id FROM questions WHERE subject_id = ".$keyvalue->subject_id." AND question_type = ".$keyvalue->question_type." ".$not_in." ORDER BY RAND() limit ".$keyvalue->no_of_question.") tbl")->result();
    //                             if(!empty($question))
    //                             {
    //                                 foreach ($question as $question_value) {
                                        
    //                                     $exp = explode(',', $question_value->questions_str);

    //                                     for ($i=0; $i < count($exp) ; $i++) { 
    //                                         $markingsel = $this->db->query("SELECT id,positive_mark FROM marking WHERE id = ".$keyvalue->marking."")->row();
    //                                         if(!empty($markingsel))
    //                                         {
    //                                             $totalsubjmark += $markingsel->positive_mark;
    //                                         }
    //                                     }
    //                                     if($q_id!=0) //check quest type 
    //                                     {
    //                                         $q_id = $question_value->questions_str.','.$q_id; 
    //                                     }else
    //                                     {
    //                                         $q_id = $question_value->questions_str;
    //                                     }
    //                                 }
    //                             }else
    //                             {
    //                                 //$this->session->set_flashdata('failed' ,'Insufficient questions.');
    //                                 echo 1000; exit; //INSUFFICENT QUESTION
    //                             }
    //                         }else
    //                         {
    //                             // $question = $this->db->query("SELECT group_concat(questions.id) as questions_str FROM questions INNER JOIN questions_comprehensive as qc ON questions.comprehensive_id = qc.id WHERE questions.subject_id = ".$keyvalue->subject_id." AND questions.question_type = ".$keyvalue->question_type." AND qc.no_of_question = ".$keyvalue->no_of_question." ORDER BY RAND()")->result();
    //                             $question = $this->db->query("SELECT group_concat(tbl.id) as questions_str FROM (SELECT questions.id FROM questions INNER JOIN questions_comprehensive as qc ON questions.comprehensive_id = qc.id WHERE questions.subject_id = ".$keyvalue->subject_id." AND questions.question_type = ".$keyvalue->question_type." AND qc.no_of_question = ".$keyvalue->no_of_question." ORDER BY RAND() limit ".$keyvalue->no_of_question." ) tbl")->result();
    //                             if(!empty($question))
    //                             {
    //                                 foreach ($question as $question_value) {
    //                                     //$arr[] = $question_value;
    //                                     if($q_id!=0) //check quest type 
    //                                     {
    //                                         $q_id = $question_value->questions_str.','.$q_id; 
    //                                     }else
    //                                     {
    //                                         $q_id = $question_value->questions_str;
    //                                     }
    //                                     $exp1 = explode(',', $question_value->questions_str);
    //                                     for ($i=0; $i < count($exp1) ; $i++) { 
    //                                         $markingsel = $this->db->query("SELECT id,positive_mark FROM marking WHERE id = ".$keyvalue->marking."")->row();
    //                                         if(!empty($markingsel))
    //                                         {
    //                                             $totalsubjmark += $markingsel->positive_mark;
    //                                         }
    //                                     }
    //                                 }
    //                             }else
    //                             {
    //                                 //$this->session->set_flashdata('failed' ,'Insufficient questions.');
    //                                 echo 1000; exit; //INSUFFICENT QUESTION
    //                             }
    //                         } 
    //                     }
    //                 }else
    //                 {
    //                     echo 2000; //SECTION NOT FOUND
    //                 }
    //                 $total_question[] = $q_id;
    //                 $sub_id[] = array('subject_id'=>$key->subject_id,'section'=>$section_arr);    
    //                 $quearr[] = array('subject_id'=>$key->subject_id,'subject_name'=>$key->subject_name,'subject_total_mark'=>$totalsubjmark,'question'=>explode(',',$q_id));
    //             }
    //             $impquest = implode(',', $total_question);
    //             $quescount = substr_count($impquest,",")+1;
    //             if($quescount==$sele_course->no_of_question)
    //             {
    //                 $arr = array(
    //                     'added_by'=>'admin',
    //                     'added_id'=>'1',
    //                     'type'=>'automated',
    //                     'test_name'=>$test_name,
    //                     'course_id'=>$courseid,
    //                     'subject_id'=>json_encode($sub_id),
    //                     'questions_id'=>$impquest,
    //                     'subject_pattern'=>json_encode(array('que'=>$quearr)),
    //                     'no_of_question'=>$sele_course->no_of_question,
    //                     'token'=>md5($this->common_model->uniquetoken().$sele_course->autoid.time()),
    //                     'create_at'=>date('Y-m-d H:i:s')
    //                     );
                    
    //                 $insert = $this->common_model->common_insert("musp",$arr);
    //                 if($insert!=false)
    //                 {
    //                     echo 200; 
    //                 }else
    //                 {
    //                     echo 201;
    //                 }
    //             }else
    //             {
    //                 echo 3000;  //Question not match with total no. of questions
    //             }
    //         }else
    //         {
    //             echo 4000;  //Subject not found
    //         }
    //     }else
    //     {
    //         echo 5000;  //course not found

    //     }
    //     //create by YM 07-06-18
    // }

    public function change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("automated_course",array('status'=>$status),array('id'=>$user_id));

          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function generated_test_list(){
        
        $data['generated_test_list'] = $this->common_model->getData('musp',array('type'=>'automated'),'id','DESC');     
        //$data['auomated_test'] = $this->db->query("SELECT distinct course_id, subject_id FROM automated_course_and_subject")->result();
        //print_r($data);
        //exit;
       
        $this->load->view('admin/automated_fulltest/generated_test_list', $data);
    }

    public function question_list($id = false){
        
        $data['generated_test_list'] = $this->common_model->common_getRow('musp',array('id'=>$id));
        //print_r($data);
        //exit;     
        $this->load->view('admin/automated_fulltest/question_list', $data);
    }

    public function update_test(){

       if(isset($_POST['submit']))
       {    
         $id = $this->input->post('test_id');
         $o_date = $this->input->post('o_date');
         $desc = $this->input->post('desc');
         $name = $this->input->post('name');

            $arr = array('organised_date'=>$o_date,
                         'test_desc'=>$desc,
                         'test_name'=>$name,
                        );

           $updateid = $this->common_model->updateData('musp',$arr,array('id'=>$id));
            
            if($updateid == '' || $updateid == '0') {
                $this->session->set_flashdata('failed' ,'Something went wrong');    
                redirect(base_url('automated_fulltest/generated_test_list'));
            }
            else
            {
            $this->session->set_flashdata('success' ,'test updated successfully'); 
               redirect(base_url('automated_fulltest/generated_test_list'));
            }
       }
    }

    public function get_course_time(){
        $id = $this->input->post('course_id');
        $update = $this->common_model->common_getRow("course",array('id'=>$id));
        //echo $update->total_course_time;

          if($update)
          {
             echo $update->total_course_time; exit; 
          }
    }

}