<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('download');

	}

	private $encryptKey = 'W1^t@c96vLoP4a3x';
    private $iv = 'mP6hg0934bvk$#z5';
    private $blocksize = 16;
    
	public function authController($token='',$type='',$file_name=''){

		if(isset($_SERVER["HTTP_REFERER"])){
			$referer =  $_SERVER["HTTP_REFERER"];
			$parsed_url = parse_url($referer);
			if($this->checkHost($parsed_url['host'])==1){
					//$domain = str_ireplace('www.', '', parse_url($referer, PHP_URL_HOST));
					//echo $domain;
					if($token!='' && ($type!='' && ($type==1 || $type==3 || $type==4 || $type==6)) && $file_name!=''){
					$query = $this->db->get_where('user', array('user_token' => $token,'user_type'=>1,'user_status'=>1)); 
					//only students can download these files
						if ($query->num_rows() > 0)
						{
							
							if($type==1){  

											//for brochure	
											$pth    =   file_get_contents(base_url()."uploads/brochure/".$file_name);
			                                $nme    =   $file_name;
			                               force_download($nme, $pth);

							}else if($type==3){
											//for subjective exam paper	
											$file_name = $this->decrypt($file_name); 
											$pth    =   file_get_contents(base_url()."uploads/subjective/exam_paper/".$file_name);
				                            $nme    =   $file_name;
				                            force_download($nme, $pth);
				                            echo "Exam paper downloaded successfully. Please close the window.";	
							}	
							else if($type==4){
											//for subjective content	
											$file_name = $this->decrypt($file_name); 
											$pth    =   file_get_contents(base_url()."uploads/subjective/content/".$file_name);
				                            $nme    =   $file_name;
				                            force_download($nme, $pth);
							}					                     					
							else if($type==6){
											//for coaching content
								          	$file_name = $this->decrypt($file_name); 
											$pth    =   file_get_contents(base_url()."uploads/content_file/".$file_name);
				                            $nme    =   $file_name;
				                            force_download($nme, $pth);
							}


					
						}
					
					}
				}
	}else{
		echo "Expired or broken link";
	}

}



 public function checkHost($host){
  $valid_host = array('localhost','witcircle.com','http://witcircle.com','http://witcircle.com/','com.witcircle','13.126.96.162','13.127.112.245');
	if(in_array($host, $valid_host)){
		return true;
	}else {
		return false;
	}
 }	
  
 private function unpad($str, $blocksize)
    {
        $len = mb_strlen($str);
        $pad = ord( $str[$len - 1] );
        if ($pad && $pad < $blocksize) {
            $pm = preg_match('/' . chr($pad) . '{' . $pad . '}$/', $str);
            if( $pm ) {
                return mb_substr($str, 0, $len - $pad);
            }
        }
     	   return $str;
    }


 private function decrypt($data)
    {
        return $this->unpad(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, 
            $this->encryptKey, 
            hex2bin($data),
            MCRYPT_MODE_CBC, $this->iv), $this->blocksize);
    }
	
}