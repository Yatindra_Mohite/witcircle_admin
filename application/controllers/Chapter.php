<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chapter extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
	}

	public function index(){
       
     $currentdate = date("Y-m-d H:i:s");

		if(isset($_POST['submit']))
		{   
		    $course_id =0;
		    $subject_id =$this->input->post ('subject_id');
		    $chapter_name = strtolower($this->input->post ('chapter_name'));
			 
			$check = $this->common_model->getData('chapter',array('chapter_name'=>$chapter_name,'subject_id'=>$subject_id));
            if( count($check) > 0){

                $this->session->set_flashdata('failed' ,'This chapter is alredy added in your list.');	
			    redirect(base_url('chapter'));
            }
            else
            {
            	$insertid = $this->common_model->common_insert('chapter',array('course_id'=>$course_id,'subject_id'=>$subject_id,'chapter_name'=>$chapter_name,'create_at'=>$currentdate));
			 
				if($insertid == '' || $insertid == '0')
				{
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('course'));
				}
				else
				{   
                    $this->db->query("UPDATE global_counters SET chapter_count = chapter_count + 1 WHERE id = 1");

	                $this->session->set_flashdata('success' ,'Chapter added successfully');	
				    redirect(base_url('chapter/chapter_list'));
				}
            }		
		}
		$data['subjects'] = $this->db->query("select * from course_subject")->result();		
    	$this->load->view('admin/chapter/add_chapter',$data);
	 }

	 public function get_subject()
	{
	  $course_id = $this->input->post('course_id');

	  $sub = $sub = $this->db->query("SELECT * FROM `course_subject` WHERE `course_id` LIKE '%-$course_id,%'")->result();

	  if (empty($sub)) {?>
		  <option value="" selected="selected">No subject added for this course</option>
		   <?php }else{?>
	      <option value="" selected="selected">Select Subject</option>
	  <?php }
	  
	  foreach ($sub as $key) 
	  { 
	  ?>
	    <option value="<?php echo $key->id; ?>"><?php echo $key->subject_name;?></option>
	   <?php
	  }
	}
    
    public function chapter_list(){

		$data['chapter_list'] = $this->common_model->getData('chapter',array(),'id','DESC');
				 
       $this->load->view('admin/chapter/chapter_list', $data);
	}

	public function change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("chapter",array('status'=>$status),array('id'=>$user_id));
        
          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit($id = false){
       
     $id = $this->common_model->id_decrypt($id);

		if(isset($_POST['submit']))
		{   
			     $course_id =0;
			     $subject_id =$this->input->post ('subject_id');
			     $chapter_name = strtolower($this->input->post ('chapter_name'));
				 
				 $updateid = $this->common_model->updateData('chapter',array('course_id'=>$course_id,'subject_id'=>$subject_id,'chapter_name'=>$chapter_name),array('id'=>$id));
				 
				 if($updateid == '' || $updateid == '0') {
				 	
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('course/chapter_list'));
				 }
				 else
				 {
                    $this->session->set_flashdata('success' ,'Chapter added successfully');	
				    redirect(base_url('chapter/chapter_list'));
				 }	
		}
		
    $data['chapter_edit'] = $this->common_model->common_getRow('chapter',array('id'=>$id));

    $this->load->view('admin/chapter/edit_chapter', $data);
	 }
}