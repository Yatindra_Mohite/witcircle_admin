<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subscription_model extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
	}

	public function index(){
    	$data['sub'] = $this->common_model->common_getRow('subscription',array('subscription_id'=>1));
    	$data['sub2'] = $this->common_model->common_getRow('subscription',array('subscription_id'=>2));
    	$data['sub3'] = $this->common_model->common_getRow('subscription',array('subscription_id'=>3));


		$this->load->view('admin/subscription_model/subscription_model',$data);
	}

    public function one(){
       
      if(isset($_POST['submit']))
      { 
      	$arr = array(
			'subscription_name' => $this->input->post('name'),
			'subscription_amount' => $this->input->post('price'),
		 	'subscription_desc' => $this->input->post('text'),
        );
        $updateid = $this->common_model->updateData('subscription',$arr,array('subscription_id'=>1),array());

        if($updateid == '' || $updateid == '0')
		{
		 	$this->session->set_flashdata('failed' ,'Something went wrong');	
		    redirect(base_url('subscription_model'));
		}
		else
		{   
            $this->session->set_flashdata('success' ,'Subscription model updated successfully');	
		    redirect(base_url('subscription_model'));
		}	

      }
		$this->load->view('admin/subscription_model/subscription_model');
	}

	public function two(){
       
      if(isset($_POST['submit']))
      { 
      	$arr = array(
			'subscription_name' => $this->input->post('name2'),
			'subscription_amount' => $this->input->post('price2'),
		 	'subscription_desc' => $this->input->post('text2'),
        );
       // print_r($arr);
       // exit;
        $updateid = $this->common_model->updateData('subscription',$arr,array('subscription_id'=>2),array());

        if($updateid == '' || $updateid == '0')
		{
		 	$this->session->set_flashdata('failed2' ,'Something went wrong');	
		    redirect(base_url('subscription_model'));
		}
		else
		{   
            $this->session->set_flashdata('success2' ,'Subscription model updated successfully');	
		    redirect(base_url('subscription_model'));
		}	

      }
		$this->load->view('admin/subscription_model/subscription_model');
	}

	public function three(){
       
      if(isset($_POST['submit']))
      { 
      	$arr = array(
			'subscription_name' => $this->input->post('name3'),
			'subscription_amount' => $this->input->post('price3'),
		 	'subscription_desc' => $this->input->post('text3'),
        );
       // print_r($arr);
       // exit;
        $updateid = $this->common_model->updateData('subscription',$arr,array('subscription_id'=>3),array());

        if($updateid == '' || $updateid == '0')
		{
		 	$this->session->set_flashdata('failed3' ,'Something went wrong');	
		    redirect(base_url('subscription_model'));
		}
		else
		{   
            $this->session->set_flashdata('success3','Subscription model updated successfully');	
		    redirect(base_url('subscription_model'));
		}	

      }
		$this->load->view('admin/subscription_model/subscription_model');
	}

	public function update_data(){
         $statuss = $this->input->post('status_id');
       //exit;
         $updateid = $this->common_model->updateData('subscription',array('status'=>$statuss),array('subscription_id'=>1),array());
	}

	public function update_data2(){
         $statuss = $this->input->post('status_id');
       //exit;
         $updateid = $this->common_model->updateData('subscription',array('status'=>$statuss),array('subscription_id'=>2),array());
	}

	public function update_data3(){
         $statuss = $this->input->post('status_id');
       //exit;
         $updateid = $this->common_model->updateData('subscription',array('status'=>$statuss),array('subscription_id'=>3),array());
	}

}	

