<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
     
			// $currentdate = date("Y-m-d H:i:s");
			// echo $currentdate;
			// exit;
			// define('currentdate', $currentdate);

		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
		
	}

	public function index(){
        
        $currentdate = date("Y-m-d H:i:s");

		if(isset($_POST['submit']))
		{   
		     $course_name = strtolower($this->input->post ('course_name'));
		     $question_time =  $this->input->post ('question_time');
		     $course_time =  $this->input->post ('course_time');
		     $sec_pos_mark =  $this->input->post ('sectional_positive_mark');
		     $sec_neg_mark =  $this->input->post ('sectional_negative_mark');
			 $price =$this->input->post('price');
			 $subjective_price =  $this->input->post ('subjective_price');
		     $sectional_price =  $this->input->post ('sectional_price');
			 
			 $insertid = $this->common_model->common_insert('course',array('course_name'=>$course_name,'subjective_coin'=>$subjective_price,'sectional_price'=>$sectional_price,'total_course_time'=>$course_time,'course_coin'=>$price,'time_per_question'=>$question_time,'sectional_positive_mark'=>$sec_pos_mark,'sectional_negative_mark'=>$sec_neg_mark,'create_at'=>$currentdate));
			 
			 if($insertid == '' || $insertid == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('course/course_list'));
			 }
			 else
			 {
	             //       for($i=0; $i < count($s); $i++) 
	             // { 
	             //    $v_data = array( 'course_id'=>$insertid,
	             //                     'name'=>$s[$i],
	             //                     'positive_mark'=>$s1[$i],
	             //                     'partial_mark'=>$s2[$i],
	             //                     'negative_mark'=>$s3[$i],
	             //                     'create_at'=>$currentdate
	             //                    );

	             //    $mark = $this->common_model->common_insert('marking',$v_data);
	             // }

			 	$this->db->query("UPDATE global_counters SET course_count = course_count + 1 WHERE id = 1");

	            $this->session->set_flashdata('success' ,'Course added successfully');	
			    redirect(base_url('course/course_list'));
			 }	
		}
       $this->load->view('admin/course/add_course');
	}

	public function course_list(){

	$data['course_list'] = $this->common_model->getData('course',array(),'id','DESC');
				 
       $this->load->view('admin/course/course_list', $data);
	}

	public function change_status(){
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('admin_status');
              
        $update = $this->common_model->updateData("course",array('status'=>$status),array('id'=>$user_id));

          if($update)
          {
             echo '1000';exit; 
          }
    }

    public function edit($id = false){

    	$id = $this->common_model->id_decrypt($id);
        
		if(isset($_POST['submit']))
		{   
		    $course_name = strtolower($this->input->post ('course_name'));
		    $question_time =  $this->input->post ('question_time');
			$course_time =  $this->input->post ('course_time');
			$sec_pos_mark =  $this->input->post ('sectional_positive_mark');
			$sec_neg_mark =  $this->input->post ('sectional_negative_mark');
			$price =$this->input->post('price');
			$subjective_price =  $this->input->post ('subjective_price');
		    $sectional_price =  $this->input->post ('sectional_price');
			//exit;
			$update = $this->common_model->updateData('course',array('course_name'=>$course_name,'subjective_coin'=>$subjective_price,'sectional_price'=>$sectional_price,'course_coin'=>$price,'time_per_question'=>$question_time,'total_course_time'=>$course_time,'sectional_positive_mark'=>$sec_pos_mark,'sectional_negative_mark'=>$sec_neg_mark),array('id'=>$id));
			//echo $a = $this->db->last_query();
			//exit;
			 
			if($update == '' || $update == '0') {
			 	
			  $this->session->set_flashdata('failed' ,'Something went wrong');	
			  redirect(base_url('course/course_list'));
			}
			else
			{
                $this->session->set_flashdata('success' ,'Course updated successfully');	
			    redirect(base_url('course/course_list'));
			}	
		}
        
       $data['course_edit'] = $this->common_model->common_getRow('course',array('id'=>$id));
      // print_r($data['course_edit']);
       //exit;

       $this->load->view('admin/course/edit_course', $data);
	}
  
}