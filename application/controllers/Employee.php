<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( (!$userid = $this->session->userdata('admin_id')) && $this->session->userdata('admin_id')!=1){
			redirect(base_url('login'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
		date_default_timezone_set('Asia/Kolkata');
	    
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
	}
	
public function index()
{ 
   $uid = $this->session->userdata('admin_id');
   $this->db->order_by('user_id','DESC');
   $data['sub_admin_data'] = $this->db->get_where('admin',array('user_id !='=>$uid,'role !='=>'admin'))->result();
   $this->load->view('admin/employee/employee_list',$data);
}
  
public function add_employee()
{ 
   $admin_id = $this->session->userdata('admin_id');
   $arr1 = array();
   $this->db->group_by('sidebar_menu.menu_id');
	   	$this->db->join("sidebar_sub_menu","sidebar_sub_menu.menu_id=sidebar_menu.menu_id AND sidebar_sub_menu.status=1","left");
	   	//$this->db->where_not_in('sidebar_sub_menu.sub_menu_id','33,34,35,36,37,38,39',false);
	   	$menusubmenudata = $this->db->select("sidebar_menu.order_id,sidebar_menu.menu_id,sidebar_menu.menu_name,sidebar_menu.page_name,GROUP_CONCAT(sidebar_sub_menu.sub_menu_id) as submenuid,GROUP_CONCAT(sidebar_sub_menu.sub_menu_name) as submenuname,GROUP_CONCAT(sidebar_sub_menu.page_name) as submenupagename")->get_where("sidebar_menu",array('sidebar_menu.status'=>1,'sidebar_menu.menu_id !='=>1))->result();
		if(!empty($menusubmenudata))
		{
			foreach ($menusubmenudata as $key) {
				$menid[] = $key->menu_id;
				$arr1[] =array(
					'order_id'=>$key->order_id,
					'menu_id'=>$key->menu_id,
					'menu_name'=>$key->menu_name,
					'page_name'=>$key->page_name,
					'submenuid'=>$key->submenuid,
					'submenuname'=>$key->submenuname,
					'submenupagename'=>$key->submenupagename
				);
			}
			//$imment = implode(',', $menid);
		}
		// $menu_witout_submenu = $this->db->query("SELECT sidebar_menu.order_id,sidebar_menu.menu_id,sidebar_menu.menu_name,sidebar_menu.page_name FROM sidebar_menu WHERE menu_id NOT IN ($imment) AND menu_id !=1")->result();
		// if(!empty($menu_witout_submenu))
		// {
		// 	foreach ($menu_witout_submenu as $value) {
		// 		$arr1[] =array(
		// 			'order_id'=>$value->order_id,
		// 			'menu_id'=>$value->menu_id,
		// 			'menu_name'=>$value->menu_name,
		// 			'page_name'=>$value->page_name,
		// 			'submenuid'=>'',
		// 			'submenuname'=>'',
		// 			'submenupagename'=>''
		// 		);
		// 	}
		// }
		
		// usort($arr1, function($a, $b) {
		//     return $a['order_id'] <=> $b['order_id'];
		// });
  
   if(!empty($arr1))
   {
   		foreach ($arr1 as $key){
   			
   			if(!empty($key['submenuid']) && $key['submenuid'] != 'NULL')
   			{
	   			$expsubid = explode(',', $key['submenuid']);
	   			$expsubname = explode(',', $key['submenuname']);
	   			$ccount = count($expsubid);
	   			for ($i=0; $i < $ccount ; $i++) { 
	   				$arr[] = array('id'=>$expsubid[$i],'name'=>$expsubname[$i]);
	   			}
   			}else
   			{
   				$arr[] = array('id'=>$key['menu_id'],'name'=>$key['menu_name']);
   			}
   		}
   }
   
   	if(isset($_POST['submit']))
   	{ 

	   	   $email = $this->input->post('email');
	   	   $check_email = $this->db->select('user_id')->get_where('admin',array("user_email"=>$email))->row();
	   	   if(!empty($check_email))
	   	   {	

		   	   // 		$this->session->set_flashdata('name', $this->input->post('name'));
		   	   // 		$this->session->set_flashdata('passw', $this->input->post('password'));
		   	   // 		$this->session->set_flashdata('num', $this->input->post('number'));
		   	   // 		// $this->session->set_flashdata('image', $this->input->post('image'));
		   	   // 		// $this->session->set_flashdata('langOpt3', $this->input->post('langOpt3[]'));
	   	   	// 	$this->session->set_flashdata('failed', 'Email already exists.');
		      	// redirect('employee/add_employee');
	   	   }else
	   	   {
		   	   	$menuar = $this->input->post('langOpt3');
				if(!empty($menuar))
				{
					$images = 'default-medium.png';
					if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
				    {  
						$date = date("ymdhis");
						$config['upload_path'] = 'uploads/sub_admin_image/';
						$config['allowed_types'] = 'jpg|png|jpeg';
						$filename = $_FILES['image']['name'];
						$subFileName = explode('.',$filename);
						$ExtFileName = end($subFileName);
						$config['file_name'] = md5($filename.$date).'.'.$ExtFileName;
			            
						$this->load->library('upload', $config);
						$this->upload->initialize($config);

						if($this->upload->do_upload('image'))
			            { 
			              $upload_data = $this->upload->data();
			              $images = $upload_data['file_name'];
					       //ini_set("memory_limit", "-1");
			             // $config['image_library']  = 'gd2';
			              ////$config['source_image']   = 'uploads/admin_image/'.$image;
			              //$config['create_thumb']   = TRUE;
			              //$config['maintain_ratio'] = TRUE;
			              //$config['width']          = "100";
			              //$config['height']         = "100";
						  //$config['new_image'] = 'uploads/admin_image1/'.$image;
						  
						  //$this->load->library('image_lib');
			              //$this->image_lib->initialize($config);
			              //$newimage =  $this->image_lib->resize();
			              //$this->image_lib->clear();
			              //$x12 = explode('.', $image);//for extension
			              //$thumb_img =  $x12[0].'.'.$x12[1];
			            }
			            else
						{  
							$this->data['err']= $this->upload->display_errors();
							$this->session->set_flashdata('error_pic', $this->data['err']);		
							redirect('employee/add_employee'); exit;
						}
					}
					array_push($menuar, 'Dashboard');
					foreach ($menuar as $key) {
						$arrm[] = json_encode($key);
					}
					$imp = implode(',',$arrm);
					
					$menu_clm = $submenu_clm = $temp_menu = $temp_menuss = array();
					$getid = $this->db->query("SELECT menu_id,'mid',page_name FROM sidebar_menu WHERE menu_name IN ($imp)
												UNION ALL
											SELECT sub_menu_id,menu_id as mid,page_name FROM sidebar_sub_menu WHERE sub_menu_name IN ($imp)")->result();
					if(!empty($getid))
					{
						foreach ($getid as $value) {
							
							if($value->mid == 'mid')
							{
								$menu_clm[] = $value->menu_id;
								$temp_menuss[] = json_encode($value->page_name);
							}else
							{
								$temp_menu[] = json_encode($value->page_name);
								$menu_clm[] = $value->mid;
								$submenu_clm[] = $value->menu_id;
							}
						}
					    
						$submenu_clm_comma = implode(',',$submenu_clm);
						$tempsubmenu_clm_comma = implode(',',$temp_menu);
						$tempmenus_clm_comma = implode(',',$temp_menuss);
						$menu_clm_comma = implode(',',array_unique($menu_clm));
					   	$admin_data = array(
							'user_name'=>$this->input->post('name'),
							'user_password'=>sha1($this->input->post('password')),
							'user_mobile'=>$this->input->post('number'),
							'user_email'=>$email,
							'role'=>'subadmin',
							'menu_id'=>$menu_clm_comma,
							'submenu_id'=>$submenu_clm_comma,
							'temp_menu'=>$tempmenus_clm_comma,
							'temp_submenu'=>$tempsubmenu_clm_comma,
							'user_add_date'=>datetime,
							'user_profile_pic'=>$images
							);
					   	$insertdata = $this->common_model->common_insert("admin",$admin_data);
				   	  	$this->session->set_flashdata('success', 'Subadmin account successfully added.');
				      	redirect('employee/add_employee');
					}else
					{
						$this->session->set_flashdata('failed', 'Menu and Submenu required.');
			      		redirect('employee/add_employee');
					}
				}else
				{
					$this->session->set_flashdata('failed', 'Menu and Submenu required.');
			      	redirect('employee/add_employee');
				}
	   	   }
  	}
  	$data['menudata'] = $arr;
  	$this->load->view('admin/employee/add_employee',$data);
}

public function check_email()
{
	$email = $this->input->post('email');
	$check_email = $this->db->select('user_id')->get_where('admin',array("user_email"=>$email))->row();
   	if(!empty($check_email))
   	{	
   		echo "201"; exit;
    }
}

public function edit_detail()
{ 
   $adminid = $this->uri->segment(3);
   $id = $this->common_model->id_decrypt($adminid);
   $data['admin_data'] = $this->common_model->common_getRow('admin',array('user_id'=>$id));
   if(!empty($data['admin_data']))
   {
	   	$arr = array(); $imment = '0';
		$this->db->group_by('sidebar_menu.menu_id');
	   	$this->db->join("sidebar_sub_menu","sidebar_sub_menu.menu_id=sidebar_menu.menu_id AND sidebar_sub_menu.status=1","left");
	   	//$this->db->where_not_in('sidebar_sub_menu.sub_menu_id','33,34,35,36,37,38,39',false);
	   	$menusubmenudata = $this->db->select("sidebar_menu.order_id,sidebar_menu.menu_id,sidebar_menu.menu_name,sidebar_menu.page_name,GROUP_CONCAT(sidebar_sub_menu.sub_menu_id) as submenuid,GROUP_CONCAT(sidebar_sub_menu.sub_menu_name) as submenuname,GROUP_CONCAT(sidebar_sub_menu.page_name) as submenupagename")->get_where("sidebar_menu",array('sidebar_menu.status'=>1,'sidebar_menu.menu_id !='=>1))->result();
		if(!empty($menusubmenudata))
		{
			foreach ($menusubmenudata as $key) {
				//$menid[] = $key->menu_id;
				$arr[] =array(
					'order_id'=>$key->order_id,
					'menu_id'=>$key->menu_id,
					'menu_name'=>$key->menu_name,
					'page_name'=>$key->page_name,
					'submenuid'=>$key->submenuid,
					'submenuname'=>$key->submenuname,
					'submenupagename'=>$key->submenupagename
				);
			}
			//$imment = implode(',', $menid);
		}
		// $menu_witout_submenu = $this->db->query("SELECT sidebar_menu.order_id,sidebar_menu.menu_id,sidebar_menu.menu_name,sidebar_menu.page_name FROM sidebar_menu WHERE menu_id NOT IN ($imment) AND menu_id !=1")->result();
		// if(!empty($menu_witout_submenu))
		// {
		// 	foreach ($menu_witout_submenu as $value) {
		// 		$arr[] =array(
		// 			'order_id'=>$value->order_id,
		// 			'menu_id'=>$value->menu_id,
		// 			'menu_name'=>$value->menu_name,
		// 			'page_name'=>$value->page_name,
		// 			'submenuid'=>'',
		// 			'submenuname'=>'',
		// 			'submenupagename'=>''
		// 		);
		// 	}
		// }
		
		// usort($arr, function($a, $b) {
		//     return $a['order_id'] <=> $b['order_id'];
		// });
		
	   	if(!empty($arr))
	   	{
	   		$expsubmenu = array();
	   		if(!empty($data['admin_data']->temp_submenu)){
	   			$usermenu = $data['admin_data']->temp_menu.','.$data['admin_data']->temp_submenu;
	   			$submenuid= $data['admin_data']->submenu_id;
	   			$expsubmenu = explode(',',$submenuid);
	   		}else
	   		{
	   			$usermenu = $data['admin_data']->temp_menu;
	   		}
	   		$explode = explode(',',$usermenu);
	   		
	   		foreach ($arr as $key) {
	   		
	   			if(!empty($key['submenuid']) && $key['submenuid'] != 'NULL')
	   			{
		   			$expsubid = explode(',', $key['submenuid']);
		   			$expsubname = explode(',', $key['submenuname']);
		   			$expsubnamepage = explode(',', $key['submenupagename']);
		   			$ccount = count($expsubid);
		   			for ($i=0; $i < $ccount ; $i++) { 
		   				
		   			//print_r(($exparr));exit;
		   			//print_r($expsubnamepage[$i]);
		   				$check='';
		   				if(in_array(json_encode($expsubnamepage[$i]),$explode))
		   				{
		   					if(!empty($expsubmenu) && in_array($expsubid[$i],$expsubmenu))
		   					{
		   						$check = 'selected';
		   					}
		   				}
		   				$droparr[] = "<option value='".$expsubname[$i]."' $check>".$expsubname[$i]."</option>";
		   					//$arr[] = array('id'=>$expsubid[$i],'name'=>$expsubname[$i]);
		   			}
		   		}else
	   			{
	   				$check='';
	   				if(in_array(json_encode($key['page_name']),$explode))
	   				{
	   					$check = 'selected';
	   				}
	   				$droparr[] = "<option value='".$key['menu_name']."' $check>".$key['menu_name']."</option>";
	   				//$arr[] = array('id'=>$key->menu_id,'name'=>$key->menu_name);
	   			}
	   		} 
	   }
	 	if(isset($_POST['submit']))
	   	{ 
			$checkcont = $this->db->select("user_id")->get_where("admin",array('user_email'=>$this->input->post('email'),'user_id !='=>$data['admin_data']->user_id))->row();
	   	   	if(!empty($checkcont))
	   	   	{
	   	   		$this->session->set_flashdata('failed', 'Email already exists.');
		   		redirect('employee/edit_detail/'.$adminid);	
	   	   	}else
	   	   	{
	   	   		$menuar = $this->input->post('langOpt3');
				if(!empty($menuar))
				{
					if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
				    {  
						$date = date("ymdhis");
						$config['upload_path'] = 'uploads/sub_admin_image/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$filename = $_FILES['image']['name'];
						$subFileName = explode('.',$filename);
						$ExtFileName = end($subFileName);
						$config['file_name'] = md5($filename.$date).'.'.$ExtFileName;
			            
						$this->load->library('upload', $config);
						$this->upload->initialize($config);

						if($this->upload->do_upload('image'))
			            { 
				              $upload_data = $this->upload->data();
				              $images = $upload_data['file_name'];
						       //ini_set("memory_limit", "-1");
				             	// $config['image_library']  = 'gd2';
				              ////$config['source_image']   = 'uploads/admin_image/'.$image;
				              //$config['create_thumb']   = TRUE;
				              //$config['maintain_ratio'] = TRUE;
				              //$config['width']          = "100";
				              //$config['height']         = "100";
							  //$config['new_image'] = 'uploads/admin_image1/'.$image;
							  
							  //$this->load->library('image_lib');
				              //$this->image_lib->initialize($config);
				              //$newimage =  $this->image_lib->resize();
				              //$this->image_lib->clear();
				              //$x12 = explode('.', $image);//for extension
				              //$thumb_img =  $x12[0].'.'.$x12[1];
			            }
			            else
						{  
							$this->data['err']= $this->upload->display_errors();
							$this->session->set_flashdata('error_pic', $this->data['err']);		
							redirect('employee/edit_detail'); exit;
						}
					}
					else
					{ 
				   		$images = $data['admin_data']->user_profile_pic;
					}

					//array_push($menuar, 'Dashboard');
					array_unshift($menuar,'Dashboard','get subject new','get chapter','get subject');
					
					foreach ($menuar as $key) {
						$arrm[] = json_encode($key);
					}
					$imp = implode(',',$arrm);
					
					$menu_clm = $submenu_clm = $temp_menu = $temp_menuss = array();
					$getid = $this->db->query("SELECT menu_id,'mid',page_name FROM sidebar_menu WHERE menu_name IN ($imp)
												UNION ALL
											SELECT sub_menu_id,menu_id as mid,page_name FROM sidebar_sub_menu WHERE sub_menu_name IN ($imp)")->result();
					
					if(!empty($getid))
					{
						foreach ($getid as $value) {
							
							if($value->mid == 'mid')
							{
								$menu_clm[] = $value->menu_id;
								$temp_menuss[] = json_encode($value->page_name);
							}else
							{
								$temp_menu[] = json_encode($value->page_name);
								if(!in_array($value->menu_id,array('33','34','35','36','37','38','39')))
								{
									$menu_clm[] = $value->mid;
								}
								$submenu_clm[] = $value->menu_id;
							}
						}
					    
					//print_r($submenu_clm_comma);exit;
						$submenu_clm_comma = implode(',',$submenu_clm);
						$tempsubmenu_clm_comma = implode(',',array_unique($temp_menu));
						$tempmenus_clm_comma = implode(',',$temp_menuss);
						$menu_clm_comma = implode(',',array_unique($menu_clm));
					   	$admin_data = array(
							'user_name'=>$this->input->post('name'),
							'user_mobile'=>$this->input->post('number'),
							'user_email'=>$this->input->post('email'),
							'menu_id'=>$menu_clm_comma,
							'submenu_id'=>$submenu_clm_comma,
							'temp_menu'=>$tempmenus_clm_comma,
							'temp_submenu'=>$tempsubmenu_clm_comma,
							'user_profile_pic'=>$images,
							'role'=>''
							);
					   	$update = $this->common_model->updateData('admin',$admin_data,array('user_id'=>$id));
				  		//print_r($update);exit;
				  	 	$this->session->set_flashdata('success', 'Profile successfully updated.');
				      	redirect('employee/edit_detail/'.$adminid);
					}else
					{
						$this->session->set_flashdata('failed', 'Menu and Submenu required.');
			      		redirect('employee/edit_detail/'.$adminid);
					}
				}else
				{
					$this->session->set_flashdata('failed', 'Menu and Submenu required.');
			      	redirect('employee/edit_detail/'.$adminid);
				}
	   	   	}
		}
	   	

	   	if(isset($_POST['submit1']))
	   	{
	   	 	//$data['admin_data'] = $this->common_model->common_getRow('admin',array('user_id'=>$id));
	        //echo $data['admin_data']->user_password;
	        //exit;
	  		// if($data['admin_data']->user_password!=$old_password1)
			// { 
			// 	$this->session->set_flashdata('fail', 'Invalid old password');
			// 	redirect('employee/edit_employee_detail');
			// }
		   $admin_data = array(
				  'user_password'=>sha1($this->input->post('c_password')),
				);
	 		$update = $this->common_model->updateData('admin',$admin_data,array('user_id'=>$id));
	   	 	$this->session->set_flashdata('success', 'Password successfully updated.');
	   		redirect('employee/edit_detail/'.$adminid);	
	   	}	

   $data['options'] = $droparr;
   }
   $this->load->view('admin/employee/edit_employee_detail',$data);
}

function permission_detail($user_id)
{
	$uid = $this->session->userdata('admin_id');
	//echo $user_id;exit;
	if(!empty($user_id))
	{
		$subadminid = $this->common_model->id_decrypt($user_id);
	   	$this->db->order_by('user_id','DESC');
	   	$sub_admin_data = $this->db->get_where('admin',array('user_id'=>$subadminid,'user_id !='=>$uid,'role !='=>'admin'))->result();
	   	if(!empty($sub_admin_data))
	   	{	$subid = '';
		   	foreach ($sub_admin_data as $key) {
			   	if(!empty($key->temp_submenu)){
			        $usermenu = str_replace('"',"'",$key->temp_menu).','.str_replace('"',"'",$key->temp_submenu);
			    	$subid = $key->submenu_id;
			    }else
			    {
			        $usermenu = str_replace('"','',$key->temp_menu);
			   		$menbid = $key->menu_id;
			   	}
			    $darr = $arr = array();
			    $explode = explode(',',$usermenu);
			    $explodeid = explode(',',$usermenu);
			    $aa = "'get subject new','get subject','get chapter'";
			    
			    //$name = $this->db->select("sub_menu_name")->get_where("sidebar_sub_menu")->result();
			   	//$where = "sidebar_menu.page_name IN ($usermenu) OR sidebar_sub_menu.page_name IN ($usermenu)";
			    $this->db->where_in('sidebar_menu.page_name',$usermenu,false);
			    $submenudata = $this->db->select("sidebar_menu.menu_name as submenuid")->get_where("sidebar_menu",array())->result();
			    if(!empty($submenudata))
			    {
			        foreach ($submenudata as $value) {
			            $arr[] = $value->submenuid;
			        }
			    }
			   	$this->db->where_in('sidebar_sub_menu.sub_menu_id',$subid,false);
			    $this->db->where_not_in('sidebar_sub_menu.sub_menu_name',$aa ,false);
			   	$menudata = $this->db->select("sidebar_sub_menu.sub_menu_name as submenuid")->get_where("sidebar_sub_menu",array())->result();
			    if(!empty($menudata))
			    {
			        foreach ($menudata as $valuear) {
			            $arr[] = $valuear->submenuid;
			        }
			    }
	   		}
	   		$data['permission_arr'] = $arr;
	   	}else
	   	{
			redirect('employee');
	   	}
	}else
	{
		redirect('employee');
	}

   $this->load->view('admin/employee/permission_detail',$data);	
}

public function change_status(){
    $user_id = $this->input->post('user_id');
    $status = $this->input->post('admin_status');
    $update = $this->common_model->updateData("admin",array('user_status'=>$status),array('user_id'=>$user_id));
    if($update)
    {   
      	echo "1000";exit;
	}
}

}
