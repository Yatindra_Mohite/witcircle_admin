<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sectional_test extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
	}

	public function isJSON($string){
		   return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
		} 

	public function index()
	{

        $currentdate = date("Y-m-d H:i:s");

		if(isset($_POST['submit']))
		{   
		    $question_id = $this->input->post ('question_type');

		    if ($question_id != 10)// only for comprehensive
			{

				if ($question_id == 2) {
				   $ids = $this->input->post ('answer_id2');
				} 
				 
				elseif ($question_id == 4) {
				   $ids = $this->input->post ('answer_id1');
				} 
				 
				elseif ($question_id == 5) {
				   $ids = implode(",", $this->input->post ('answer_id3'));

				}

				$option = array();        
	            $j = 0;
	            for ($i = 1; $i < 7; $i++) { 
	                $a=str_replace('"', "'",$this->input->post('text_desc'.$i));
	                if ($a!='') {
	                    $j++;
	                   $data1["id"] = $j;
	                    $data1["option"] = $a;
	                    $option[] = $data1;    
	                    $data1= [];
	               }
	            } 
	            $op = stripslashes(json_encode($option));
                nl2br(strip_tags($op));

	            if($this->isJSON($op)){
				 //echo "It's JSON";
				}else{
					//echo "ihihihihih";
					$this->session->set_flashdata('failed' ,'Something went wrong in your options please check.');	
			    	redirect(base_url('sectional_test'));
				}

				//exit;
			    $u_data = array(
			    // 'guide_line'=> $this->session->userdata('guide_line'),
			    'added_by' => 'admin',
			    'added_id' => $this->session->userdata('admin_id'),
			    'test_id' => '0',
			    'subject_id' => $this->input->post ('subject_id'),
			    'question_type' => $question_id,
			    'marking_scheme' => '0',
			    'chapter_id' => $this->input->post ('chapter_id'),
			    'question' => $this->input->post('ques_desc'),
			    'options' => $op,
			    'answer'=> $ids,
			    'create_at'=>date("Y-m-d H:i:s"),
			    );
			    
			    //print_r($u_data);
			    //exit;

			    $insertid = $this->common_model->common_insert('questions',$u_data);

			}
			else
			{   
                $u_data = array(
                'test_id' => '0',
                'question_type' => $question_id,
                'chapter_id'=> $this->input->post ('chapter_id'),
			    'question' => $this->input->post ('ques_desc'),
			    'subject_id' => $this->input->post ('subject_id'),
			    'no_of_question' => $this->input->post ('com_total_question'),
			    'create_at'=>date("Y-m-d H:i:s"),
			    );
                
                $insertid = $this->common_model->common_insert('questions_comprehensive',$u_data)	;

			}    
		    
			if($insertid == '' && $insertid == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('sectional_test'));
			}
			else
			{
				$this->session->set_userdata ( array (
					            'chapter_id_sess2' => $this->input->post ('chapter_id'),
								'subject_id_sess2'   => $this->input->post ('subject_id'),
								'question_type_sess2'   => $this->input->post ('question_type'),
								'marking_scheme_sess2'   => $this->input->post ('marking_scheme'),								
								));
                
                $this->db->query("UPDATE global_counters SET sectional_test_count = sectional_test_count + 1 WHERE id = 1");

               $this->session->set_flashdata('success' ,'Test added successfully');	
			   redirect(base_url('sectional_test/'));
			}	
		}

	  $data['subjects'] = $this->db->query("select * from course_subject")->result();
	  
	  $data['question_type'] = $this->common_model->getData('question_type',array(),'id','DESC');

      $this->load->view('admin/sectional_test/add_sectional_test',$data);
	}
	
	public function edit_question($ques = false,$sub_id =false , $chap_id = false)
	{
        $id = $this->common_model->id_decrypt($ques);

        if(isset($_POST['submit']))
		{   
		    $question_id = $this->input->post ('question_type');

			if ($question_id == 2) {
			   $ids = $this->input->post ('answer_id2');
			} 
			 
			elseif ($question_id == 4) {
			   $ids = $this->input->post ('answer_id1');
			} 
			 
			elseif ($question_id == 5) {
			   $ids = implode(",", $this->input->post ('answer_id3'));
			}

			$option = array();        
            $j = 0;
            for ($i = 1; $i < 7; $i++) { 
                $a=str_replace('"', "'",$this->input->post('text_desc'.$i));
                if ($a!='') {
                    $j++;
                   $data1["id"] = $j;
                    $data1["option"] = $a;
                    $option[] = $data1;    
                    $data1= [];
               }
            } 
            $op = stripslashes(json_encode($option));

            if($this->isJSON($op)){
				 //echo "It's JSON";
			}else{
				//echo "ihihihihih";
				$this->session->set_flashdata('failed' ,'Something went wrong in your options please check.');	
		    	redirect(base_url('sectional_test/edit_question/'.$ques.'/'.$sub_id.'/'.$chap_id));
		    	//echo "<script> alert('please check') </script>";
			}
			//exit;

		    $u_data = array(
		   // 'guide_line'=> $this->input->post('guide_line'),		
		    'added_by' => 'admin',
		    'added_id' => $this->session->userdata('admin_id'),
		    'test_id' => '0',
		    'subject_id' => $this->input->post ('subject_id'),
		    'question_type' => $question_id,
		    'marking_scheme' => '0',
		    'chapter_id' => $this->input->post ('chapter_id'),
		    'question' => $this->input->post('ques_desc'),
		    'options' => $op,
		    'answer'=> $ids,
		    'create_at'=>date("Y-m-d H:i:s"),
		    );

		    //print_r($u_data);
		    //exit;
        
		    $insertid = $this->common_model->updateData('questions',$u_data,array('id'=>$id),array());   

			if($insertid == '' && $insertid == '0')
				{
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('sectional_test/sectional_test_list'));
			}
			else
			{  
				$log_data['opr'] = 'edit';
                $log_data['by_user'] = $this->session->userdata('admin_id');
                $log_data['data_type'] = 'sectional test question';
                $log_data['type_id'] = $id;
                $log_data['ip'] = $this->input->ip_address();
                $log_data['create_at'] = 'NOW()';
                $this->db->insert('log_table', $log_data);

               $this->session->set_flashdata('success' ,'Test Update successfully');	
			   redirect(base_url('sectional_test/all_question/'.$sub_id.'/'.$chap_id));
			}
        }

        $data['subjects'] = $this->db->query("select * from course_subject")->result();
	  
	    $data['question_type'] = $this->common_model->getData('question_type',array(),'id','DESC');

	    $data['question_edit'] = $this->common_model->common_getRow('questions',array('id'=>$id));

	    $data['chapter_list'] = $this->common_model->getData('chapter',array('subject_id'=>$data['question_edit']->subject_id));
        
        $this->load->view('admin/sectional_test/edit_sectional_test', $data);
	}

	public function get_chapter()
	{
		$subject_id = $this->input->post('subject_id');
		$chapter=$this->db->query("select * from chapter where subject_id=".$subject_id)->result();
		 if (empty($chapter)) {?>
		  <option value="" selected="selected">No chapters in this subject</option>
		   <?php }else{?>
	      <option value="" disabled selected="selected">Select chapter</option>
	  <?php }
	  
	  foreach ($chapter as $key) 
	  { 
	  ?>
	    <option value="<?php echo $key->id;?>" <?php if($key->id == $this->session->userdata('chapter_id_sess2')){ echo "selected";} ?>><?php echo $key->chapter_name;?></option>
	   <?php
	  }
	}
	
    public function sectional_test_list()
    {
	   $data['sectional_list'] = $this->db->query("SELECT DISTINCT subject_id, chapter_id,count(chapter_id) as no_of_q FROM questions WHERE test_id = 0 AND chapter_id!= 0 GROUP BY chapter_id,subject_id")->result();
	  				 
       $this->load->view('admin/sectional_test/sectional_list', $data);
	}

	public function all_question($subject_id =false , $chapter_id = false)
    {

	   $s_id = $this->common_model->id_decrypt($subject_id);

	   $c_id = $this->common_model->id_decrypt($chapter_id);

       $data['que_list'] = $this->common_model->getData('questions',array('subject_id'=>$s_id,'chapter_id'=>$c_id,'comprehensive_id'=>'0','status'=>1));
       //print_r($data['subject']);
       $this->load->view('admin/sectional_test/view_question', $data);
	}

	public function com_all_question($subject_id =false , $chapter_id = false)
    {
	   $s_id = $this->common_model->id_decrypt($subject_id);

	   $c_id = $this->common_model->id_decrypt($chapter_id);

       $data['que_list'] = $this->common_model->getData('questions_comprehensive',array('subject_id'=>$s_id,'chapter_id'=>$c_id,'status'=>1));
      
       $this->load->view('admin/sectional_test/com_question_list', $data);
	}

	public function add_comprehension_question($com_id = false)
    {   
   	    //$id = $this->common_model->id_decrypt($id);

   	    $com_id = $this->common_model->id_decrypt($com_id);

   	    $data2 = $this->db->query(" SELECT subject_id,question_type,chapter_id FROM questions_comprehensive WHERE id = $com_id ORDER BY id DESC ")->result();

   	    if(isset($_POST['submit']))
		{   
			$type = $this->input->post ('type_id');

			if ($type == 1) {
			   $ids = $this->input->post ('answer_id1');
			} 
			 
			elseif ($type == 2) {
			   $ids = implode(",", $this->input->post ('answer_id3'));
			}

			$option = array();        
            $j = 0;
            for ($i = 1; $i < 7; $i++) { 
                $a=str_replace('"', "'",$this->input->post('text_desc'.$i));
                if ($a!='') {
                    $j++;
                    $data1["id"] = $j;
                    $data1["option"] = $a;
                    $option[] = $data1;    
                    $data1= [];
               }
            } 
            $op = stripslashes(json_encode($option));

            if($this->isJSON($op)){
				 //echo "It's JSON";
			}else{
				//echo "ihihihihih";
				$this->session->set_flashdata('failed' ,'Something went wrong in your options please check.');	
		    	redirect(base_url('sectional_test/add_comprehension_question/'.$com_id));
			}

		    $u_data = array(
			    'added_by' => 'admin',
			    'added_id' => $this->session->userdata('admin_id'),
			    'test_id' => '0',
			    'subject_id' => $data2[0]->subject_id,
			    'question_type' =>$data2[0]->question_type,
			    'chapter_id'=>$data2[0]->chapter_id,
			    'marking_scheme' => '0',
			    'comprehensive_id'=> $com_id,
			    'comprehensive_q_type'=>$type,
			    //'chapter_id' => '0',
			    'question' => $this->input->post('ques_desc'),
			    'options' => $op,
			    'answer'=> $ids,
			    'create_at'=>date("Y-m-d H:i:s"),
		        );

		    $insertid = $this->common_model->common_insert('questions',$u_data);
			 
			if($insertid == '' || $insertid == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('sectional_test/add_comprehension_question'));
			}
			else
			{  
                $this->db->query("UPDATE global_counters SET sectional_test_count = sectional_test_count + 1 WHERE id = 1");

               $this->session->set_flashdata('success' ,'comprehension Update successfully');	
			   redirect(base_url('sectional_test/add_comprehension_question/'.$this->common_model->id_encrypt($com_id)));
	        }
	   }
        

        $data['marking'] = $this->common_model->getData('marking',array(),'id','DESC'); 

        
 
       $this->load->view('admin/sectional_test/add_comprehension_question', $data);
    }

    public function com_question_list($com_id = false)
    {   
    	$com_id = $this->common_model->id_decrypt($com_id);

    	$data['que_list'] = $this->common_model->getData('questions',array('comprehensive_id'=>$com_id, 'chapter_id !='=>'0','status'=>1 ),'id','DESC');

        $this->load->view('admin/sectional_test/comprehension_question_list', $data);
    }

    public function edit_comprehension($com_id = false)
    {
        $id = $this->common_model->id_decrypt($com_id);
         
        if(isset($_POST['submit']))
		{   
            $sub_id = $this->input->post('subject_id');
            $chap_id = $this->input->post('chapter_id');

			$update = $this->common_model->updateData('questions_comprehensive',array('subject_id'=>$sub_id,'no_of_question'=>$this->input->post('com_total_question'),'question'=>$this->input->post('ques_desc'),'chapter_id'=>$chap_id),array('id' => $id),array());
			
			if($update == '' || $update == '0') 
			{
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			    redirect(base_url('sectional_test/sectional_test_list/'));
			}
			else
			{
               $update = $this->common_model->updateData('questions',array('subject_id'=> $sub_id,'chapter_id'=>$chap_id),array('comprehensive_id' => $id,'chapter_id !='=>'0'),array());
                $log_data['opr'] = 'edit';
                $log_data['by_user'] = $this->session->userdata('admin_id');
                $log_data['data_type'] = 'sectional test comp passage';
                $log_data['type_id'] = $id;
                $log_data['ip'] = $this->input->ip_address();
                $log_data['create_at'] = 'NOW()';
                $this->db->insert('log_table', $log_data); 		   
               $this->session->set_flashdata('success' ,'Comprehension question update successfully');	
			   redirect(base_url('sectional_test/sectional_test_list/'));
	        }
		    
		}

        $data['subjects'] = $this->db->query("select * from course_subject")->result();
        
        $data['question_edit'] = $this->common_model->common_getRow('questions_comprehensive',array('id'=>$id));

        $data['chapter_list'] = $this->common_model->getData('chapter',array('subject_id'=>$data['question_edit']->subject_id));
        
        $this->load->view('admin/sectional_test/edit_comprehension', $data);
    }

    public function edit_comprehension_question($que_id = false)
    {   
   	    //$com_id = $this->common_model->id_decrypt($com_id);

   	    $que_id1 = $this->common_model->id_decrypt($que_id);
        
        //$data2 = $this->db->query(" SELECT subject_id,question_type FROM questions_comprehensive WHERE id = $com_id ORDER BY id DESC ")->result();
   	    //print_r($data1);
   	    //exit;

   	    if(isset($_POST['submit']))
		{   
			$type = $this->input->post ('type_id');
			$chap = $this->input->post ('get_chapter');
            $bb = $this->input->post ('get_com_id');


			if ($type == 1) {
			   $ids = $this->input->post ('answer_id1');
			} 
			 
			elseif ($type == 2) {
			   $ids = implode(",", $this->input->post ('answer_id3'));
			}

			$option = array();        
            $j = 0;
            for ($i = 1; $i < 7; $i++) { 
                $a=str_replace('"', "'",$this->input->post('text_desc'.$i));
                if ($a!='') {
                    $j++;
                   $data1["id"] = $j;
                    $data1["option"] = $a;
                    $option[] = $data1;    
                    $data1= [];
               }
            } 
            $op = stripslashes(json_encode($option));

            if($this->isJSON($op)){
				 //echo "It's JSON";
			}else{
				//echo "ihihihihih";
				$this->session->set_flashdata('failed' ,'Something went wrong in your options please check.');	
		    	redirect(base_url('sectional_test/edit_comprehension_question/'.$que_id));
			}

		    $u_data = array(
			    //'added_by' => 'admin',
			    //'added_id' => $this->session->userdata('admin_id'),
			    //'test_id' => $id,
			    //'subject_id' => $data2[0]->subject_id,
			    //'question_type' =>$data2[0]->question_type,
			    'marking_scheme' => $this->input->post ('marking_scheme'),
			    //'comprehensive_id'=> $com_id,
			    'comprehensive_q_type'=>$type,
			    'chapter_id' => $chap,
			    'question' => $this->input->post('ques_desc'),
			    'options' => $op,
			    'answer'=> $ids,
			    'create_at'=>date("Y-m-d H:i:s")
		        );

		    $update = $this->common_model->updateData('questions',$u_data,array('id' =>$que_id1 ),array());
			 
			if($update == '' || $update == '0') {
			 	$this->session->set_flashdata('failed' ,'Something went wrong');	
			     redirect(base_url('sectional_test/com_question_list/'.$this->common_model->id_encrypt($bb)));
			}
			else
			{
              $log_data['opr'] = 'edit';
                $log_data['by_user'] = $this->session->userdata('admin_id');
                $log_data['data_type'] = 'sectional test comp question';
                $log_data['type_id'] = $que_id1;
                $log_data['ip'] = $this->input->ip_address();
                $log_data['create_at'] = 'NOW()';
                $this->db->insert('log_table', $log_data); 		
             $this->session->set_flashdata('success' ,'Comprehension question update successfully');	
			   redirect(base_url('sectional_test/com_question_list/'.$this->common_model->id_encrypt($bb)));
	        }
	   } 

	    $data['question_edit'] = $this->common_model->common_getRow('questions',array('id'=>$que_id1));

        $data['marking'] = $this->common_model->getData('marking',array(),'id','DESC'); 
 
       $this->load->view('admin/sectional_test/edit_comprehension_question',$data);
    }    

    public function delete_question($question_id = false)
    {
        $id = $this->common_model->id_decrypt($question_id);
        $data_querry = $this->db->query("SELECT * FROM mock_test where FIND_IN_SET ('" . $id . "',questions_id) ")->result();
        if (count($data_querry) > 0) {
            echo '100';exit;
        } else {
            $delete = $this->common_model->updateData("questions", array('status' => 0), array('id' => $id));
            if ($delete) {
                $log_data['opr'] = 'delete';
                $log_data['by_user'] = $this->session->userdata('admin_id');
                $log_data['data_type'] = 'sectional test question';
                $log_data['type_id'] = $id;
                $log_data['ip'] = $this->input->ip_address();
                $log_data['create_at'] = 'NOW()';
                $this->db->insert('log_table', $log_data);
                echo '1000';exit;
            }
        }
    }


    public function delete_comp($id = false)
    {
        $data_querry = $this->db->query("SELECT * FROM questions where comprehensive_id=$id")->result();
        $flag = false; // if that question exist in any test;
        foreach ($data_querry as $key) {
            $data_que = $this->db->query("SELECT id FROM mock_test where FIND_IN_SET ('" . $key->id . "',questions_id) ")->result();
            if (count($data_que) > 0) {
                $flag = true;
                break;
            }
        }
        if ($flag) {
            echo '100';exit;
        } else {
            $delete = $this->common_model->updateData("questions_comprehensive", array('status' => 0), array('id' => $id));
            $delete = $this->common_model->updateData("questions", array('status' => 0), array('comprehensive_id' => $id));
            if ($delete) {
            	$log_data['opr'] = 'delete';
                $log_data['by_user'] = $this->session->userdata('admin_id');
                $log_data['data_type'] = 'sectional test comp passage';
                $log_data['type_id'] = $id;
                $log_data['ip'] = $this->input->ip_address();
                $log_data['create_at'] = 'NOW()';
                $this->db->insert('log_table', $log_data);
                echo '1000';exit;
            }
        }
    }



      public function delete_comp_que($id = false)
    {
        $id = $this->common_model->id_decrypt($id);
        $data_querry = $this->db->query("SELECT * FROM mock_test where FIND_IN_SET ('" . $id . "',questions_id) ")->result();
        if (count($data_querry) > 0) {
            echo '100';exit;
        } else {
            $delete = $this->common_model->updateData("questions", array('status' => 0), array('id' => $id));
            if ($delete) {
                $log_data['opr'] = 'delete';
                $log_data['by_user'] = $this->session->userdata('admin_id');
                $log_data['data_type'] = 'sectional test comp question';
                $log_data['type_id'] = $id;
                $log_data['ip'] = $this->input->ip_address();
                $log_data['create_at'] = 'NOW()';
                $this->db->insert('log_table', $log_data);
                echo '1000';exit;
            }
        }
    }



}