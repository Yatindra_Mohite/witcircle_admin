<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		if($this->session->userdata('admin_id') != 1 && $this->session->userdata('role') !='admin')
		{
			$uri = $this->uri->segment(1);
			$result = $this->common_model->check_permission($uri);
			if($result!='true')
			{
				redirect(base_url($result));
			}
		}
	}

	public function add_faq(){

		if(isset($_POST['submit']))
		{   
		    $course_id =0;
		    $question =$this->input->post ('question');
		    $answer = $this->input->post ('answer');
			 
			
            	$insertid = $this->common_model->common_insert('faq',array('question'=>$question,'answer'=>$answer));
			 
				if($insertid == '' || $insertid == '0')
				{
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('faq/faq_list'));
				}
				else
				{   
	                $this->session->set_flashdata('success' ,'Faq added successfully');	
				    redirect(base_url('faq/add_faq'));
				}
            		
		}
		//$data['subjects'] = $this->db->query("select * from course_subject")->result();		
    	$this->load->view('admin/faq/add_faq');
	 }

    public function faq_list(){

		$data['faq_list'] = $this->common_model->getData('faq',array(),'id','DESC');
				 
       $this->load->view('admin/faq/faq_list', $data);
	}

	public function delete($id = false){
		$delete = $this->common_model->deleteData("faq",array('id'=>$id));
		    if($delete)
		    {
		         echo '1000';exit; 
		    }
	}

	public function edit($id = false){

		if(isset($_POST['submit']))
		{   
		    $course_id =0;
		    $question =$this->input->post ('question');
		    $answer = $this->input->post ('answer');
			 
			
            	$insertid = $this->common_model->Updatedata('faq',array('question'=>$question,'answer'=>$answer),array('id'=>$id));
			 
				if($insertid == '' || $insertid == '0')
				{
				 	$this->session->set_flashdata('failed' ,'Something went wrong');	
				    redirect(base_url('faq/faq_list'));
				}
				else
				{   
	                $this->session->set_flashdata('success' ,'Faq updated successfully');	
				    redirect(base_url('faq/faq_list'));
				}
            		
		}
		$data['faq'] = $this->common_model->common_getRow('faq',array('id'=>$id));
		//print_r($data['faq']);
		//exit;		
    	$this->load->view('admin/faq/edit_faq',$data);

	}	 

}