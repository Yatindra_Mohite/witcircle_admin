<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Lead Management | All Leads</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url()?>template/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <?php include("header.php"); ?>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php include("sidebar.php"); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Leads
                            <small><big>All</big></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>order">All Leads</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Leads List</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            	<?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                            <!-- END EXAMPLE TABLE PORTLET-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>Leads  <?php echo $sh_head_page?></div>
                                    <div class="actions">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                               	<th> Project Name </th>
                                                <th> Client </th>
                                                <th> Project Manager </th>
                                                <th> Status</th>
                                                <th> Last Call </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                            	<th> # </th>
                                                <th> Project Name </th>
                                                <th> Client </th>
                                                <th> Project Manager </th>
                                                <th> Status</th>
                                                <th> Last Call </th>
                                                <th> Action </th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
										$sno=0;
										
											foreach($data as $key)
											{
												$sno++?>
												<tr>
													<td><?php echo $sno?> </td>
													<td>
                                                    	<strong><?php echo $key->project_name;?></strong>
														<p class="font-yellow-gold"><i class='fa fa-clock-o'></i> <?php echo date("d, M-Y h:i a", strtotime($key->project_add_date));?></p>
													</td>
													<td>
														<?php echo $key->project_client_name?>
                                                        <br>
                                                        <span class="label label-sm label-green"><?php echo $key->project_client_email;?></span>
														<p class="font-yellow-gold"><i class='fa fa-mobile-phone'></i> <?php echo $key->project_client_contact_no?></p>
													</td>
													<td>
														<?php echo $key->user_name?>
													</td>
													<td>
                                                    	<?php echo $key->project_status_name?>
													</td>
													<td>
														
													</td>
                                                    <td>
                                                    <a href="<?php echo base_url()?>leads/details/<?php echo $this->common_model->id_encrypt($key->project_id);?>"><span class="label label-sm label-primary" title="Click here fot lead details"><i class="fa fa-pencil"></i> Edit</span></a>
                                                    <a href="<?php echo base_url()?>leads/details/<?php echo $this->common_model->id_encrypt($key->project_id);?>"><span class="label label-sm label-purple" title="Click here fot lead details"><i class="fa fa-search"></i> View</span></a>
                                                    </td>
												</tr>
                                                <?php if(!empty($key->project_platforms)){?>
                                                <tr>
                                                	<td>&nbsp;</td>
                                                    <td colspan="6">
                                                    <?php $key->project_platforms=substr("$key->project_platforms", 0, -1);
													$key->project_platforms=substr("$key->project_platforms", 1);
													$x=explode(',-', $key->project_platforms);
													foreach($x as $x_value)
													{
														$platform=$this->common_model->getDataField('platform_name,platform_class','platforms
',array('platform_id'=>$x_value));?>
                                                    <a href="javascript:;"><span class="label label-sm label-<?php echo $platform[0]->platform_class?>"><?php echo $platform[0]->platform_name?></span></a>
                                                    <?php } ?>
                                                    </td>
                                                </tr>
												<?php
												}
											}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <?php include("sidebar_right.php"); ?>
            <!-- END QUICK SIDEBAR -->
        </div>
        <!--start  Modal  popup to update status-->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Assign Service Center to Order - </h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo base_url()?>order/asign_center">
                  <div class="col-md-8" style="padding-top:10px;padding-bottom:10px">
                  	<input type="hidden" name="order_id" id="order_id">
                    <input type="hidden" name="center_id" id="center_id">
                    <input type="hidden" name="redirect_to" id="redirect_to" value="<?php echo $this->uri->segment(2);?>">
                    <select name="pass_center"id="pass_center" class="form-control" required>
                      <option value="">Select</option>
                    </select>
                  </div>
                  <div class="col-md-4" style="padding-top:10px;padding-bottom:10px">
                    <button name="submit_update" type="submit" class="btn green uppercase" value="Assign Service Center">Assign Service Center</button>
                  </div>
                </form>
              </div>
              <div class="modal-footer"></div>
            </div>
          </div>
        </div>
		<!--end  Modal  popup to update status-->
        
        <!--start  Modal order remark to update-->
        <div id="myModal_remark" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Remark to Order - </h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo base_url()?>order/update_remark">
                  <div class="col-md-8" style="padding-top:10px;padding-bottom:10px">
                  	<input type="hidden" name="order_id2" id="order_id2">
                    <input type="hidden" name="redirect_to2" id="redirect_to2" value="<?php echo $this->uri->segment(2);?>">
                    <textarea name="order_remark" id="order_remark" class="form-control" required></textarea>
                  </div>
                  <div class="col-md-4" style="padding-top:10px;padding-bottom:10px">
                    <button name="submit_remark" type="submit" class="btn green uppercase" value="Update">Submit Remark</button>
                  </div>
                </form>
              </div>
              <div class="modal-footer"></div>
            </div>
          </div>
        </div>
		<!--end Modal order remark to update-->
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include("footer.php"); ?>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>
<script>
  function asign_center(id1,id2,city_id)
  {
	  $('#order_id').val(id1);
	  $('#center_id').val(id2);
	str = 'city_id='+city_id+"&id2="+id2;
	//alert(str)
	$.ajax({
        type:"POST",
        url:"<?php echo base_url('order/add_center_filter');?>",
        data:str,
        success:function(data)
		{
                          //alert(data);
                            $('#pass_center').empty();
                            $('#pass_center').append('<option value="" >Select</option>');
                            $('#pass_center').append(data);
        }
    });
  }
</script>
<script>
  function update_order(id1)
  {
	$('#order_id2').val(id1);
	str = 'id1='+id1;
	//alert(str)
	$.ajax({
        type:"POST",
        url:"<?php echo base_url('order/add_remark');?>",
        data:str,
        success:function(data)
		{
			$("#order_remark").val(data);
            //alert(data);
        }
    });
  }
</script>
<script type="text/javascript">
function trim(value) {
  value = value.replace(/^\s+/,'');
  value = value.replace(/\s+$/,'');
  return value;
}
</script>