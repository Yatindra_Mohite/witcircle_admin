 <div class="page-sidebar-wrapper">
                    
                    <div class="page-sidebar navbar-collapse collapse">
                    

                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                           
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                           <!-- <li class="sidebar-search-wrapper">
                                <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>
                            </li> -->
                        <?php $role = $this->session->userdata('role'); 
                            if($role=='admin')
                            { ?>
                                <li class="nav-item start <?php if($this->uri->segment(1)=='dashboard'){ echo 'active';} ?>">
                                <a href="<?php echo base_url('dashboard');?>" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <!--<span class="arrow open"></span> -->
                                </a>
                               <!--<ul class="sub-menu">
                                    <li class="nav-item start active open">
                                        <a href="index.html" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Dashboard 1</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                </ul> -->
                            <!-- ****************************************end*********************************** -->    

                            <li class="nav-item  <?php if($this->uri->segment(1)=='course'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Course</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='course' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('course');?>" class="nav-link">
                                            <span class="title">Add Course</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='course' && $this->uri->segment(2)=='course_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('course/course_list');?>" class="nav-link">
                                            <span class="title">Course list</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- ****************************************end*********************************** -->    

                            <li class="nav-item  <?php if($this->uri->segment(1)=='subject'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Subject</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='subject' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('subject');?>" class="nav-link">
                                            <span class="title">Add Subject</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='subject' && $this->uri->segment(2)=='subject_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('subject/subject_list');?>" class="nav-link">
                                            <span class="title">Subject List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                             <!-- ****************************************end*********************************** -->
                            <li class="nav-item  <?php if($this->uri->segment(1)=='chapter'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Chapter</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='chapter' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('chapter');?>" class="nav-link">
                                            <span class="title">Add Chapter</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='chapter' && $this->uri->segment(2)=='chapter_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('chapter/chapter_list');?>" class="nav-link">
                                            <span class="title">Chapter List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                             <!-- ****************************************end*********************************** -->
                            <li class="nav-item  <?php if($this->uri->segment(1)=='MUSP'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-list-alt"></i>
                                    <span class="title">Full Test</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='MUSP' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('MUSP');?>" class="nav-link">
                                            <span class="title">Add Full Test</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='MUSP' && $this->uri->segment(2)=='musp_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('MUSP/musp_list');?>" class="nav-link">
                                            <span class="title">Full Test List</span>
                                        </a>
                                    </li>
                                    <!-- <li class="nav-item <?php if($this->uri->segment(1)=='MUSP' && $this->uri->segment(2)=='add_question'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('MUSP/add_question');?>" class="nav-link">
                                            <span class="title">Add Question</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='automated_fulltest'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-list-alt"></i>
                                    <span class="title">Automated Full Test</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='automated_fulltest' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('automated_fulltest/add_automated_full_test');?>" class="nav-link">
                                            <span class="title">Add Automated Full Test</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='automated_fulltest' && $this->uri->segment(2)=='musp_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('automated_fulltest/automated_test_list');?>" class="nav-link">
                                            <span class="title">Automated Full Test List</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='automated_fulltest' && $this->uri->segment(2)=='generated_test_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('automated_fulltest/generated_test_list');?>" class="nav-link">
                                            <span class="title">Generated Test List</span>
                                        </a>
                                    </li>
                                    <!-- <li class="nav-item <?php// if($this->uri->segment(1)=='automated_fulltest' && $this->uri->segment(2)=='add_question'){ echo 'active';}?> ">
                                        <a href="<?php //echo base_url('MUSP/add_question');?>" class="nav-link">
                                            <span class="title">Add Question</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </li>
                              <!-- ****************************************end*********************************** -->
                            <li class="nav-item  <?php if($this->uri->segment(1)=='sectional_test'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-list-alt"></i>
                                    <span class="title">Sectional Test</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='sectional_test' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('sectional_test');?>" class="nav-link">
                                            <span class="title">Add Test</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='sectional_test' && $this->uri->segment(2)=='sectional_test_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('sectional_test/sectional_test_list');?>" class="nav-link">
                                            <span class="title">Test List</span>
                                        </a>
                                    </li>
                                   
                                </ul>
                            </li>
                                                        
                            <li class="nav-item  <?php if($this->uri->segment(1)=='marking_scheme'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-th-list"></i>
                                    <span class="title">Marking Scheme</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='marking_scheme' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('marking_scheme');?>" class="nav-link">
                                            <span class="title">Add Scheme</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='marking_scheme' && $this->uri->segment(2)=='scheme_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('marking_scheme/scheme_list');?>" class="nav-link">
                                            <span class="title">Scheme List</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>
                            <!-- ****************************************end*********************************** -->
                            <li class="nav-item  <?php if($this->uri->segment(1)=='coaching'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="title">Coaching</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='coaching' && $this->uri->segment(2)==''){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('coaching');?>" class="nav-link">
                                            <span class="title">Coaching List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                             <li class="nav-item  <?php if($this->uri->segment(1)=='student'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="title">Students</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='student' && $this->uri->segment(2)==''){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('student');?>" class="nav-link">
                                            <span class="title">Student List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='incomplete_registration'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-remove-sign"></i>
                                    <span class="title">Incomplete Registration</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='incomplete_registration' && $this->uri->segment(2)==''){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('incomplete_registration');?>" class="nav-link">
                                            <span class="title">Users List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <!-- witcircle_admin/in_short/subject_list -->
                            <!-- witcircle_admin/add_ws/add_courses -->
                            <li class="nav-item  <?php if($this->uri->segment(2)=='add_courses' || $this->uri->segment(2)=='courses_list' || ($this->uri->segment(2)=='subject_list' && $this->uri->segment(1)!='subject' )|| ($this->uri->segment(2)=='chapter_list' && $this->uri->segment(1)!='chapter' ) ){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-tasks"></i>
                                    <span class="title">Subjective & Witcard</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='add_ws' && $this->uri->segment(2)=='add_courses'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('add_ws/add_courses');?>" class="nav-link">
                                            <span class="title">Add Course</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='add_ws' && $this->uri->segment(2)=='courses_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('add_ws/courses_list');?>" class="nav-link">
                                            <span class="title">Course List</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='subject_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('in_short/subject_list');?>" class="nav-link">
                                            <span class="title">Subject List</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='chapter_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('in_short/chapter_list');?>" class="nav-link">
                                            <span class="title">Chapter List</span>
                                        </a>
                                    </li>
                                </ul>
                                <!-- <ul class="sub-menu">
                                    
                                </ul> -->
                            </li>
                            
                            <li class="nav-item  <?php if($this->uri->segment(1)=='subjective'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-phone"></i>
                                    <span class="title">Subjective</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='subjective' && $this->uri->segment(2)=='add_exam_paper'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('subjective/add_exam_paper');?>" class="nav-link">
                                            <span class="title">Add Exam Paper</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='subjective' && $this->uri->segment(2)=='exam_paper_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('subjective/exam_paper_list');?>" class="nav-link">
                                            <span class="title">Exam Paper List</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='subjective' && $this->uri->segment(2)=='add_content'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('subjective/add_content');?>" class="nav-link">
                                            <span class="title">Add Content</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='subjective' && $this->uri->segment(2)==''){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('subjective/content_list');?>" class="nav-link">
                                            <span class="title">Content List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='in_short' || $this->uri->segment(2)!='' ){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-phone"></i>
                                    <span class="title">Wit Card</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    
                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='news_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('in_short/news_list');?>" class="nav-link">
                                            <span class="title">News List</span>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='current_affairs_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('in_short/current_affairs_list');?>" class="nav-link">
                                            <span class="title">Current Affairs List</span>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='study_zone_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('in_short/study_zone_list');?>" class="nav-link">
                                            <span class="title">Study Zone List</span>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='gk_card_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('in_short/gk_card_list');?>" class="nav-link">
                                            <span class="title">General knowledge List</span>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='gk_chapter_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('in_short/gk_chapter_list');?>" class="nav-link">
                                            <span class="title">GK Chapters</span>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='witcard_price'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('in_short/witcard_price');?>" class="nav-link">
                                            <span class="title">Witcard Price</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='advertisement'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-blackboard"></i>
                                    <span class="title">Advertisement</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    
                                    <li class="nav-item <?php if($this->uri->segment(1)=='advertisement' && $this->uri->segment(2)=='advertisement_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('advertisement/advertisement_list');?>" class="nav-link">
                                            <span class="title">Advertisement List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='rank'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-star"></i>
                                    <span class="title">Rank</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='rank' && $this->uri->segment(2)=='add_rank'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('rank/add_rank');?>" class="nav-link">
                                            <span class="title">Add Rank</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='in_short' && $this->uri->segment(2)=='chapter_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('rank');?>" class="nav-link">
                                            <span class="title">Rank List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                             <li class="nav-item  <?php if($this->uri->segment(1)=='promo_code'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="fa fa-barcode"></i>
                                    <span class="title">Promo Code</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='promo_code' && $this->uri->segment(2)=='add_rank'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('promo_code/add_promo_code');?>" class="nav-link">
                                            <span class="title">Add Promo Code</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='promo_code' && $this->uri->segment(2)=='chapter_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('promo_code/promo_code_list');?>" class="nav-link">
                                            <span class="title">Promo Code List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='Faq'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-align-left"></i>
                                    <span class="title">FAQ</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='faq' && $this->uri->segment(2)=='add_faq'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('faq/add_faq');?>" class="nav-link">
                                            <span class="title">Add FAQ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='faq' && $this->uri->segment(2)=='faq_list'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('faq/faq_list');?>" class="nav-link">
                                            <span class="title">FAQ List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='subscription_model'){ echo 'active';}?>">
                                <a href="<?php echo base_url('subscription_model');?>" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-usd"></i>
                                    <span class="title">Subscription Model</span>
                                    <!-- <span class="arrow"></span> -->
                                </a>
                            </li>

                            <li class="nav-item  <?php if($this->uri->segment(1)=='employee'){ echo 'active';}?>">
                                <a href="#" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="title">Employee</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(2)=='add_employee' && $this->uri->segment(2)=='add_faq'){ echo 'active';}?>">
                                        <a href="<?php echo base_url('employee/add_employee');?>" class="nav-link">
                                            <span class="title">Add Employee</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='employee' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('employee');?>" class="nav-link">
                                            <span class="title">Employee List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php }else
                        {
                            $menuid= $this->session->userdata("menu");
                            $submenuid= $this->session->userdata("sub_menu");

                            $getmenudata = $this->db->query("SELECT sidebar_menu.url,sidebar_menu.menu_id,sidebar_menu.page_name,sidebar_menu.menu_name,GROUP_CONCAT(sidebar_sub_menu.sub_menu_id) as subid FROM sidebar_menu LEFT JOIN sidebar_sub_menu ON (sidebar_menu.menu_id = sidebar_sub_menu.menu_id AND sidebar_sub_menu.status = 1 ) WHERE sidebar_menu.menu_id IN ($menuid) AND sidebar_menu.status = 1 GROUP BY sidebar_menu.menu_id ORDER BY sidebar_menu.order_id ASC")->result();
                        //echo $this->db->last_query();exit;
                            if(!empty($getmenudata))
                            {
                                foreach ($getmenudata as $key) { ?>
                                    
                                    <li class="nav-item  <?php if($this->uri->segment(1)==$key->page_name){ echo 'active';}?>">
                                        <a href="<?php if($key->url!=''){ echo base_url($key->url); }else{ echo '#'; }?>" class="nav-link nav-toggle">
                                            <i class="glyphicon glyphicon-user"></i>
                                            <span class="title"><?php echo $key->menu_name; ?></span>
                                            <?php if($key->url == ''){ echo '<span class="arrow"></span>'; } ?>
                                            </a>
                                            <?php if($key->subid != 'NULL' && !empty($submenuid))
                                            {
                                                $submenudata = $this->db->query("SELECT sidebar_sub_menu.sub_menu_id,sidebar_sub_menu.sub_menu_name,sidebar_sub_menu.page_name,sidebar_sub_menu.url FROM sidebar_sub_menu WHERE sidebar_sub_menu.menu_id = ".$key->menu_id." AND sidebar_sub_menu.sub_menu_id IN (".$submenuid.") AND sidebar_sub_menu.status = 1 AND sub_menu_id NOT IN (33,34,35,36,37,38,39) ORDER BY sidebar_sub_menu.order_id ASC")->result();
                                                if(!empty($submenudata))
                                                { ?>
                                                    <ul class="sub-menu">
                                                    <?php 
                                                    foreach ($submenudata as $value) { ?>

                                                        <li class="nav-item <?php if($this->uri->segment(1)== $value->page_name){ echo 'active';}?>">
                                                            <a href="<?php echo base_url($value->url);?>" class="nav-link">
                                                                <span class="title"><?php echo $value->sub_menu_name; ?></span>
                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                    </ul>
                                                <?php }
                                            }
                                        ?>
                                    </li>

                                <?php }
                            }
                        } 
                        



                        ?>    
                           
                        </ul>
                    </div>
                    <!-- END SIDEBAR -->
                </div>