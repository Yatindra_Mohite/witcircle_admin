<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Add Course</title>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Course
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Course</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Course</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" onsubmit="myfun()" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Course<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Course name" name="course_name" class="form-control" data-parsley-pattern="/^[a-zA-Z\s]*$/" data-parsley-required-message="course name is required"  required/>
                        </div>
                    </div>
                </div>

                 <div id="b" class="form-body bloack">
                    <div class="form-group">
                        <label class="control-label col-md-3">Add Couser Type <span class="required">*</span></label>
                        <div class="col-md-8">
                                <input id="b2" type="checkbox" name="type_id1"  value="2" > Subjective  &nbsp; &nbsp; &nbsp; <input id="b1" type="checkbox" name="type_id2" value="1" > Wit cards   
                        </div>
                    </div>

                    <div id="h" class="form-group" style="display: none">
                        <label class="control-label col-md-3">Subjective coin<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input id="h1" type="number" placeholder="Add coin" name="sub_coin" class="form-control"/>
                        </div>
                    </div>

                    <div id="f" class="form-group" style="display: none">
                        <label class="control-label col-md-3">Wtc Card Coin<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input id="f1" type="number" placeholder="Add coin" name="witcard_coin" class="form-control"/>
                        </div>
                    </div>
               </div>


                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>add_ws/add_courses"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script type="text/javascript">
    function myfun(){

    if($('input:checkbox[name=type_id1]').is(':checked') || $('input:checkbox[name=type_id2]').is(':checked')){
      
    }else{
      alert("please select course type");
     return false;   
    }
    //var a = $("#b1").val();
    //var b = $("#b2").val();
    
    //alert(a);
    //alert(b);

        // if (a  == '' || b == '') {
        //     
        //     return false;
        // } 

    }
</script>
<script type="text/javascript">
        function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('Add_ws/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }location.reload();
        }
    </script>
    
    <script type="text/javascript">

    $(document).ready(function(){
        $('#b2').click(function(){
            if($(this).prop("checked") == true){
                $("#h").css("display", "block");
                $('#h1').attr("required", "required");
            }
            else if($(this).prop("checked") == false){
                $("#h").css("display", "none");
                $('#h1').removeAttr('required');
            }
        });


        $('#b1').click(function(){
            if($(this).prop("checked") == true){
                $("#f").css("display", "block");
                $('#f1').attr("required", "required");
            }
            else if($(this).prop("checked") == false){
                $("#f").css("display", "none");
                $('#f1').removeAttr('required');
            }
        });
    });
  
        
//$('#multi,#simple,#for_comprehension1,#integer').removeAttr('required');
$('#integer').attr("required", "required");

</script>



</body>



</html>