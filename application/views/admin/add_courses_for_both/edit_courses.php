<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Edit Course</title>
    <style type="">
        
        input[type="checkbox"][readonly] {
          pointer-events: none;
        }
    </style>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Course
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Course</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Course</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('add_ws/edit/'.$this->common_model->id_encrypt($course_edit->id));?>" id="form11" class="form-horizontal form-row-seperated" onsubmit="return myfun()" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">

                    <div class="form-group">
                        <label class="control-label col-md-3">Course<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Course name" name="course_name" class="form-control" data-parsley-pattern="/^[a-zA-Z\s]*$/" data-parsley-required-message="course name is required" value="<?php echo $course_edit->name;?>" required/>
                        </div>
                    </div>
                </div>

                 <div id="b" class="form-body bloack">
                    <div class="form-group">
                        <label class="control-label col-md-3">Add Couser Type  <span class="required">*</span></label>
                        <div class="col-md-8">
                                <input type="checkbox" name="type_id1"  value="2" readonly="readonly" <?php  if($course_edit->subjective_id > 0){echo "checked";} ?> > Subjective  &nbsp; &nbsp; &nbsp; <input readonly="readonly" type="checkbox" name="type_id2" value="1" <?php  if($course_edit->witcard_id > 0){echo "checked";} ?> > Wit cards   
                        </div>
                    </div>
               </div>


                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>add_ws/add_courses"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script type="text/javascript">
    function myfun(){

    if($('input:checkbox[name=type_id1]').is(':checked') || $('input:checkbox[name=type_id2]').is(':checked')){
      
    }else{
      alert("please select course type");
     return false;   
    }
    //var a = $("#b1").val();
    //var b = $("#b2").val();
    
    //alert(a);
    //alert(b);

        // if (a  == '' || b == '') {
        //     
        //     return false;
        // } 

    }
</script>
<script type="text/javascript">
        function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('Add_ws/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }location.reload();
        }
    </script>

</body>

</html>