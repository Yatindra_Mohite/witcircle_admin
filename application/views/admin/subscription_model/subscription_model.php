<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Subscription Model</title>
    <style>
    .right-side.pull-right input[type="checkbox"]{
       width: 20px; /*Desired width*/
       height: 20px; /*Desired height*/
       position: relative;
       top: 5px;
}
</style>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Subscription Model
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Subscription Model</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Subscription Model (Plan One)</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('subscription_model/one') ?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label col-md-3">Name<span class="required" required > * </span></label>
                        <div class="col-md-9">
                             <input type="text" placeholder="name" name="name" value="<?php echo $sub->subscription_name?>" class="form-control" data-parsley-required-message="name is required"  required/>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Price<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="number" placeholder="price" name="price" value="<?php echo $sub->subscription_amount?>" class="form-control" data-parsley-required-message="price is required"  required/>
                        </div>
                    </div>
                </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required">*</span></label>
                        <div class="col-md-9">
                            <textarea name="text" placeholder="Write here"  class="form-control" required=""><?php echo $sub->subscription_desc?></textarea>
                        </div>
                    </div>
                </div>
                </div>
            </div>  

                <div class="form-actions text-left">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <div class="right-side pull-right">Active/Inactive &nbsp; &nbsp;<input id="check1" onClick="change()" value="<?php echo $sub->status ?>" type="checkbox" name="sub" <?php if($sub->status == 1){echo "checked";}?> ></div>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Subscription Model (Plan Two)</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success2'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success2'),"</div>"; 
           }
           if($this->session->flashdata('failed2'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed2'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('subscription_model/two')?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label col-md-3">Name<span class="required" required > * </span></label>
                        <div class="col-md-9">
                             <input type="text" placeholder="name" name="name2" class="form-control" value="<?php echo $sub2->subscription_name?>" data-parsley-required-message="name is required"  required/>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Price<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="number" placeholder="price" name="price2" class="form-control" value="<?php echo $sub2->subscription_amount?>" data-parsley-required-message="price is required"  required/>
                        </div>
                    </div>
                </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required">*</span></label>
                        <div class="col-md-9">
                            <textarea name="text2" placeholder="Write here" class="form-control" required=""><?php echo $sub2->subscription_desc?></textarea>
                        </div>
                    </div>
                </div>
                </div>
            </div>  

                <div class="form-actions text-left">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <div class="right-side pull-right">Active/Inactive &nbsp; &nbsp;<input type="checkbox"  name="sub"  <?php if($sub2->status == 1){echo "checked";}?> ></div>

                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Subscription Model (Plan Three)</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success3'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success3'),"</div>"; 
           }
           if($this->session->flashdata('failed3'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed3'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('subscription_model/three')?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label col-md-3">Name<span class="required" required > * </span></label>
                        <div class="col-md-9">
                             <input type="text" placeholder="name" name="name3" class="form-control" value="<?php echo $sub3->subscription_name?>" data-parsley-required-message="name is required"  required/>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Price<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="number" placeholder="price" name="price3" class="form-control" value="<?php echo $sub3->subscription_amount?>" data-parsley-required-message="price is required"  required/>
                        </div>
                    </div>
                </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required">*</span></label>
                        <div class="col-md-9">
                            <textarea name="text3" placeholder="Write here" class="form-control" required=""><?php echo $sub3->subscription_desc?></textarea>
                        </div>
                    </div>
                </div>
                </div>
            </div>  

                <div class="form-actions text-left">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <div class="right-side pull-right">Active/Inactive &nbsp; &nbsp;<input type="checkbox"  name="sub"  <?php if($sub3->status == 1){echo "checked";}?> ></div>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>
<script type="text/javascript">
      function change(){
          var a = $('#check1').val();
          if(a == 1){
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                var str = "status_id="+'0';

                $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>subscription_model/update_data/",
                    data:str,
                    success:function(data)
                    { 
                     //alert(data);
                      alert('Inactived');
                    }
                });

                $('#check1').val('0');
            }else{
                 location.reload();
            }
             
          }else{
            var r = confirm('Are you really want to change status?');
            if(r==true)
            { 
                var str = "status_id="+'1';

                $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>subscription_model/update_data/",
                    data:str,
                    success:function(data)
                    { 
                     //alert(data);
                      alert('Activated');

                    }
                });
                $('#check1').val('1');
            }else{
                location.reload();
            }
          }

      }
</script>


</body>

</html>