<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Course
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Course</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Course</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('course/edit/'.$this->common_model->id_encrypt($course_edit->id));?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Add Cource<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Cource Name" name="course_name" class="form-control" value="<?php echo ucwords($course_edit->course_name);?>" data-parsley-required-message="Course name is required"  required/>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Full Test Price<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Price" name="price" value="<?php echo $course_edit->course_coin;?>" class="form-control" data-parsley-type="number" data-parsley-required-message="Price is required"  required/>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Subjective Price<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Subjective Price" value="<?php echo $course_edit->subjective_coin;?>" name="subjective_price" data-parsley-type="number" class="form-control" data-parsley-required-message="Price is required"  required/>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Sectional Price<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Sectional Price" value="<?php echo $course_edit->sectional_price;?>" name="sectional_price" data-parsley-type="number" class="form-control" data-parsley-required-message="Price is required"  required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                      <label class="control-label col-md-3">Per Question Time (IN MIN)</label>
                      <div class="col-md-6">
                          <input class="form-control" name="question_time" value="<?php echo $course_edit->time_per_question;?>" data-parsley-type="number" maxlength="8" type="text" data-parsley-required-message="Question time is required" />
                      </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Total Course Time<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Course time" name="course_time" value="<?php echo $course_edit->total_course_time;?>" data-parsley-type="number" class="form-control" data-parsley-required-message="time is required"  required/>
                           
                        </div>
                    </div>
                </div>

                 <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Sectional Test Positive Mark<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="1.00" value="<?php echo $course_edit->sectional_positive_mark;?>" data-parsley-pattern="[0-9]*(\.?[0-9]{2}$)" name="sectional_positive_mark" class="form-control" data-parsley-required-message="Positive mark is required"  required/>
                           
                        </div>
                    </div>
                </div>

                 <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Sectional Test Negative Mark<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="0.25" value="<?php echo $course_edit->sectional_negative_mark;?>" data-parsley-pattern="[0-9]*(\.?[0-9]{2}$)" name="sectional_negative_mark" class="form-control" data-parsley-required-message="Negative mark is required"  required/>
                           
                        </div>
                    </div>
                </div> 
              

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

        
    </body>

</html>