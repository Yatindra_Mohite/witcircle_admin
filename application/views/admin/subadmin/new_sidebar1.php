 <div class="page-sidebar-wrapper">
                    
                    <div class="page-sidebar navbar-collapse collapse">
                       
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                           
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                           <!-- <li class="sidebar-search-wrapper">
                                <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>
                            </li> -->
                            
                            <li class="nav-item  <?php if($this->uri->segment(1)=='MUSP'){ echo 'active';}?>">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="title">Full Test</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(1)=='MUSP' && $this->uri->segment(2)==''){ echo 'active';}?>">
                                        <a href="<?php echo base_url('subadmin/MUSP');?>" class="nav-link">
                                            <span class="title">Add Full Test</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(1)=='MUSP' && $this->uri->segment(2)=='musp_list'){ echo 'active';}?> ">
                                        <a href="<?php echo base_url('subadmin/MUSP/musp_list');?>" class="nav-link">
                                            <span class="title">Full Test List</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>    
                        </ul>
                    </div>
                    <!-- END SIDEBAR -->
                </div>