<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/subadmin/head.php"); ?>
    <title>Edit Full Test</title> 
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/subadmin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/subadmin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Full Test
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>subadmin/dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Full Test</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Full Test</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('subadmin/MUSP/edit/'.$this->common_model->id_encrypt($musp_edit->id));?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Test Description<span class="required">*</span></label>
                        <div class="col-md-6">
                            <textarea class="form-control" placeholder="Text Description" name="text_desc" cols="5" rows="5"><?php echo $musp_edit->test_desc;?></textarea>
                        </div>
                    </div>
                </div>

                 <div class="form-group">
                  <label class="control-label col-md-3">Full Test name<span class="required">*</span></label>
                    <div class="col-md-6">
                          <input type="text" placeholder="MUSP test name" class="form-control" value="<?php echo $musp_edit->test_name?>" name="test_name" data-parsley-required-message="test name is required"  required>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">No of Question<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="no of Question" name="no_of_question"  value="<?php echo $musp_edit->no_of_question ?>" class="form-control" data-parsley-required-message="nomber of question name is required"  required/>
                             <?php echo form_error('b_name', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green"  id="x" name="submit" value="Submit" >
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/subadmin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>


<script>
function getsubject(course_id)
 { 
   var str = "course_id="+course_id;
   //alert(str);

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>subadmin/MUSP/get_subject/",
        data:str,
        success:function(data)
        { 
          //alert(data);
          $('#subject').empty();
          if(data == 'You have no subject for this course select other') {
           $("#x").attr('disabled', 'disabled');
           $('#subject').append("<h5 class='control-label col-md-12'>"+data+"</h5>");
          }else
          {
          $('#subject').append(data);
          $("#x").removeAttr('disabled');
          }
          
        }
   });
   }
 </script>

    </body>

</html>