<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Question List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/subadmin/head.php"); ?>
        <style>
                    .answer-td p{
                        display: inline-block;
                        margin-left: 15px;
                    }
                </style>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/subadmin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/subadmin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Question List
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>subadmin/dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Question List</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }
                                 $cid = $this->uri->segment(5);
                                 
                                ?>
                            
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Question List</div>
                                        <div class="col-md-4" style="padding-top: 3px; float: right;">
                                        <select  class="form-control" id="subject_filter">
                                                <option value="">All Subject</option>
                                                <?php foreach($sub_list as $count): ?>
                                                    <option value="<?php echo $count->subject_id ;?>"<?php if($count->subject_id==$cid){ echo "selected"; } ?>><?php echo $count->subject_name; ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                        </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Questions </center></th>
                                                <th><center>Question Type</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Questions</center></th>
                                                <th><center>Question Type</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($que_list))
                                        {   $i = 0;
                                            foreach($que_list as $key)
                                            { 
                                                $i++;

                                              ?>
                                            <tr>    <?php $a = json_decode($key->options); ?>
                                                    <td><center><?php echo $i;?></center></td>
                                                    <td class="answer-td"><center><?php  echo $key->question;?></center>
                                                    <b>Options - </b><?php if (!empty($a[0])) { echo "".$a[0]->option; } ?>
                                                    <?php if (!empty($a[1])) { echo ",".$a[1]->option; } ?>
                                                    <?php if (!empty($a[2])) { echo ",".$a[2]->option; } ?>
                                                    <?php if (!empty($a[3])) { echo ",".$a[3]->option; } ?>
                                                    <?php if (!empty($a[4])) { echo ",".$a[4]->option; } ?>
                                                    <?php if (!empty($a[05])) { echo ",".$a[5]->option; } ?><br>
                                                    <b>Answer - <?php echo $key->answer?> </b>
                                                    </td>
                                                    <td><center><?php if($key->question_type == 4){echo "Single";} elseif($key->question_type == 5){echo "Multiple";} elseif($key->question_type == 2){echo "Integer";}?></center></td>     
                                                    <td><center><a href="<?php echo base_url('subadmin/MUSP/edit_question/'.$this->uri->segment(4).'/'.$this->common_model->id_encrypt($key->id));?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span></center></td>    
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/subadmin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script type="text/javascript">
       $(document).ready(function()
       {    
           $("#subject_filter").change(function()
           {
               var subject_id = $('#subject_filter').val();
               
               document.location.href =  "<?php echo base_url('subadmin/MUSP/according_to_subject_list')?>/"+<?php echo $this->uri->segment(3)?>+"/"+subject_id
           }); 
       });
    </script>



     


