<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Add Section</title>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Section</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('')?>dashboard">Home</a>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Section (<?php $subject = $this->common_model->common_getRow('course_subject',array('id'=>$this->uri->segment(4))); echo ucwords($subject->subject_name);?>)</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
            if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                   <div class="form-group">
                        <label class="control-label col-md-3">Section Name <span class="required">*</span></label>
                        <div class="col-md-6">
                             <input type="text" placeholder="Section name" name="section_name" required class="form-control" />
                        </div>
                   </div>


                   <div class="form-body">
                    <div id="payment_type" class="form-group">
                        <label class="control-label col-md-3">Marking <span class="required"> * </span></label>
                         <?php $marking = $this->common_model->getData('marking',array('status'=>'1'));?>
                        <div class="col-md-5">
                            <select name="marking" class="form-control" required="">
                             <option value="">Select Marking</option>
                                <?php if(!empty($marking)){
                                                 foreach($marking as $key)
                                                 {?>
                                                 <option value="<?php echo $key->id;?>"><?php echo $key->name;?></option>
                                                <?php } 
                                            } ?>
                            </select>
                                
                            </select>
                        </div>
                    </div>

                    <div class="form-body">
                    <div id="payment_type" class="form-group">
                        <label class="control-label col-md-3">Question Type<span class="required"> * </span></label>
                         <?php $question_type = $this->common_model->getData('question_type',array('status'=>'1'));?>
                        <div class="col-md-5">
                            <select name="question_type" class="form-control" required="">
                             <option value="">Select Question Type</option>
                                <?php if(!empty($question_type)){
                                                 foreach($question_type as $key)
                                                 {?>
                                                 <option value="<?php echo $key->id;?>"><?php echo $key->name;?></option>
                                                <?php } 
                                            } ?>
                            </select>
                                
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">No. of Question<span class="required">*</span></label>
                        <div class="col-md-6">
                             <input type="text" data-parsley-type="digits" placeholder="No. of question" name="no_of_question" required class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Question start to<span class="required" > * </span></label>

                        <div class="col-md-3">
                          <div class="col-md-8">
                              <input type="text" placeholder="5" data-parsley-type="digits" name="starting_number" class="form-control" required="">
                          </div>
                        </div>

                        <label class="control-label col-md-2">Question end to<span class="required" > * </span></label>

                        
                        <div class="col-md-3">
                          <div class="col-md-8">
                              <input type="text" placeholder="15" data-parsley-type="digits" name="end_number" class="form-control" required="">
                              
                              <input type="hidden" name="course_id" value="<?php echo $this->uri->segment(3);?>" >
                              <input type="hidden" name="subject_id" value="<?php echo $this->uri->segment(4);?>" >
                          </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                        </div>
                    </div>
                </div>

            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>


  </script>

        
  </body>

</html>