    <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Generated Test List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
    
        <div class="page-header navbar navbar-fixed-top" id="myid">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Generated Test List
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Generated Test List</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Generated Test List</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Course Name </center></th>
                                                <th><center>Update</center></th>
                                                <th><center>No of Question </center></th>
                                                <th><center>Question List</center></th>
                                                <th><center>Status</center></th>
                                                
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Course Name </center></th>
                                                <th><center>Update</center></th>
                                                <th><center>No of Question </center></th>
                                                <th><center>Question List</center></th>
                                                <th><center>Status</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <div id="myDiv">
                                            <img id="loading-image" src="<?php echo base_url().'uploads/loader.gif'; ?>" style="display: none; width:50px; height: 50px; margin-left: 40%; position: absolute; margin-top: -1%;"/>
                                        </div>    
                                        <?php 
                                        if(!empty($generated_test_list))
                                        {   $i = 0;
                                            foreach($generated_test_list as $key)
                                            { 
                                                $i++;

                                              ?>
                                            <tr>
                                                    <td><center><?php echo $i;?></center></td>
                                                    <td><center><?php  echo $key->test_name;?></center></td>
                                                    <td><center><!-- <a href="<?php //echo base_url('automated_fulltest/automated_subject_list/'.$key->course_id);?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></a> -->
                                                    <a href="#add_basic" onclick="update_data('<?php echo $key->id;?>','<?php echo $key->test_name;?>','<?php echo $key->organised_date;?>','<?php echo $key->test_desc;?>',)" data-toggle="modal" aria-hidden="true" title="click here to Add" type="button" id="" class="btn green">Update</a>
                                                    </center></td> 
                                                    <td><center><?php echo $key->no_of_question;?></center></td>
                                                    <td><center><a href="<?php echo base_url('automated_fulltest/question_list/'.$key->id);?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></a></center></td>
                                                    <td><center><select style="" class="form-control" onchange="changestatus('<?php echo $key->id;?>',this.value)">
                                                    <option value="1"<?php if($key->status==1){ echo 'selected';} ?>> Active </option>
                                                    <option value="0"<?php if($key->status==0){ echo 'selected';} ?>> Inactive </option>
                                                    </select></center></td>
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>

        <div class="modal fade" id="add_basic" tabindex="-1" role="add_basic" aria-hidden="true">
             <form action="<?php echo base_url('automated_fulltest/update_test')?>" method="post"> 
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Update Test Details</h4>
                      </div>
                      <div class="portlet-body form">
                      
                      <div class="modal-body"> 
                            <div class="form-body">
                              <div class="form-group">
                                  <label class="control-label col-md-3">Name<span class="required">*</span></label>
                                  <div class="col-md-9">
                                      <input type="hidden" name="test_id" id="test_id" value="" ">
                                      <input type="text" id="name" placeholder="name" value="" name="name" class="form-control"/>
                                      <span id="add_err" style="margin-left: 0px;list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></span>
                                  </div>
                              </div>
                            </div>
                            <div class="form-body">
                              <div class="form-group">
                                  <label class="control-label col-md-3">Organised Date<span class="required">*</span></label>
                                  <div class="col-md-9">
                                      <input type="text" id="date1" placeholder="0000-00-00" value="" name="o_date" class="form-control"/>
                                      
                                  </div>
                              </div>
                            </div>
                           <div class="form-body">
                              <div class="form-group">
                                  <label class="control-label col-md-3">Description<span class="required">*</span></label>
                                  <div class="col-md-9">
                                  <textarea  placeholder="description" id="desc" name="desc"  rows="5" cols="6" class="form-control"></textarea>
                                   <span id="add_err1" style="margin-left: 0px;list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></span>
                                  </div>
                              </div>
                          </div>
                            <div style="margin-left: 150px;margin-top:20px;display: inline-block;">
                            <div class="row" >
                                  <div class="col-md-offset-3 col-md-6">
                                      <input type="submit" class="btn green" id="add_submit" name="submit" value="Submit" >
                                  </div>
                            </div>
                          </div>
                      </div>
                    </div>
                   
                      <div class="modal-footer">
                      </div>
                  </div>
              </div>
             </form>
            </div>

      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
</html>

<script type="text/javascript">
       function update_data(id,name,date,desc){
           $('#test_id').val(id);
           $('#name').val(name);
           $('#date1').val(date);
           $('#desc').val(desc);

       }

       $( function() {
         $( "#date1" ).datepicker({
            changeMonth: true,
            changeYear: true,
         /*   yearRange: '1990:'+(new Date).getFullYear(),*/
            dateFormat:'yy-mm-dd',
            minDate: 0
         });
       } );

       function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('/MUSP/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }else{ location.reload();}
        }
</script>




     


