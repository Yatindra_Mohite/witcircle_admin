    <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Automated Test List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
    
        <div class="page-header navbar navbar-fixed-top" id="myid">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Automated Test List
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Automated Test List</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class="icon-bubbles font-blue"></i>
                                        <span class="caption-subject font-blue bold uppercase">Question List</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="portlet_comments_1">
                                            <!-- BEGIN: Comments -->
                                            <div class="mt-comments">
                                                <div class="mt-comment remove-hover">
                                                    <div class="mt-comment-img">
                                                        <img src="assets/pages/media/users/avatar1.jpg"> </div>
                                                    <div class="mt-comment-body">

                                                      <?php
                                                       //= $generated_test_list->questions_id);
                                                      //exit;
                                                      $listing = explode(',',$generated_test_list->questions_id);
                                                       $i = 0;

                                                       foreach ($listing as $key ) {
                                                        $i++;
                                                       //         
                                                        $ques = $this->common_model->common_getRow('questions',array('id'=>$key));
                                                        ///print_r($ques->options);
                                                        $a = json_decode($ques->options);

                                                        echo "<b> Que - </b>".$i."<br><br>"."<div>".$ques->question."</div>";?>
                                                        <b style="color: red";>Options - </b><?php if (!empty($a[0])) { echo "".$a[0]->option; } ?>
                                                        <?php if (!empty($a[1])) { echo $a[1]->option; } ?>
                                                        <?php if (!empty($a[2])) { echo $a[2]->option; } ?>
                                                        <?php if (!empty($a[3])) { echo $a[3]->option; } ?>
                                                        <?php if (!empty($a[4])) { echo $a[4]->option; } ?>
                                                        <?php if (!empty($a[5])) { echo $a[5]->option; } ?><br>
                                                        <b style="color: green;">Answer - <?php echo $ques->answer?> </b><hr style="height:1px;border:none;color:#333;background-color:#333;">
                                                        <?php  } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END: Comments -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>

      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script type="text/javascript">
       function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('subadmin/MUSP/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }else{ location.reload();}
        }
</script>




     


