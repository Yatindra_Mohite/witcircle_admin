
<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Automated Full Test</title>
    <!-- <link href="template/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" /> -->


    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  
        <link href="<?php echo base_url('')?>template/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('')?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <style type="text/css">
span.areaCheckbox {
  float:left;
   width: 33%;
   padding: 5px 0;
} 
span.noq {
   float:left;
   width: 33%;
   margin-bottom: 30px;
   padding-right: 20px;
} 
span.subTime {
   float:left;
   width: 33%;
    margin-bottom: 31px;
   padding-right: 20px;
}
.subTime input.from-control {
   width: 100%;
   padding: 5px;
}
span.noq input.from-control{
     width: 100%;
   padding: 5px;
}
li.parsley-required {
    position: absolute;
}
        </style>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Automated Full Test
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Automated Full Test</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Automated Full Test</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo  base_url('automated_fulltest/add_automated_full_test') ?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                     <?php $course = $this->db->query("SELECT * FROM course WHERE status = 1 ")->result(); ?>
                        <label class="control-label col-md-3">Select Course<span class="required">*</span></label>
                        <div class="col-md-6">
                            <select class="bs-select form-control" name="course_id" onchange="getSubject(this.value)" required>
                                 <option value="" >Select Course</option>
                                    <?php if(!empty($course)){
                                             foreach($course as $key)
                                             {?>
                                             <option value="<?php echo $key->id;?>"><?php echo ucwords($key->course_name);?></option>
                                            <?php } 
                                    } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Select subject<span class="required">*</span></label>
                        <div id="subject" class="col-md-8">
                            Select Course First 
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">No. of question(Course)<span class="required">*</span></label>
                        <div class="col-md-6">
                             <input type="number" id="no_ques" placeholder="No of question" min="1" name="no_of_question_course" required class="form-control" />
                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="control-label col-md-3">Course Time (min.)<span class="required">*</span></label>
                        <div class="col-md-6">
                             <input id="time" type="text" placeholder="Course Time" readonly="" name="time" required class="form-control" />
                        </div>
                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>automated_fulltest/add_automated_full_test"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>
<script>
$("#submit").click(function(e){
  //console.log('sdsd');
  var check_counting = 0; 
  var check_count  = $("input[type='checkbox']").length;
  //console.log(check_count);
  var isFilled = false;
  var req_num = [];
  var total = 0;  // total time
  var total_no = 0; /// for course total no of question


  $( ".new_class" ).each(function( index ) {
    
    var get_class = $(this).attr('class').split(/\s/);
    var final_class = get_class[2];
    
    if($( this ).context.checked == true ){

        $('.'+final_class).attr("required", "required");
        //console.log($('.req'+(index+1)));
        if ($('.req'+(index+1)).val() != '') {
          isFilled = true;
          var get_data =$('.req'+(index+1)).val();
          total = (+total) + (+get_data);



        }
        req_num.push($('.req'+(index+1)));
        //console.log($('.check'+(index+1)));

        var get_data_ques =$('.check_total'+(index+1)).val();
	          
        total_no = (+total_no) + (+get_data_ques);
        //console.log(total_no);


    }else{
        
        check_counting++;
        $('.'+final_class).removeAttr('required');       
    }
    
  });

  if (isFilled) {
    for (var i = 0; i < req_num.length; i++) {
      req_num[i].attr("required", "required");

    }
    if($('#time').val() != total){
          alert('Course time and subject time not matched.');
          e.preventDefault();
       }
  }else{
      $('.'+req_num[i]).removeAttr('required');       
  }

  if (check_count == check_counting) {
   // $('.'+final_class).removeAttr('required');
    alert('Select course and subject first');
    //return false;
    e.preventDefault();
  }

  if($('#no_ques').val() != total_no){
          alert('Total no. question (course) should be equal to the total no. of question(subject)');
          e.preventDefault();
       }

});
</script>

<script>
function getSubject(course_id)
 { 
 
   var str = "course_id="+course_id;
   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>automated_fulltest/get_subject/",
        data:str,
        success:function(data)
        { 
         
          $('#subject').empty();
          $('#subject').append(data);
          
        }
   });

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>automated_fulltest/get_course_time/",
        data:str,
        success:function(time)
        { 
          //alert(time);
          $('#time').empty();
          $('#time').val(time);
          
        }
   });
   }
 </script>
        

</body>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url('')?>template/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<script src="<?php echo base_url('')?>template/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>

</html>