<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Automated Subject List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
    
        <div class="page-header navbar navbar-fixed-top" id="myid">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Automated Subject List
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Automated Subject List</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Automated Subject List</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Subject Name</center></th>
                                                <th><center>Section Details</center></th>
                                                <th><center>Add Section</center></th>
                                                <th><center>No of Question </center></th>
                                                
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Subject Name</center></th>
                                                <th><center>Section Details</center></th>
                                                <th><center>Add Section</center></th>
                                                <th><center>No of Question </center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <div id="myDiv">
                                            <img id="loading-image" src="<?php echo base_url().'uploads/loader.gif'; ?>" style="display: none; width:50px; height: 50px; margin-left: 40%; position: absolute; margin-top: -1%;"/>
                                        </div>    
                                        <?php 
                                        if(!empty($auomated_test))
                                        {   $i = 0;
                                            foreach($auomated_test as $key)
                                            { 
                                                $i++;

                                              ?>
                                            <tr>
                                                    <td><center><?php echo $i;?></center></td>
                                                    <td><center><?php $subject = $this->common_model->common_getRow('course_subject',array('id'=>$key->subject_id)); echo ucwords($subject->subject_name);?></center></td>
                                                    <td><?php $section = $this->common_model->getData('automated_section',array('subject_id'=>$key->subject_id,'course_id'=>$key->course_id));
                                                        foreach ($section as $values){?>
                                                        <b>Section name - </b> <?php echo $values->section_name?> <p style="float: right;"><b>Type - </b><?php $question_type = $this->common_model->common_getRow('question_type',array('id'=>$values->question_type)); echo ucwords($question_type->name);?></p><br>
                                                        <b>No. of question - </b> <?php echo $values->no_of_question?><br>
                                                        <b>Start to - </b> <?php echo $values->start_to?> <b>End to - </b> <?php echo $values->end_to?><br> 
                                                        <b>Section name - </b> <?php echo $values->section_name?><hr>     
                                                      <?php  }
                                                    ?></td>
                                                    <td><center><a href="<?php echo base_url('automated_fulltest/add_section/'.$key->course_id."/".$key->subject_id);?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></a></span></center></td> 
                                                    <td><center><?php echo $key->no_of_question;?></center></td>
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                               
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>

      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script type="text/javascript">
       function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('subadmin/MUSP/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }else{ location.reload();}
        }
</script>




     


