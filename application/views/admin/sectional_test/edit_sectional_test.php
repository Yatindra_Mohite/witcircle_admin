<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Edit Sectional Test</title> 

     <link href="<?php echo base_url()?>editor/codemirror.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>editor/editor/css/froala_editor.pkgd.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>editor/editor/css/froala_style.min.css" rel="stylesheet">
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Sectional Test
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Sectional Test</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Sectional Test</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('sectional_test/edit_question/'.$this->common_model->id_encrypt($question_edit->id).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5));?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
               
               <div class="form-group">
                  <label class="control-label col-md-3">Subject<span class="required">*</span></label>
                    <div class="col-md-8">
                         <select class="form-control" name="subject_id" onchange="getChapter(this.value)" required>
                                 <option value="" >Select Subject</option>
                                        <?php if(!empty($subjects)){
                                                 foreach($subjects as $key)
                                                 {?>
                                                 <option value="<?php echo $key->id;?>" <?php if($key->id == $question_edit->subject_id){echo "selected";} ?>><?php echo ucwords($key->subject_name);?></option>
                                                <?php } 
                                            } ?>
                         </select>
                    </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-md-3">Chapter<span class="required" required > * </span></label>
                        <div class="col-md-8">
                            <select class="form-control" name="chapter_id" id="chapter" required>
                                 <?php if(!empty($chapter_list)){
                                                 foreach($chapter_list as $key)
                                                 {?>
                                                 <option value="<?php echo $key->id;?>"<?php if($key->id == $question_edit->chapter_id){echo "selected";} ?>><?php echo ucwords($key->chapter_name);?></option>
                                                <?php } 
                                            } ?>    
                            </select>
                        </div>
                </div>
              
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Question Type<span class="required"> * </span></label>
                        <div class="col-md-8">
                            <select class="form-control" id="yoo" name="question_type" onchange="gettype(this.value)" required="">
                                        <option value=""> Question Type</option>
                                        <?php if(!empty($question_type)){
                                                 foreach($question_type as $key)
                                                 {if($key->id!=10){
                                                  ?>
                                                 <option value="<?php echo $key->id; ?>" <?php if($key->id == $question_edit->question_type){ echo "selected";} ?>><?php echo $key->name;?></option>
                                                <?php } } } ?>
                                </select>
                        </div>        
                    </div>
                </div>

                 <!-- <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Guide Line<span class="required">*</span></label>
                        <div class="col-md-8">
                            <textarea  class="edit" placeholder="Text Description" name="guide_line" cols="5" rows="5"><?php echo $question_edit->guide_line;?></textarea>
                        </div>
                    </div>
                </div> -->

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Question Description<span class="required">*</span></label>
                        <div class="col-md-8">
                            <textarea  class="edit" placeholder="Text Description" name="ques_desc" cols="5" rows="5"><?php echo $question_edit->question;?></textarea>
                        </div>
                    </div>
                </div>

                <?php $a = json_decode($question_edit->options); ?>

                <div id="for_integer" >
                     <div class="form-group">
                      <label class="control-label col-md-3"> (option 1)<span class="required">*</span></label>
                        <div class="col-md-8">
                               <textarea  class="edit" placeholder="Text Description" name="text_desc1" cols="5" rows="5"><?php if (!empty($a[0])) { echo $a[0]->option; } ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 2)<span class="required">*</span></label>
                        <div class="col-md-8">
                              <textarea  class="edit" placeholder="Text Description" name="text_desc2" cols="5" rows="5"><?php if (!empty($a[1])) { echo $a[1]->option; } ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 3)<span class="required">*</span></label>
                        <div class="col-md-8">
                             <textarea  class="edit" placeholder="Text Description" name="text_desc3" cols="5" rows="5"><?php if (!empty($a[2])) { echo $a[2]->option; } ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 4)<span class="required">*</span></label>
                        <div class="col-md-8">
                              <textarea  class="edit" placeholder="Text Description" name="text_desc4" cols="5" rows="5"><?php if (!empty($a[3])) { echo $a[3]->option; } ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 5)<span class="required">*</span></label>
                        <div class="col-md-8" >
                              <textarea  class="edit" placeholder="Text Description" name="text_desc5" cols="5" rows="5"><?php if (!empty($a[4])) { echo $a[4]->option; } ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 6)<span class="required">*</span></label>
                        <div class="col-md-8">
                             <textarea  class="edit" placeholder="Text Description" name="text_desc6" cols="5" rows="5"><?php if (!empty($a[5])) { echo $a[5]->option; } ?></textarea>
                        </div>
                    </div>
                </div>

                <div id="simple_question" <?php if($question_edit->question_type == "4"){ echo "style= 'display: block;'";}else{ echo "style='display: none;'";}?> >
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Answer<span class="required">*</span></label>
                                <div class="col-md-8">

                                    <?php for ($i=1; $i < 7; $i++) { ?>
                                       <input id="simple" type="radio" name="answer_id1"  value="<?php echo $i;?>" <?php if($i == $question_edit->answer){ echo "checked";} ?> required> <?php echo $i."&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"; 
                                   }?>
                                </div>
                            </div>
                        </div>
                        <!-- simple question end -->
                </div>

                <div class="portlet-body form" <?php if($question_edit->question_type == "2"){ echo "style= 'display: block;'";}else{ echo "style='display: none;'";}?> id="integer_type">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Answer<span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input id="integer" type="text" placeholder="Write Answer" value="<?php echo $question_edit->answer;?>" name="answer_id2" class="form-control" data-parsley-required-message="nomber of question name is required"  required/>
                                   
                                </div>
                            </div>
                        </div>
                </div>        
                        <!-- chom end -->

                <div id="multi_correct" <?php if($question_edit->question_type == "5"){ echo "style= 'display: block;'";}else{ echo "style='display: none;'";}?> >
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Answer<span class="required">*</span></label>
                                <div class="col-md-8">

                                    <?php for ($i=1; $i < 7; $i++) { ?>
                                       <input id="multi" type="checkbox" name="answer_id3[]"  value="<?php echo $i;?>" <?php $multi_check = $question_edit->answer; $new =explode(",", $multi_check);  foreach ($new as $key ){ if($i == $key){ echo "checked";} }?>  required> <?php echo $i."&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"; 
                                   }?>

                                </div>
                            </div>
                        </div>
                      <!-- simple question end -->
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green"  id="x" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>MUSP"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>


<script>
function getChapter(subject_id)
 { 
   var str = "subject_id="+subject_id;
   //alert(str);

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>sectional_test/get_chapter/",
        data:str,
        success:function(data)
        { 
         // alert(data);
          $('#chapter').empty();
          $('#chapter').append(data);
        }
   });
   }
// $('#chapter option').attr('selected', false);
 </script>

<script src="<?php echo base_url()?>editor/editor/js/codemirror.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>editor/editor/js/xml.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>editor/editor/js/froala_editor.pkgd.min.js" type="text/javascript"></script>

<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
        $(function() {
            $('.edit').froalaEditor({
                // Set the file upload URL.
                imageUploadURL: '/witcircle_admin/editor/upload_image.php',

                imageUploadParams: {
                    id: 'my_editor'
                }
            })

            // $('.edit1').froalaEditor({
            //     // Set the file upload URL.
            //     imageUploadURL: '/witcircle_admin/editor/upload_image.php',

            //     imageUploadParams: {
            //         id: 'my_editor'
            //     }
            // })
        });
    </script>


<script type="text/javascript">
$(document).ready(function(){
     $yoo = $("#yoo").val();
         gettype($yoo);
});
</script>


<script type="text/javascript">

  function gettype(id){
    //alert(id)
    if ( id == "2") {
     // alert('asdasda');
        $("#integer_type").show();
        $("#simple_question").hide();
        $("#multi_correct").hide();

        $("#for_integer").hide();
        $('#multi,#simple').removeAttr('required');
       // document.getElementById("multi").required = false;
        //document.getElementById("simple").required = false;
    }

    if ( id == "4") {
     
        $("#integer_type").hide();
        $("#simple_question").show();
        $("#multi_correct").hide();

        $("#for_integer").show();
        $('#multi,#integer').removeAttr('required');
        //document.getElementById("integer").required = false;
        //document.getElementById("multi").required = false;
    }

    if ( id == "5") {
     
        $("#integer_type").hide();
        $("#simple_question").hide();
        $("#multi_correct").show();

        $("#for_integer").show();
        $('#simple,#integer').removeAttr('required');
        //document.getElementById("integer").required = false;
       // document.getElementById("simple").required = false;
    }

    if ( id == "10") {
     // alert('asdasda');
        $("#integer_type").hide();
        $("#simple_question").hide();
        $("#multi_correct").hide();
        $("#for_integer").hide();
        $("#for_comprehension").hide();

        $('#multi,#simple,#for_comprehension1,#integer').removeAttr('required');
        //$('#integer').attr("required", "required");
    }
  
  }
</script>   


    </body>

</html>
