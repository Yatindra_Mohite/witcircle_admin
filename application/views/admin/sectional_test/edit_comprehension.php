<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Edit Comprehension</title> 

     <link href="<?php echo base_url()?>editor/codemirror.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>editor/editor/css/froala_editor.pkgd.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>editor/editor/css/froala_style.min.css" rel="stylesheet">
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Comprehension
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Comprehension</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Comprehension</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('sectional_test/edit_comprehension/'.$this->common_model->id_encrypt($question_edit->id));?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
               
               <div class="form-group">
                  <label class="control-label col-md-3">Subject<span class="required">*</span></label>
                    <div class="col-md-8">
                         <select class="form-control" name="subject_id" onchange="getChapter(this.value)" required>
                                 <option value="" >Select Subject</option>
                                        <?php if(!empty($subjects)){
                                                 foreach($subjects as $key)
                                                 {?>
                                                 <option value="<?php echo $key->id;?>"<?php if($key->id == $question_edit->subject_id){echo "selected";} ?>><?php echo ucwords($key->subject_name);?></option>
                                                <?php } 
                                            } ?>
                            </select>
                    </div>
                </div>
                <div class="form-group">
                        <label class="control-label col-md-3">Chapter<span class="required" required > * </span></label>
                        <div class="col-md-8">
                            <select class="form-control" name="chapter_id" id="chapter" required>
                                 <?php if(!empty($chapter_list)){
                                                 foreach($chapter_list as $key)
                                                 {?>
                                                 <option value="<?php echo $key->id;?>"<?php if($key->id == $question_edit->chapter_id){echo "selected";} ?>><?php echo ucwords($key->chapter_name);?></option>
                                                <?php } 
                                            } ?>    
                            </select>
                        </div>
                </div>
              

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Question Description<span class="required">*</span></label>
                        <div class="col-md-8">
                            <textarea  class="edit" placeholder="Text Description"  name="ques_desc" cols="5" rows="5"><?php echo $question_edit->question ?></textarea>

                         <!--    <input type="file" class="form-control" name=""> -->
                        </div>
                    </div>
                </div>


                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green"  id="x" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>MUSP"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>


<script>
function getChapter(subject_id)
 { 
   var str = "subject_id="+subject_id;
   //alert(str);

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>sectional_test/get_chapter/",
        data:str,
        success:function(data)
        { 
         // alert(data);
          $('#chapter').empty();
          $('#chapter').append(data);
        }
   });
   }
//$('#chapter option').attr('selected', false);
 </script>

<script src="<?php echo base_url()?>editor/editor/js/codemirror.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>editor/editor/js/xml.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>editor/editor/js/froala_editor.pkgd.min.js" type="text/javascript"></script>

<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
        $(function() {
            $('.edit').froalaEditor({
                // Set the file upload URL.
                imageUploadURL: '/witcircle_admin/editor/upload_image.php',

                imageUploadParams: {
                    id: 'my_editor'
                }
            })

            // $('.edit1').froalaEditor({
            //     // Set the file upload URL.
            //     imageUploadURL: '/witcircle_admin/editor/upload_image.php',

            //     imageUploadParams: {
            //         id: 'my_editor'
            //     }
            // })
        });
    </script>


<script type="text/javascript">
$(document).ready(function(){
     $("#comprehensive_question").hide();
});
</script>


    </body>

</html>
