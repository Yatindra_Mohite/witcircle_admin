<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Add Comprehension Question</title> 

     <link href="<?php echo base_url()?>editor/codemirror.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>editor/editor/css/froala_editor.pkgd.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>editor/editor/css/froala_style.min.css" rel="stylesheet">

    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
        
          
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Comprehension Question
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Comprehension Question</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Comprehension Question - </div>
                                               
                                            </div>
        <div class="portlet-body form" >
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
               <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Question Type<span class="required">*</span></label>
                        <div class="col-md-8">
                                <input type="radio" name="type_id" onclick="gettype(this.value)" value="2"> Multiple <input type="radio" name="type_id" onclick="gettype(this.value)" value="1"> Single   
                        </div>
                    </div>
               </div>
                
              
                
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Question Description<span class="required">*</span></label>
                        <div class="col-md-8">
                            <textarea  class="edit" placeholder="Text Description"  name="ques_desc" cols="5" rows="5"></textarea>
                        </div>
                    </div>
                </div>

                <div id="for_integer" >
                     <div class="form-group">
                      <label class="control-label col-md-3"> (option 1)<span class="required">*</span></label>
                        <div class="col-md-8">
                               <textarea  class="edit" placeholder="Text Description" name="text_desc1" cols="5" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 2)<span class="required">*</span></label>
                        <div class="col-md-8">
                              <textarea  class="edit" placeholder="Text Description" name="text_desc2" cols="5" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 3)<span class="required">*</span></label>
                        <div class="col-md-8">
                             <textarea  class="edit" placeholder="Text Description" name="text_desc3" cols="5" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 4)<span class="required">*</span></label>
                        <div class="col-md-8">
                              <textarea  class="edit" placeholder="Text Description" name="text_desc4" cols="5" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 5)<span class="required">*</span></label>
                        <div class="col-md-8" >
                              <textarea  class="edit" placeholder="Text Description" name="text_desc5" cols="5" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3"> (option 6)<span class="required">*</span></label>
                        <div class="col-md-8">
                             <textarea  class="edit" placeholder="Text Description" name="text_desc6" cols="5" rows="5"></textarea>
                        </div>
                    </div>
                </div>

                <div id="simple_question" style="display: none;">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Answer<span class="required">*</span></label>
                                <div class="col-md-8">

                                    <?php for ($i=1; $i < 7; $i++) { ?>
                                       <input class="abc1" type="radio" name="answer_id1"  value="<?php echo $i;?>" required="required"> <?php echo $i."&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"; 
                                   }?>

                                    <!-- <input type="text" placeholder="Write Answer" name="no_of_question" class="form-control" data-parsley-required-message="answer name is required"  required/> -->
                                </div>
                            </div>
                        </div>
                        <!-- simple question end -->
                </div>

                 <div id="multi_correct" style="display: none;" >
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Answer<span class="required">*</span></label>
                                <div class="col-md-8">

                                    <?php for ($i=1; $i < 7; $i++) { ?>
                                       <input class="abc2" type="checkbox" name="answer_id3[]"  value="<?php echo $i;?>" required="required"> <?php echo $i."&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"; 
                                   }?>

                                </div>
                            </div>
                        </div>
                        
                        <!-- simple question end -->
                </div> 

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green"  id="x" name="submit" value="Submit" >
                            <a href="<?php echo base_url('sectional_test')?>"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
        <div>

        
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>

<script src="<?php echo base_url()?>editor/editor/js/codemirror.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>editor/editor/js/xml.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>editor/editor/js/froala_editor.pkgd.min.js" type="text/javascript"></script>

<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
        $(function() {
            $('.edit').froalaEditor({
                // Set the file upload URL.
                imageUploadURL: '/witcircle_admin/editor/upload_image.php',

                imageUploadParams: {
                    id: 'my_editor'
                }
            })
        });
    </script>


<!-- <script type="text/javascript">
$(document).ready(function(){
     $("#comprehensive_question").hide();
});
</script> -->


<script type="text/javascript">

  function gettype(id){
    //alert(id)

    if ( id == "2") {
        $("#multi_correct").show();
        $("#simple_question").hide();

        $('.abc1').removeAttr('required');
        $('.abc2').attr("required", "required");
    //alert('2');
    }

    if ( id == "1") {
        $("#simple_question").show();
        $("#multi_correct").hide();

        $('.abc2').removeAttr('required');
        $('.abc1').attr("required", "required");
   // alert('1');
    }

  }
</script>

  </body>

</html>