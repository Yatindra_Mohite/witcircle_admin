<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Witcard</title>
    <link href="http://34.208.171.55/witcircle_admin/editor/editor/css/froala_editor.pkgd.min.css" rel="stylesheet">
    <link href="http://34.208.171.55/witcircle_admin/editor/editor/css/froala_style.min.css" rel="stylesheet">
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Witcard
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Witcard</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Witcard</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                
                <div class="form-group">
                        <label class="control-label col-md-3">Card Section<span class="required" > * </span></label>
                        <div class="col-md-8">
                            <select class="form-control" name="card_section" id="my_section" onchange="getSection(this.value)" required>
                                <option value="">Select Card</option>
                                <option value="1">News</option>
                                <option value="2">Study Zone</option>
                                <option value="3">Current Affairs</option>
                                <option value="4">General Knowledge</option>
                            </select>

                        </div>
                </div>
                <div id="change" style="display: none;">
	                <div class="form-group">
	                        <?php $course = $this->db->query("SELECT * FROM subjective_and_cards_course WHERE witcard_id > 0 AND status = 1 ")->result(); ?>
	                        <label class="control-label col-md-3">Course<span class="required" > * </span></label>
	                        <div class="col-md-8">
	                            <select class="form-control" name="course_id" id="course" onchange="getSubject(this.value)" required>
	                                <option value="">Select Course</option>
	                                <?php 
	                                  foreach ($course as $key ) {
	                                    echo "<option value='".$key->id."'>".ucwords($key->name)."</option>";
	                                  }
	                                ?>
	                            </select>

	                        </div>
	                        <div id="load" style="display: none;"><img src="<?php echo base_url('uploads/load.gif')?>"></div>
	                </div>
	                <div class="form-group">
	                        <label class="control-label col-md-3">Subject<span class="required" > * </span></label>
	                        <div class="col-md-8">
	                            <select class="form-control" name="subject_id" onchange="getChapter(this.value)" id="subject" required>
	                                <option value="" >Select course first</option>      
	                            </select>
	                        </div>
	                        <div id="load1" style="display: none;"><img src="<?php echo base_url('uploads/load.gif')?>"></div>
	                </div>
	                <div class="form-group">
	                        <label class="control-label col-md-3">Chapter<span class="required" > * </span></label>
	                        <div class="col-md-8">
	                            <select class="form-control" name="chapter_id" id="chapter" required>
	                                <option value="" >Select subject first</option>      
	                            </select>
	                        </div>
	                </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-md-3">Witcard Type<span class="required" > * </span></label>
                        <div class="col-md-8">
                            <select class="form-control" onchange="changeFiled(this.value)" name="type" id="" required>
                                <option value="">Select Witcard type</option>
                                <option value="1">Heading-Image Only</option>
                                <option value="2">Image-Heading-text</option>      
                                <option value="3">Heading-Text</option>
                                <option value="4">Image</option>
                                <option value="5">Polling</option>      
                            </select>
                        </div>
                </div>

                <div class="form-group" id="A">
                        <label class="control-label col-md-3">Heading<span class="required" > * </span></label>
                        <div class="col-md-8">
                            <input type="text" id="heading" placeholder="Enter heading" name="heading" class="form-control" required="">
                        </div>
                </div>

                <div class="form-group" id="B">
                        <label class="control-label col-md-3">Image<span class="required" > * </span></label>
                        <div class="col-md-8">
                            <input type="file" id="file" name="image" class="form-control" required="">
                        </div>
                </div>

                <div class="form-group" id="C">
                        <label class="control-label col-md-3">Text<span class="required" > * </span></label>
                        <div class="col-md-8">
                            <textarea name="text" id="text" cols="5" maxlength="60" class="form-control" rows="5" required=""></textarea>
                        </div>
                </div>
                
                <div id="D" style="display: none;">
                    <div class="form-group" >
                            <label class="control-label col-md-3">Question<span class="required" > * </span></label>
                            <div class="col-md-8">
                                <textarea name="questions" id="questions" cols="5" maxlength="60" class="form-control" rows="2" required=""></textarea>
                            </div>
                    </div>
                    <div class="form-group" >
                            <label class="control-label col-md-3">Option 1<span class="required" > * </span></label>
                            <div class="col-md-8">
                                <input type="text" id="ques1" placeholder="option 1" maxlength="60" name="opt_1" class="form-control" required="">
                            </div>
                    </div>
                    <div class="form-group" >
                            <label class="control-label col-md-3">Option 2<span class="required" > * </span></label>
                            <div class="col-md-8">
                                <input type="text" id="ques2" placeholder="option 2" maxlength="60" name="opt_2" class="form-control" required="">
                            </div>
                    </div>
                    <div class="form-group" >
                            <label class="control-label col-md-3">Option 3<span class="required" > * </span></label>
                            <div class="col-md-8">
                                <input type="text" id="ques3" placeholder="option 3" maxlength="60" name="opt_3" class="form-control" required="">
                            </div>
                    </div>
                    <div class="form-group" >
                            <label class="control-label col-md-3">Option 4<span class="required" > * </span></label>
                            <div class="col-md-8">
                                <input type="text" id="ques4" placeholder="option 4" maxlength="60" name="opt_4" class="form-control" required="">
                            </div>
                    </div>
                    <div class="form-group" >
                            <label class="control-label col-md-3">Answer<span class="required" > * </span></label>
                            <div class="col-md-8">
                                <input type="text" id="ans" placeholder="write Answer" maxlength="1" name="answer" class="form-control" required="">
                            </div>
                    </div>
                </div>


                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>chapter"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script src="http://34.208.171.55/witcircle_admin/editor/editor/js/codemirror.min.js" type="text/javascript"></script>
<script src="http://34.208.171.55/witcircle_admin/editor/js/xml.min.js" type="text/javascript"></script>
<script src="http://34.208.171.55/witcircle_admin/editor/editor/js/froala_editor.pkgd.min.js" type="text/javascript"></script>

<script type="text/javascript">
        $(function() {
            $('#text').froalaEditor({
                // Set the file upload URL.
                imageUploadURL: '/witcircle_admin/editor/upload_image.php',

                imageUploadParams: {
                    id: 'my_editor'
                }
            })
        });
</script>

<script type="text/javascript">
     document.addEventListener("contextmenu", function (e) {

        alert(" hah haha hahah hahah ");
        e.preventDefault();
    }, false);
</script>

<script >
	function getSection(section_id){

		if (section_id == 2){
          $("#change").show();
          $("#course,#subject,#chapter").attr("required", "required");
		}else{
          $("#change").hide();
          $("#course,#subject,#chapter").removeAttr('required');
        
		}

	}
</script>

<script>
function getSubject(course_id)
 { 
  if (course_id != ''){
    $("#load").show();
  }
   
   var str = "course_id="+course_id;
   //alert(str);

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>in_short/get_subject/",
        data:str,
        success:function(data)
        { 
         // alert(data);
          $('#subject').empty();
          $('#subject').append(data);
          $("#load").hide();
        }
   });
  //$("#load").hide();
   }
</script>

<script>
function changeFiled(id)
 { 
    //alert(id);
     if (id == 1){
       $("#C").hide();
       $("#A").show();
       $("#B").show();
       $("#D").hide();
       $("#text,#questions,#ques1,#ques2,#ques3,#ques4,#ans").removeAttr('required');
       $("#heading,#file").attr("required", "required");
     }

     if (id == 2){
       $("#A").show();
       $("#B").show();
       $("#C").show();
       $("#D").hide();
       $("#questions,#ques1,#ques2,#ques3,#ques4,#ans").removeAttr('required');
       $('#heading,#file,#text').attr("required", "required");
     }

     if (id == 3){
       $("#B").hide();
       $("#A").show();
       $("#C").show();
       $("#D").hide();
       $("#file,#questions,#ques1,#ques2,#ques3,#ques4,#ans").removeAttr('required');
       $("#text,#heading").attr("required", "required");
       
     }

     if (id == 4){
       $("#A").hide();
       $("#C").hide();
       $("#B").show();
       $("#D").hide();
       $("#text,#heading,#questions,#ques1,#ques2,#ques3,#ques4,#ans").removeAttr('required');
       $("#file").attr("required");

     }

     if (id == 5){
       $("#A").hide();
       $("#C").hide();
       $("#B").hide();
       $("#D").show();
       $("#text,#heading,#file").removeAttr('required');
       $("#questions,#ques1,#ques2,#ques3,#ques4,#ans").attr("required");

     }
 }


function getChapter(subject_id)
 { 
  if (subject_id != ''){
    $("#load1").show();
  }
   
   var str = "subject_id="+subject_id;
   //alert(str);

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>in_short/get_chapter/",
        data:str,
        success:function(data)
        { 
         // alert(data);
          $('#chapter').empty();
          $('#chapter').append(data);
          $("#load1").hide();
        }
   });
  //$("#load").hide();
   }
 //$('#chapter option').attr('selected', false);
 </script>

</body>

</html>