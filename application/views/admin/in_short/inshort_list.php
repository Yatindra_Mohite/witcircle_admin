<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Witcard List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Witcard List
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Witcard List</span>
                        </li>
                        <a href="<?php echo base_url('in_short/add_inshort  ')?>"><button style="margin-top: -7px;" type="button" class="btn btn-primary btn-md pull-right">Add Witcard</button>  </a>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Witcard List</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>ALL Data </center></th>
                                                <!-- <th><center>Chapter name </center></th> -->
                                                <!-- <th><center>Subject name </center></th> -->
                                                <th><center>Heading</center></th>
                                                <th><center>Image</center></th>
                                                <th><center>Text</center></th>
                                                <th><center>Question</center></th>
                                                <th><center>Status</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>ALL Data </center></th>
                                                <!-- <th><center>Chapter name </center></th> -->
                                                <!-- <th><center>Subject name </center></th> -->
                                                <th><center>Heading</center></th>
                                                <th><center>Image</center></th>
                                                <th><center>Text</center></th>
                                                <th><center>Question</center></th>
                                                <th><center>Status</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($inshort_list))
                                        {   $i = 0;
                                            foreach($inshort_list as $key)
                                            { 
                                                $i++;

                                              ?>
                                            <tr>
                                                    <td><center><?php echo $i;?></center></td>
                                                    <td width="30%">
                                                    <b>Card Section - </b> <?php if($key->card_section == 1){echo "News";} if($key->card_section == 2){echo "Study Zone";} if($key->card_section == 3){echo "Current Affairs";}if($key->card_section == 4){echo "General Knowledge";}?><br>
                                                    <b>Course Name - </b><?php if ($key->course_id != 0) {$course = $this->common_model->common_getRow('subjective_and_cards_course',array('id'=>$key->course_id)); echo ucwords($course->name);}else{echo "NA";}?><br>
                                                    <b>Subject Name - </b><?php if ($key->subject_id != 0) { $subject = $this->common_model->common_getRow('in_short_subject',array('id'=>$key->subject_id)); echo ucwords($subject->subject_name);}else{echo "NA";}?><br>
                                                    <b>Chapter Name - </b><?php if ($key->chapter_id != 0) {$chapter = $this->common_model->common_getRow('in_short_chapter',array('id'=>$key->chapter_id)); echo ucwords($chapter->chapter_name);}else{echo "NA";}?><br><hr>
                                                    <b>Witcard Type - </b><?php if($key->short_type == 1){echo "Heading-Image Only";} if($key->short_type == 2){echo "Image-Heading-text";} if($key->short_type == 3){echo "Heading-Text";}if($key->short_type == 4){echo "Image";}if($key->short_type == 5){echo "Polling";}?>
                                                        
                                                    </td>
                                                    <td><center><?php if ($key->heading != '') { echo $key->heading;}else{echo "-";}?></center></td>

                                                    <td><center><?php if ($key->image != '') { ?> <img src = "<?php echo base_url().'/uploads/in_short/'.$key->image;?>" width="130px" height="100px"><?php }else{ echo "-";}?></center></td>
                                                    <td><center><?php if($key->text != '') { echo $key->text;}else{ echo "-";};?></center></td>
                                                    <td>
                                                        <?php if($key->short_type == 5){ ?>
                                                       <b>Question - </b> <?php echo "$key->question";?><br>

                                                       <b>Options - </b><?php echo "$key->option_1";?> , <?php echo "$key->option_2";?> , <?php echo "$key->option_3";?> ,<?php echo "$key->option_4";?><br>

                                                       <b>Answer - </b><?php echo "$key->answer";?>


                                                            <?php }else{?> <center> <?php echo "-";}?> </center>
                                                    </td>

                                                    <td><center></span><a href="<?php echo 'edit_inshort/'.$key->id;?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<a onclick='deletemain("<?php echo $key->id;?>")' href="javascript:void(0);" title="click here to delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span></a><center></td>
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class=""></td>
                                                <td class=""></td>

                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script>
        function deletemain(id)
        {
            //alert(id);
            var r = confirm('Are you really want to delete?');
            if(r==true)
            {
                $.ajax({
                   url:"<?php echo base_url('in_short/delete')?>/"+id,
                   success:function(data)
                   {   //alert(data)
                       if(data==1000)
                       {

                            location.reload();
                       }
                   }
                });
            }
        }
</script>

<!-- <script type="text/javascript">
        function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('in_short/change_status1')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }location.reload();
        }
    </script> -->




     


