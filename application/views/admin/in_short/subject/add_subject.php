<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Subject</title>
  


    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

        <!-- END THEME LAYOUT STYLES -->


    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Subject (Wit Cards)
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Subject</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Subject (Wit Cards)</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                     <?php $course = $this->db->query("SELECT * FROM course WHERE status = 1 ")->result(); ?>
                        <label class="control-label col-md-3">Select Course<span class="required">*</span></label>
                        <div class="col-md-6">
                            <select class="form-control" name="course_id"  required>
                                 <option value="" disabled>Select Courses</option>
                                    <?php if(!empty($course)){
                                             foreach($course as $key)
                                             {?>
                                             <option value="<?php echo $key->id;?>"><?php echo ucwords($key->course_name);?></option>
                                            <?php } 
                                    } ?>
                            </select>
                        </div>
                    </div>
    
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Subject<span class="required">*</span></label>
                        <div class="col-md-6">
                             <!-- <input type="text" placeholder="Subject name" name="subject_name" class="form-control" data-role="tagsinput" data-parsley-required-message="Subject name is required"  required/> -->
                             <input type="text" placeholder="Subject name" name="subject_name" data-parsley-pattern="/^[a-zA-Z\s]*$/" data-parsley-required-message="Subject name is required" required class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Title<span class="required">*</span></label>
                        <div class="col-md-6">
                             <!-- <input type="text" placeholder="Subject name" name="subject_name" class="form-control" data-role="tagsinput" data-parsley-required-message="Subject name is required"  required/> -->
                             <input type="text" placeholder="Title name" name="title_name" data-parsley-pattern="/^[a-zA-Z\s]*$/" data-parsley-required-message="Title name is required" required class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>subject"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>



<!-- <script>
function myFunction() {
    var Emptyckeditor = CKEDITOR.instances['noticemessage'].getData();
    if(Emptyckeditor == "")
    {
        $('#ab1').html('This value is required.');
        return false;
    }
    return true; 
}
</script>
<script>
   $("#submit").click(function() {
    var fileExtension = ['jpeg', 'jpg', 'png'];
    var inputfile =  $("#inputfile").val();

   if(inputfile == '') 
   {
      $("#file411").html('Image is required').css("color", "red");
      return false;
   }
   else if(inputfile != '' && $.inArray($("#inputfile").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        
            //alert("Only formats are allowed : "+fileExtension.join(', '));
        $("#file411").html('Only formats are allowed :' +fileExtension.join(', ')).css("color", "red");
        return false;
    }
    return true;
  });
 </script> -->
        
    </body>

<script type="text/javascript">
// $('#skills').tagsinput({
// confirmKeys: [13, 44],
// maxTags: 20
// });
</script>

</html>