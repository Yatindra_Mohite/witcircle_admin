<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Chapter</title>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Chapter
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Chapter</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Chapter</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('in_short/edit_chapter/'.$this->common_model->id_encrypt($chapter_edit->id));?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
              
                <div class="form-group">
                        <label class="control-label col-md-3">Subject<span class="required" required > * </span></label>
                        <div class="col-md-6">
                        <?php $subject = $this->db->query("SELECT * FROM in_short_subject")->result(); ?>
                            <select class="form-control" name="subject_id" id="subject" required>
                                         <?php if(!empty($subject)){
                                                 foreach($subject as $key1)
                                                 {?>
                                                 <option value="<?php echo $key1->id;?>"<?php if($key1->id == $chapter_edit->subject_id){ echo "selected";} ?>><?php echo ucwords($key1->subject_name);?></option>
                                                <?php } 
                                            } ?>
                                
                            </select>
                        </div>
                    </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Chapter<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Subject name" name="chapter_name" value="<?php echo ucwords($chapter_edit->chapter_name)?>" data-parsley-pattern="/^[a-zA-Z\s]*$/" class="form-control" data-parsley-required-message="chapter name is required"  required/>
                           
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>barber/add_barber"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script type="text/javascript">
 $( function() {
    $( "#date1" ).datepicker({
       changeMonth: true,
       changeYear: true,
       dateFormat:'yy-mm-dd',
       minDate: 0 
    });
  } );
  </script>
 
<script>
function myFunction() {
    var Emptyckeditor = CKEDITOR.instances['noticemessage'].getData();
    if(Emptyckeditor == "")
    {
        $('#ab1').html('This value is required.');
        return false;
    }
    return true; 
}
</script>
<script>
function getsubject(course_id)
 { 
   var str = "course_id="+course_id;
   //alert(str);

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>chapter/get_subject/",
        data:str,
        success:function(data)
        { 
         // alert(data);
          $('#subject').empty();
          $('#subject').append(data);
        }
   });
   }
 </script>
        
    </body>

</html>