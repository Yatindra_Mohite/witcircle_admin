<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Add Coupon</title>
    <!-- <link href="template/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" /> -->


    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

       
        <link href="template/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <!-- <link href="template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" /> -->
        <!-- <link href="template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" /> -->
        <link href="template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->


    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Coupon
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Coupon</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Coupon</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form name="myform" action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
            <input type="hidden" name="length" value="12">

               <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Title<span class="required">*</span></label>
                        <div class="col-md-6">
                             <input type="text" placeholder="Title" name="title" data-parsley-required-message="title is required" required class="form-control" />
                        </div>
                         
                    </div>

                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Promo Code<span class="required">*</span></label>
                        <!--<div class="col-md-6">
                             <input type="text" onkeydown="upperCaseF(this)" maxlength="6" placeholder="Enter promo code" name="coupon_code" data-parsley-required-message="cirt name is required" minlength="8" maxlength="8" required class="form-control" />
                        </div> -->
                        <div class="col-md-6">
                          <input name="coupon_code" onkeydown="upperCaseF(this)" maxlength="12" class="form-control" type="text" placeholder="Promo Code" data-parsley-required-message="Promo Code is required" id="promo_code" data-parsley-pattern="^[A-Za-z0-9 ]*$" required="">
                          <span style="margin-left: 53%;"><span class="required">*</span>Please do not start with Zero(0)</span>
                          <span  class="input-group-btn refresh-btn" style="position: absolute;top: -4px;right: 0;"><button type="button" onClick="generate();" class="btn btn-default btn-lg getNewPass" style="padding: 10px;"><span class="fa fa-refresh"></span></button></span>
                           <div id="err" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;"></div>
                        </div>
                       
                    </div>
                </div>

                 <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Usage Limit<span class="required">*</span></label>
                        <div class="col-md-6">
                             <input type="textarea" id="" placeholder="usage limit" name="usage_limit" data-parsley-required-message="Amount is required" data-parsley-pattern="^(0|[1-9][0-9]*)$"  required class="form-control" />
                             <div id="err1" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;"></div>
                        </div>
                         
                    </div>

                </div>

                <div id="b" class="form-body bloack" >
                    <div class="form-group">
                        <label class="control-label col-md-3">Apply on<span class="required">*</span></label>
                        <div class="col-md-8">
                                <input id="b1" type="radio" name="type_id" onclick="gettype2(this.value)" value="2" required="required"> Coin  &nbsp; &nbsp; &nbsp; <input id="b1" type="radio" name="type_id" onclick="gettype2(this.value)" value="1" required="required"> Subscription   
                        </div>
                    </div>
                </div>

               <div id="c" class="form-body bloack" style="display: none;">
                    <div class="form-group">
                     <?php $subscription = $this->db->query("SELECT * FROM subscription WHERE status = 1 ")->result(); ?>
                        <label class="control-label col-md-3">Select Subscription<span class="required">*</span></label>
                        <div class="col-md-6">
                            <select class="form-control " id="c1" name="subscription_id" onchange="getsub(this.value)" required="required">
                                 <option value="">Select Subscription</option>
                                    <?php if(!empty($subscription)){
                                             foreach($subscription as $key)
                                             {?>
                                             <option value="<?php echo $key->subscription_id;?>"><?php echo ucwords($key->subscription_name);?></option>
                                            <?php } 
                                    } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="a" class="form-body" style="display: none;">
                    <div class="form-group">
                        <label class="control-label col-md-3">Coin<span class="required">*</span></label>
                        <div class="col-md-6">
                             <input type="textarea" id="" placeholder="usage limit" name="usage_limit" data-parsley-required-message="Coin required" data-parsley-pattern="^(0|[1-9][0-9]*)$"  required class="form-control" />
                             <div id="err1" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;"></div>
                        </div>
                         
                    </div>

                </div>

                <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required">*</span></label>
                        <div class="col-md-6">
                             <textarea class="form-control" name="desc" cols="5" rows="5" required=""> </textarea>
                        </div>
                </div>

                <div id="f" class="form-group bloack" >
                       <label class="control-label col-md-3">Start Date<span class="required"> * </span></label>
                       <div class="col-md-3">
                          <input type="name" name="end_date" id="date2" readonly="readonly" data-parsley-required-message="This field is required" class="form-control " required="required" >
                                                 
                       </div>
                </div>

                <div id="f" class="form-group bloack" >
                       <label class="control-label col-md-3">Expire Date<span class="required"> * </span></label>
                       <div class="col-md-3">
                          <input type="name" name="end_date" id="date1" readonly="readonly" data-parsley-required-message="This field is required" class="form-control " required="required" >
                                                 
                       </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>subject"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script>
  $( function() {
   $( "#date1" ).datepicker({
      changeMonth: true,
      changeYear: true,
   /*   yearRange: '1990:'+(new Date).getFullYear(),*/
      dateFormat:'yy-mm-dd',
      minDate: 0
   });

   $( "#date2" ).datepicker({
      changeMonth: true,
      changeYear: true,
   /*   yearRange: '1990:'+(new Date).getFullYear(),*/
      dateFormat:'yy-mm-dd',
      minDate: 0
   });
 } );
</script>

<script type="text/javascript">
  $('#form11').parsley();  

  function upperCaseF(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
}

</script>
        
</body>

<script type="text/javascript">
function gettype2(id){
    //alert(id)
    if ( id == "2") {
        $("#c").css("display", "none");
        $("#a").css("display", "block");

        $('#c1').removeAttr('required');
    }else{

        $("#c").css("display", "block");
        $('#c1').attr("required", "required");
    }
  }
</script>

<script>
function getsub(cat_id)
 { 
    var str = "cat_id="+cat_id;
   //alert(str);

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>coupon/get_sub_category/",
        data:str,
        success:function(data)
        { 
          //alert(data);
          $('#d1').empty();
          $('#d1').append(data);
          // $("#load").hide();
        }
   });
  
   }
 
 </script>

 <script>
  $(document).ready(function(){
    $("#submit").click(function(){ 
        var promo_code  = $("#promo_code").val();

        var str = 0;
             $.ajax({
                    type:'POST',
                    url: '<?php echo base_url();?>coupon/check_promocode',
                    data: "promo_code="+promo_code,
                    async: false,
                    success: function(data){
                        if(data==10000)
                        {
                          str = 1;  
                          $("#err").html('Promo Code Already Exists').css('color','red');
                           setTimeout(function(){ 
                           $('#err').html('');
                            },4000);
                        }
                        else
                        { 
                          $("#err").html('');
                        }
                    },
                });
             if(str == 1)
             {
                return false;
             }   
        });
  });
</script>

<script>

function randomPassword(length) {
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

function generate() {
    myform.coupon_code.value = randomPassword(myform.length.value);
}

 </script>
</html>