<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Rank</title>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Rank Predictions
                            <small><big>Add Rank</big></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('')?>dashboard">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Rank</span>
                        </li>
                        
                    </ul>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Rank</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
            if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div id="payment_type" class="form-group">
                        <label class="control-label col-md-3">Course<span class="required"> * </span></label>
                         <?php $sub = $this->common_model->getData('course',array('status'=>'1'));?>
                        <div class="col-md-5">
                            <select name="course_id" class="form-control">
                                <?php if(!empty($sub)){
                                                 foreach($sub as $key)
                                                 {?>
                                                 <option value="<?php echo $key->id;?>"><?php echo $key->course_name;?></option>
                                                <?php } 
                                            } ?>
                            </select>
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Starting Number<span class="required" > * </span></label>

                        <div class="col-md-3">
                          <div class="col-md-8">
                              <input type="text" data-parsley-type="digits" name="starting_number" class="form-control" required="">
                          </div>
                        </div>

                        <label class="control-label col-md-2">End Number<span class="required" > * </span></label>

                        
                        <div class="col-md-3">
                          <div class="col-md-8">
                              <input type="text" data-parsley-type="digits" name="end_number" class="form-control" required="">
                          </div>
                        </div>
                    </div>
  
                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Starting Rank<span class="required" > * </span></label>

                        <div class="col-md-3">
                          <div class="col-md-8  ">
                              <input type="text" data-parsley-type="digits" name="start_rank" class="form-control" required="">
                          </div>
                        </div>

                        <label class="control-label col-md-2">End Rank<span class="required" > * </span></label>

                        
                        <div class="col-md-3">
                          <div class="col-md-8">
                              <input type="text" data-parsley-type="digits" name="end_rank" class="form-control" required="">
                          </div>
                        </div>
                    </div>
  
                </div>
                
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>rank/add_rank"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>

            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>


  </script>

        
  </body>

</html>