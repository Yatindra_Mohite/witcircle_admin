<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Rank List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Rank List
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Rank List</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Rank List</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Course Name</center></th>
                                                <th><center>Number</center></th>
                                                <th><center>Rank</center></th>
                                                <th><center>Action</center></th>

                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Course Name</center></th>
                                                <th><center>Number</center></th>
                                                <th><center>Rnak</center></th>
                                                <th><center>Action</center></th>

                                                
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($rank_predictions))
                                        {   $i = 0;
                                            foreach($rank_predictions as $key)
                                            { 
                                                $i++;
                                                   
                                              ?>
                                            <tr>
                                                    <td><center><?php echo $i;?></center></td>
                                                    <td><center><?php  $a = $this->common_model->common_getRow('course',array('id'=>$key->course_id)); echo $a->course_name;?></center></td>
                                                    <td><b>Starting Number -</b> <?php  echo $key->starting_no?><br><b>End Number -</b> <?php  echo $key->end_no?></td>
                                                    <td><b>Start Rank -</b> <?php  echo $key->start_rank?><br><b>End Rank -</b> <?php  echo $key->end_rank?></td>
                                                    <td><center><a href="<?php echo base_url('rank/edit_rank/'.$this->common_model->id_encrypt($key->id));?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>&nbsp;&nbsp;&nbsp;&nbsp;<a onclick='deletemain("<?php echo $key->id;?>")' href="javascript:void(0);" title="click here to delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span></a><center></td>
                                                        
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class=""></td>
                                                    <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class=""></td>
                                                <td class=""></td>

                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script>
         function deletemain(id)
            {
            	//alert(id);
                var r = confirm('Are you really want to delete?');
                if(r==true)
                {
                    $.ajax({
                       url:"<?php echo base_url('rank/delete_record')?>/"+id,
                       success:function(data)
                       {   
	                       if(data==1000)
	                       {
	                            location.reload();
	                       }
                       }
                    });
                }
            }
</script>




     


