<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>CKEditor 5 - Inline editor</title>
    <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/inline/ckeditor.js"></script>
</head>
<body>
    <h1>Inline editor</h1>
    <div id="editor">
        <p>This is some sample content.</p>
    </div>
    <script>
        InlineEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
</body>
</html>




<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script src="custom_tags_input.js"></script>

<div class="container">
<h2>Create Bootstrap Tags Input with jQuery, PHP & MySQL</h2>
<form method="post" class="form-horizontal" action="save.php">
<div class="form-group">
<label class="col-xs-3 control-label">Name:</label>
<div class="col-xs-8">
<input type="text" id="name" name="name" />
</div>
</div>
<div class="form-group">
<label class="col-xs-3 control-label">Your Skills:</label>
<div class="col-xs-8">
<input type="text" id="skills" name="skills" data-role="tagsinput" />
</div>
</div>
<div class="form-group">
<label class="col-xs-3 control-label"></label>
<div class="col-xs-8">
<input type="submit" name="submit" value="Save"/>
</div>
</div>
</form>
</div>

<script type="text/javascript">
    $('#skills').tagsinput({
confirmKeys: [13, 44],
maxTags: 20
});
</script>