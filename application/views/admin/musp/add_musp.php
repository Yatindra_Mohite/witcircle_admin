<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Full Test</title> 
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Full Test
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Full Test</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Full Test</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
               <div class="form-group">
                  <label class="control-label col-md-3">Full Test name<span class="required">*</span></label>
                    <div class="col-md-6">
                          <input type="text" placeholder="MUSP test name" class="form-control" name="test_name" data-parsley-required-message="test name is required"  required>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Test Description<span class="required">*</span></label>
                        <div class="col-md-6">
                            <textarea class="form-control" placeholder="Text Description" name="text_desc" cols="5" rows="5"></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">No of Question<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="no of Question" name="no_of_question" class="form-control" data-parsley-type="number" data-parsley-required-message="nomber of question name is required"  required/>
                             <?php echo form_error('b_name', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group">
                     <?php $course = $this->db->query("SELECT * FROM course")->result(); ?>
                        <label class="control-label col-md-3">Select Course<span class="required">*</span></label>
                        <div class="col-md-6">
                            <select class="form-control" name="course_id" id ="getdata" onchange="getsubject(this.value)" required>
                                 <option value="">Select Course</option>
                                        <?php if(!empty($course)){
                                                 foreach($course as $key)
                                                 {?>
                                                 <option value="<?php echo $key->id;?>"><?php echo ucwords($key->course_name);?></option>
                                                <?php } 
                                            } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <p class="text text-center">Create sections with subjects.</p>

               <!--  <div class="form-group">
                        <label class="control-label col-md-3">Sections<span class="required" required > * </span></label>
                        <div  class="input_fields_container">
                          <div class="col-md-3">
                              <input class="form-control" placeholder="Enter section name" name="section_name[]"  id="section_name" required>
                          </div>
                          <div class="col-md-3">
                              <select class="form-control" name="subject_id[]"  id="subject" required>
                                  <option disabled="disabled">Belongs to which subject</option>
                              </select>
                          </div>
                        </div>
                </div> -->
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Sections<span class="required">*</span></label>
                        <div class="col-md-6">

                        <div class="row">
                        <div class="input_fields_container">
                            <div class="col-sm-4">
                              <div class="">
                                <div><input type="text" placeholder="Enter section name" class="form-control" name="section_name[]" required>
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-4">
                              <div class="">
                                <div><input type="text" placeholder="Sectional times" data-parsley-type="number" class="form-control" name="sectional_times[]" >
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-4">
                              <div class="">
                                <div>
                                    <select class="form-control" name="subject_id[]"  id="subject" required>
                                      <option value="" >Belongs to which subject</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                      </div>
                        </div>
                            <!-- <input type="text" placeholder="Correct Question Marks" name="correct_marks" class="form-control" data-parsley-required-message="marks is required"  required/>
                             -->

                             

                        </div>

                        <div class="col-cm-3"><button type="button" class="btn btn-sm btn-primary add_more_button">Add More Fields</button></div>
                    </div>
                </div>


                
                

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green"  id="x" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>MUSP"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
    $(document).ready(function() {
    var max_fields_limit = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
        $('.add_more_button').click(function(e){ 

        //click event on add more fields button having class add_more_button
          var course_id = document.getElementById("getdata").value
          if (course_id == ''){
            alert('please select course first');
            return false;
          }

          var str = "course_id="+course_id;
       //alert(str);
          // bar2 = "";
          var abc ;
       $.ajax({
            type:"POST",
            url:"<?php echo base_url();?>MUSP/get_subject_new/",
            data:str,
            success:function(data)
            { 
             //alert(data);
             
            //  abc = data;
              // window.bar2 = abc;
              //alert(window.bar2);
              // $('#abc1').text(data);
                   if(x < max_fields_limit){ //check conditions
                //alert(bar2);
                  x++; //counter increment
                  $('.input_fields_container').append('<div class="fullWidth"> <div class="col-sm-4"><div class=""><div><input type="text" placeholder="Enter section name" class="form-control" name="section_name[]" required></div></div></div><div class="col-sm-4"><div class=""><div><input type="text" data-parsley-type="number" placeholder="Sectional times" class="form-control" name="sectional_times[]"></div></div></div><div class="col-sm-4"><div class=""><div> <select class="form-control subject3" name="subject_id[]" required>'+data+'</select></div></div></div><a href="#" class="remove_field" style="margin-left:10px;">Remove</a></div>'); //add input field
              }
            }
       });
 
 //alert(bar2);

      //  e.preventDefault();
       
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

<script>
function getsubject(course_id)
 { 
   
  // $( ".input_fields_container" ).remove();

   var str = "course_id="+course_id;
   //alert(str);

   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>MUSP/get_subject/",
        data:str,
        success:function(data)
        { 
        // alert(data);
          $('#subject').empty();
          if(data == 'You have no subject for this course select other') {
           $("#x").attr('disabled', 'disabled');
           $('#subject').append("<h5 class='control-label col-md-12'>"+data+"</h5>");
          
          }else
          {
          $('#subject').append(data);
          $('.subject3').html(data);
          $("#x").removeAttr('disabled');
          }
          
        }
   });
   }
 $('#subject option').attr('selected', false);
 </script>

    </body>

</html>
<div id="abc1" > </div>