<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Edit Comprehension</title> 

    <link href="<?php echo base_url()?>editor/codemirror.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>editor/editor/css/froala_editor.pkgd.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>editor/editor/css/froala_style.min.css" rel="stylesheet">


    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
        
          
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Comprehension
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Comprehension</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>MUSP Test name - </div>
                                               
                                            </div>
        <div class="portlet-body form" >
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('MUSP/edit_comprehension/'.$this->uri->segment(3).'/'.$this->common_model->id_encrypt($question_edit->id));?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Subject<span class="required">*</span></label>
                        <div class="col-md-8">
                             <?php if(!empty($que_active)){
                                $sub = json_decode($que_active->subject_id);
                                foreach ($sub as $key) {?>
                                     <input type="radio" name="subject_id"  value="<?php echo $key->subject_id?>" <?php if($key->subject_id == $question_edit->subject_id){ echo "checked";} ?> required>  <?php $subject = $this->common_model->common_getRow('course_subject',array('id'=>$key->subject_id)); echo ucwords($key->section_name);?>
                                <?php }
                                //exit;
                             } ?>
                        </div>
                    </div>
                </div>

                <!-- <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Guide Line<span class="required">*</span></label>
                        <div class="col-md-8">
                            <textarea  class="edit" placeholder="Text Description"  name="guide_line" cols="5" rows="5"></textarea>
                        </div>
                    </div>
                </div> -->
                
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Question Description<span class="required">*</span></label>
                        <div class="col-md-8">
                            <textarea  class="edit" placeholder="Text Description"  name="ques_desc" cols="5" rows="5"><?php echo $question_edit->question; ?></textarea>
                        </div>
                    </div>
                </div>


                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green"  id="x" name="submit" value="Submit" >
                            
                        </div>
                    </div>
                </div>
            </form>
        <div>

        
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>

<script src="<?php echo base_url()?>editor/editor/js/codemirror.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>editor/editor/js/xml.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>editor/editor/js/froala_editor.pkgd.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
        $(function() {
            $('.edit').froalaEditor({
                // Set the file upload URL.
                imageUploadURL: '/witcircle_admin/editor/upload_image.php',

                imageUploadParams: {
                    id: 'my_editor'
                }
            })

            // $('.edit1').froalaEditor({
            //     // Set the file upload URL.
            //     imageUploadURL: '/witcircle_admin/editor/upload_image.php',

            //     imageUploadParams: {
            //         id: 'my_editor'
            //     }
            // })
        });
    </script>


<script type="text/javascript">
$(document).ready(function(){
     $("#comprehensive_question").hide();
});
</script>


<script type="text/javascript">

  function gettype(id){
    
    if ( id == "2") {
     // alert('asdasda');
        $("#integer_type").show();
        $("#simple_question").hide();
        $("#multi_correct").hide();
        $("#for_integer").hide();

        $('#multi,#simple').removeAttr('required');
        $('#integer').attr("required", "required");
    }

    if ( id == "4") {
     
        $("#integer_type").hide();
        $("#simple_question").show();
        $("#multi_correct").hide();
        $("#for_integer").show();

        $('#multi,#integer').removeAttr('required');
        $('#simple').attr("required", "required");
    }

    if ( id == "5") {
     
        $("#integer_type").hide();
        $("#simple_question").hide();
        $("#multi_correct").show();
        $("#for_integer").show();

        $('#simple,#integer').removeAttr('required');
        $('#multi').attr("required", "required");
    }

    if ( id == "10") {
     // alert('asdasda');
        $("#integer_type").hide();
        $("#simple_question").hide();
        $("#multi_correct").hide();
        $("#for_integer").hide();
        $("#for_comprehension").hide();

        $('#multi,#simple,#for_comprehension1,#integer').removeAttr('required');
        //$('#integer').attr("required", "required");
    }
  
  }
</script>

  </body>

</html>