<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Full Test List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
    
        <div class="page-header navbar navbar-fixed-top" id="myid">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Full Test List
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Full Test List</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Full Test List</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Test Name</center></th>
                                                <th><center>Course Name</center></th>
                                                <!-- <th><center>Subject name </center></th> -->
                                                <th><center>No of Question </center></th>
                                                <th><center>Status</center></th>
                                                <th><center>Generate</center></th>
                                                <th><center>Add Question</center></th>
                                                <th><center>View Question</center></th>
                                                <th><center>View comp. Question</center></th>
                                                 <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Test Name</center></th>
                                                <th><center>Course Name</center></th>
                                                <!-- <th><center>Subject name </center></th> -->
                                                <th><center>No of Question </center></th>
                                                <th><center>Status</center></th>
                                                <th><center>Generate</center></th>
                                                <th><center>Add Question</center></th>
                                                <th><center>View Question</center></th>
                                                <th><center>View comp.Question</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <div id="myDiv">
                                            <img id="loading-image" src="<?php echo base_url().'uploads/loader.gif'; ?>" style="display: none; width:50px; height: 50px; margin-left: 40%; position: absolute; margin-top: -1%;"/>
                                        </div>    
                                        <?php 
                                        if(!empty($musp_list))
                                        {   $i = 0;
                                            foreach($musp_list as $key)
                                            { 
                                                $i++;

                                              ?>
                                            <tr>
                                                    <td><center><?php echo $i;?></center></td>
                                                    <td><center><?php echo $key->test_name;?></center></td>     
                                                    <td><center><?php $course = $this->common_model->common_getRow('course',array('id'=>$key->course_id)); echo ucwords($course->course_name);?></center></td>
                                                  <?php /*  <td><center><?php $sub = explode(",",$key->subject_id);
                                                         foreach ($sub as $key1)
                                                         {?>
                                                           <?php $subject = $this->common_model->common_getRow('course_subject',array('id'=>$key1) ); echo ucwords($subject->subject_name);?>
                                                        <?php }
                                                           ?></center></td> */?>
                                                    <td><center><?php echo $key->no_of_question;?></center></td>
                                                    <td><center><select style="" class="form-control" onchange="changestatus('<?php echo $key->id;?>',this.value)">
                                                    <option value="1"<?php if($key->status==1){ echo 'selected';} ?>> Active </option>
                                                    <option value="0"<?php if($key->status==0){ echo 'selected';} ?>> Inactive </option>
                                                    </select></center></td>
                                                    <td><center><button onclick="generate('<?php echo $key->id;?>')" style="color:blue;">Generate</button></center></td>
                                                    <td><center><a href="<?php echo 'add_question/'.$this->common_model->id_encrypt($key->id);?>"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></a></span></a></center></td>
                                                    <td><center><a href="<?php echo 'question_list/'.$this->common_model->id_encrypt($key->id);?>"><span class="fa fa-list" aria-hidden="true"></a></span></center></td>
                                                    <td><center><a href="<?php echo 'comprehension_question_list/'.$this->common_model->id_encrypt($key->id);?>"><span class="fa fa-list" aria-hidden="true"></a></span></center></td>
                                                    <td><center><a href="<?php echo 'edit/'.$this->common_model->id_encrypt($key->id);?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span></center></td>
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class=""></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script type="text/javascript">
       function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('subadmin/MUSP/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }location.reload();
        }

        function generate(id) {
            var divIdHtml = $("#myDiv").html();
            //$("#loading-image").show();
            var str = "id="+id;
            $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('MUSP/generate_musp_test')?>/",
                   data:str,
                    beforeSend: function() {
                      $("#loading-image").show();
                    },
                   success:function(data)
                   {  
                      $("#loading-image").hide();
                      if(data==1000)
                       {
                            alert("Successfully generated.");
                            location.reload();
                       }else if(data == 2001)
                       {
                            alert("Either question not available OR Marking scheme not available for this test.");
                            location.reload();
                       }
                       else if(data == 3001)
                       {
                            alert("Test not found.");
                            location.reload();
                       }
                       else if(data == 4001)
                       {
                            alert("Somthing went wrong, please try again later.");
                            location.reload();
                       }
                       else if(data == 5001)
                       {
                            alert("Insufficient No. of question.");
                            location.reload();
                       }
                   }
                });
        }


    </script>




     


