<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Edit | Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
       <?php $this->load->view('admin/head'); ?>
        <link href="<?php echo base_url();?>template/assets/global/css/jquery.multiselect.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            .profile-userpic img{ max-width: 120px; min-height: 120px;}
            
        </style>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Admin Profile </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <?php if(!empty($this->session->flashdata('success'))){ echo "<span style='color:green'>".$this->session->flashdata('success')."</span>"; }?>
                        <?php if(!empty($this->session->flashdata('failed'))){ echo "<span style='color:red'>".$this->session->flashdata('failed')."</span>"; }?>
                    </ul>
                    <div id="error"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="col-sm-4">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet bordered">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic">
                                   <?php  
                                        if(!empty($admin_data->user_profile_pic))
                                        {
                                         $image = $admin_data->user_profile_pic;
                                        }else{ $image = 'default-medium.png';}

                                       $id = $this->uri->segment(3);
                                        

                                     ?>    
                                    <img  src="<?php echo base_url('uploads/sub_admin_image/'.$image);?>" class="img-responsive img-circle" alt="" width="30px" height="30px"> </div>
                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name"> <?php if(isset($admin_data->user_name)){echo $admin_data->user_name;}?> 
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet light bordered">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Contact Details</span>
                                    </div>
                                    <div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-envelope-o"></i>
                                            <?php if(isset($admin_data->user_email)){ echo $admin_data->user_email; } ?>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-md-6" >
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption caption-md">
                                                    <i class="icon-bar-chart theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Update Information</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div>
                                                    <form id="login-form" action="<?php echo base_url('employee/edit_detail/'.$id);?>" method="post" id="profile_form" data-parsley-validate="" enctype="multipart/form-data" >
                                                    <input type="hidden" name="admin_id1" valu="<?php if($admin_data){echo $admin_data->user_id;}?>">
                                                        <div>
                                                            <div class="form-group">
                                                                <input class="form-control" name="name" id="name"  type="text" placeholder="Name" value="<?php if($admin_data){echo $admin_data->user_name;}?>" data-parsley-length="[3, 50]" data-parsley-error-message="Name should be minimum 3 character and maximum 50 character long." data-parsley-pattern="^[A-Za-z ]*$">
                                                            </div>
                                                            <?php echo form_error('email'); ?>
                                                            <div class="form-group">
                                                                <input class="form-control" data-parsley-type="email" name="email" type="text" placeholder="Email" data-parsley-type="email" id="email" value="<?php if($admin_data){echo $admin_data->user_email;}?>" required >
                                                            </div>
                                                             <div class="form-group">
                                                                <input type="number" placeholder="Enter Contact number" value="<?php if($admin_data){echo $admin_data->user_mobile;}?>" name="number" id="number" class="form-control" />
                                                            </div>
                                                            <span id="validmobile" style="color:red;margin-bottom: 5%;" ></span>
                                                            <div class="form-group">
                                                                <input class="form-control" name="image" type="file" id="inputfile" >
                                                               <?php if($this->session->flashdata('error_pic')){ echo "<div style='color:red;list-style-type: none;font-size: 0.9em;line-height: 0.9em;'>",$this->session->flashdata('error_pic'),"</div>"; }?>
                                                            <span id="file411"></span>
                                                            </div>
                                                            <div class="form-group">
                                                                    <select class="form-control" name="langOpt3[]" multiple id="langOpt3" >
                                                                        <?php 
                                                                        if(!empty($options))
                                                                        {
                                                                            print_r($options);
                                                                        }
                                                                        ?>
                                                                    </select>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <input  type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption caption-md">
                                                    <i class="icon-bar-chart theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Update password</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-scrollable table-scrollable-borderless">
                                                    <form id="login_form" action="<?php echo base_url('employee/edit_detail/'.$id);?>" method="post" data-parsley-validate="">
                                                        <div class="modal-body">
                                                            <!-- <div class="form-group">
                                                                <input class="form-control" id="old_password" name="O_password" type="password" placeholder="Old Password" required>
                                                                <span id="err" style="color:red;margin-left:0px;list-style-type: none;font-size: 0.9em;line-height: 0.9em;"></span>
                                                                <?php ///if(!empty($this->session->flashdata('fail'))){echo "<span style='color:red'>".$this->session->flashdata('fail')."</span>"; }?>
                                                            </div> -->
                                                            <div class="form-group">
                                                                <input class="form-control" id="password" name="password" type="password" minlength="8" maxlength="16" data-parsley-length-message="Password should be minimum 8 character and maximum 16 character long." placeholder="New Password" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <input class="form-control" data-parsley-equalto="#password" name="c_password" type="password" minlength="8" maxlength="16" data-parsley-length-message="Password should be minimum 8 character and maximum 16 character long." placeholder="Confirm Password" required>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <input type="submit" name="submit1" id="submit1" class="btn btn-primary" value="Submit">

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                             
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
      
        <?php $this->load->view('admin/footer');?>
        <script src="<?php echo base_url();?>template/assets/global/js/jquery.multiselect.js"></script>
      </body>
       
<script type="text/javascript">
  $('#profile_form').parsley();
  $('#login_form').parsley();

$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Select Menu and Submenu',
    search: true,
    selectAll: true
});
</script>
  
<script>
    var inputBox = document.getElementById("number");
    var invalidChars = [
      "-",
      "+",
      "e"
    ];
    inputBox.addEventListener("keydown", function(e) {
      if (invalidChars.includes(e.key)) {
        e.preventDefault();
      }
    });

    $("#submit").click(function(){ 
    var name = $("#name").val();
    var email = $("#email").val();
    var contact = $("#number").val();
    
    var menu = $("#langOpt3").val();
    // Regular Expression For Email
    // Conditions
    var fileExtension = ["jpeg", "jpg", "png"];
        var inputfile =  $("#inputfile").val();

        if(inputfile != '' && $.inArray($("#inputfile").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
               //alert("Only formats are allowed : "+fileExtension.join(', '));
           $("#file411").html('Only accepting : ' +fileExtension.join(', ')+' format').css("color", "red");
           setTimeout(function(){
           $('#file411').html('');
           },4000);
           return false;
        }

    var msg = '';

        if (name != '' && email != '' && contact != '' && menu != '' && menu != null) {
            if (contact.length != 10) {
               msg = "The Contact No. must be at least 10 digit...!"; 
               $("#validmobile").html(msg);
                setTimeout(function(){
               $('#validmobile').html('');
               //$("#validmobile").removeClass("alert alert-danger");
               },2500);
                return false;
            } 
            else
            {
                return true;
            }
            
    } else {
        msg = "All fields are required.....!";
    }
    if(msg !=''){
        $("#error").html(msg).addClass("alert alert-danger");
        setTimeout(function(){
               $('#error').html('');
               $("#error").removeClass("alert alert-danger");
               },3000);
        
        return false;
    }
});

</script>     
</html>