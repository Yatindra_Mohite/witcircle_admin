<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <link href="<?php echo base_url();?>template/assets/global/css/jquery.multiselect.css" rel="stylesheet" type="text/css">
    <title>Add Employee</title>
    
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Employee
                                <!-- <small></small> -->
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Employee</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Employee Detail</div>
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger' id='error2'>",$this->session->flashdata('failed'),"</div>"; 
           }
           if($this->session->flashdata('error_pic'))
           {
             echo "<div class='alert alert-danger' id='error2'>",$this->session->flashdata('error_pic'),"</div>"; 
           }

           ?>
          <div id='error'></div>
            <form action="" id="form11"  class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate=''>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Employee Name<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Enter name" data-parsley-length="[3, 50]" value="<?php echo $this->session->flashdata('name');?>" data-parsley-error-message="Name should be minimum 3 character and maximum 50 character long." name="name" id="name" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Email Id<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="email" placeholder="Enter Email id"  name="email" id="email" class="form-control"  autocomplete="" />
                        </div>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Password<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="password" minlength="8" maxlength="16" value="<?php echo $this->session->flashdata('passw');?>"  data-parsley-error-message="Password should be minimum 8 character and maximum 16 character long." placeholder="Enter Password" name="password" id="password" class="form-control" autocomplete="" />
                        </div>
                    </div>
                </div>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Mobile Number<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="number" placeholder="Enter Contact number" value="<?php echo $this->session->flashdata('num');?>" name="number" id="number" value="" class="form-control" />
                        </div>
                    </div>
                </div>
                <span id="validmobile" style="color:red;margin-left: 26%;" ></span>
                <!-- <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Mobile OTP<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="number" placeholder="Enter OTP number" name="otp" class="form-control" data-parsley-required-message="TOP is required"  required/>
                        </div>
                    </div>
                </div> -->
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Image<span class="required">*</span></label>
                        <div class="col-md-6">
                            <input type="file" name="image"  id="inputfile" />
                        </div>
                    </div>
                     <span id="file411" style="color:red;margin-left: 26%;"></span>
                </div>
                 <div class="form-group">
                        <label class="control-label col-md-3">Select Menu<span class="required" > * </span></label>
                        <div class="col-md-6">
                            <select class="form-control" name="langOpt3[]" multiple id="langOpt3" >
                                <!-- <ul class="list-unstyled"> -->
                                <!-- <option value="Dashboard" selected="selected" readonly checked>Dashboard</option> -->
                                <?php 
                                if(!empty($menudata))
                                {
                                    foreach ($menudata as $key ) { ?>
                                        
                                        <option value="<?php echo $key['name'];?>"><?php echo $key['name']; ?> </option>

                                   <?php }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" name="submit" value="Submit" >&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn default" onClick="history.go(0)">Clear</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
           </div>
            </div>
                </div>
                    </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
     <?php $this->load->view('admin/footer'); ?>
       <script src="<?php echo base_url();?>template/assets/global/js/jquery.multiselect.js"></script>
<script type="text/javascript">
  $('#form11').parsley();  

 $('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Select Menu and Submenu',
    search: true,
    selectAll: true
});

var inputBox = document.getElementById("number");
    var invalidChars = [
      "-",
      "+",
      "e"
    ];
    inputBox.addEventListener("keydown", function(e) {
      if (invalidChars.includes(e.key)) {
        e.preventDefault();
      }
    });
$("#submit").click(function(){ 
    
    var name = $("#name").val();
    var email = $("#email").val();
    var contact = $("#number").val();
    var password = $("#password").val();
    var menu = $("#langOpt3").val();
    // Regular Expression For Email
    // Conditions
        var fileExtension = ['jpeg', 'jpg', 'png'];
        var inputfile =  $("#inputfile").val();

        if(inputfile != '' && $.inArray($("#inputfile").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
               //alert("Only formats are allowed : "+fileExtension.join(', '));
           $("#file411").html('Only accepting : ' +fileExtension.join(', ')+' format').css("color", "red");
           setTimeout(function(){
           $('#file411').html('');
           },4000);
           return false;
        }
    var msg = '';
    
    if (name != '' && email != '' && contact != '' && menu != '' && menu != null && password != '') {
            if (contact.length != 10) {
               msg = "The Contact No. must be at least 10 digit...!"; 
               $("#validmobile").html(msg);
                setTimeout(function(){
               $('#validmobile').html('');
               //$("#validmobile").removeClass("alert alert-danger");
               },2500);
                return false;
            } else if(isNaN(contact)==true) {
                msg = "Invalid contact number"; 
               $("#validmobile").html(msg);
                setTimeout(function(){
               $('#validmobile').html('');
               //$("#validmobile").removeClass("alert alert-danger");
               },2500);
            }else
            {
                return true;
            }
    } else {
          msg = "All fields are required.....!";
    }
    if(msg !=''){
        $("#error").html(msg).addClass("alert alert-danger");
        setTimeout(function(){
               $('#error').html('');
               $("#error").removeClass("alert alert-danger");
               },2500);
        
        return false;
    }
});
</script>

</body>

</html>