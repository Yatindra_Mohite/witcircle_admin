<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Employee List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
        <style type="text/css">
            span.shift {
    position: absolute;
    top: 0;
    right: 0;
}
        </style>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Employee List
                                <!-- <small></small> -->
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Employee List</span>
                        </li>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Employee Detail</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Name</center></th>
                                                <th><center>Status</center></th>
                                                <th><center>Permission</center></th>
                                                <th><center>Edit</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Name</center></th>
                                                <th><center>Status</center></th>
                                                <th><center>View</center></th>
                                                <th><center>Edit</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($sub_admin_data))
                                        {   $i = 0;
                                          	$subid = '';
                                            foreach($sub_admin_data as $key)
                                            { 
                                                $i++;
                                              ?>
                                                <tr>
                                                    <td><center><?php echo $i;?></center></td>
                                                    <td>
                                                        <p style="position: relative;"><?php echo ucwords($key->user_name);?> <br>
                                                        <span class="pull"><strong>Mob: </strong><?php echo $key->user_mobile;?> </span><br>
                                                        <span class="pull"><strong>Email: </strong><?php echo $key->user_email;?></span>
                                                        <span class="pull shift"><img src="<?php echo base_url().'uploads/sub_admin_image/'.$key->user_profile_pic; ?>" height="50px" width="50px"></span>
                                                        </p>
                                                    </td>     
                                                    <td><center><select style="" class="form-control" onchange="changestatus('<?php echo $key->user_id;?>',this.value)">
                                                    <option value="1"<?php if($key->user_status==1){ echo 'selected';} ?>> Active </option>
                                                    <option value="0"<?php if($key->user_status==0){ echo 'selected';} ?>> Inactive </option>
                                                    </select></center></td>
                                                    <td><center><a href="<?php echo 'employee/permission_detail/'.$this->common_model->id_encrypt($key->user_id);?>" class=" btn btn-view">View</a></center></td>    
                                                    <td><center><a href="<?php echo 'employee/edit_detail/'.$this->common_model->id_encrypt($key->user_id);?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span></center></td>
                                                </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                            <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class=""></td>
                                            </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script type="text/javascript">
        function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            var msg ='';
            if(status==1){ msg = 'Activated'; }else{ msg = 'Deactivated'; }
            var r = confirm('Are you really want to '+msg+' this employee');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('employee/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   
                        if(data==1000)
                        {
                            alert("Employee successfully "+msg+'.');
                            //location.reload();
                        }
                   }
                });
            }else{
                location.reload();
            }
        }
    </script>




     


