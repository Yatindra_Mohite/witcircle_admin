<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Add Marking Scheme</title> 
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Marking Scheme
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Add Scheme</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Scheme</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
            

                 <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Questions Marking<span class="required">*</span></label>
                        <div class="col-md-6">

                        <div class="row">
                        <div class="input_fields_container">
                            <div class="col-sm-3">
                              <div class="">
                                <div><input type="text" placeholder="Scheme name" data-parsley-pattern="/^[a-zA-Z\s]*$/" class="form-control" name="section_name[]" required>
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-3">
                              <div class="">
                                <div><input type="text" placeholder="Positive mark" data-parsley-pattern="[0-9]*(\.?[0-9]{2}$)" class="form-control" name="positive_mark[]" required>
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-3">
                              <div class="">
                                <div><input type="text" placeholder="Partial mark" data-parsley-pattern="[0-9]*(\.?[0-9]{2}$)" class="form-control" name="partial_mark[]" required>
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-3">
                              <div class="">
                                <div><input type="text" placeholder="Negative mark" data-parsley-pattern="[0-9]*(\.?[0-9]{2}$)" class="form-control" name="negative_mark[]" required>
                                </div>
                              </div>
                            </div>

                      </div>
                        </div>
                         

                        </div>

                        <div class="col-cm-3"><button class="btn btn-sm btn-primary add_more_button">Add More Fields</button></div>
                    </div>
                </div>

                
                

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green"  id="x" name="submit" value="Submit" >
                            <a href="<?php echo base_url()?>Marking_scheme"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
    $(document).ready(function() {
    var max_fields_limit      = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
            x++; //counter increment
            $('.input_fields_container').append('<div class="fullWidth"> <div class="col-sm-3"><div class=""><div><input type="text" placeholder="Scheme name" class="form-control" name="section_name[]" required></div></div></div><div class="col-sm-3"><div class=""><div><input type="text" placeholder="Positive mark" class="form-control" name="positive_mark[]" required></div></div></div><div class="col-sm-3"><div class=""><div><input type="text" placeholder="Partial mark" class="form-control" name="partial_mark[]" required></div></div></div><div class="col-sm-3"><div class=""><div><input type="text" placeholder="Negative mark" class="form-control" name="negative_mark[]" required></div></div></div><a href="#" class="remove_field" style="margin-left:10px;">Remove</a></div>'); //add input field
        }
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

    </body>

</html>
