<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    <title>Edit Scheme</title> 
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Marking Scheme
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Edit Scheme</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Scheme</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }
           if($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('marking_scheme/edit/'.$this->common_model->id_encrypt($scheme_edit->id));?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
             
                  <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Questions Marking<span class="required">*</span></label>
                        <div class="col-md-6">

                        <div class="row">
                        <div class="input_fields_container">
                            <div class="col-sm-3">
                              <label>Scheme Name</label>
                              <div class="">
                                <div><input type="text" value="<?php echo $scheme_edit->name;?>" placeholder="Scheme name" class="form-control" name="section_name" required>
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-3">
                              <label>Positive Mark</label>
                              <div class="">
                                <div><input type="text"  value="<?php echo $scheme_edit->positive_mark;?>" placeholder="Positive mark" class="form-control" name="positive_mark" required>
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-3">
                              <label>Partial Mark</label>
                              <div class="">
                                <div><input type="text" value="<?php echo $scheme_edit->partial_mark;?>" placeholder="Partial mark" class="form-control" name="partial_mark" required>
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-3">
                              <label>Negative Mark</label>
                              <div class="">
                                <div><input type="text" value="<?php echo $scheme_edit->negative_mark;?>" placeholder="Negative mark" class="form-control" name="negative_mark" required>
                                </div>
                              </div>
                            </div>

                      </div>
                        </div>
                         

                        </div>
          
                    </div>
                     <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green"  id="x" name="submit" value="Update" >
                        </div>
                    </div>
                </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

    </body>

</html>