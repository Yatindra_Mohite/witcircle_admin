<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Content List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                 <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Content List
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard/">Home</a>
                        </li>
                        <li>
                            <span class="active">Content List</span>
                        </li>
                        <a href="<?php echo base_url('subjective/add_content')?>"><button style="margin-top: -7px;" type="button" class="btn btn-primary btn-md pull-right">Add Content</button>  </a>
                    </ul>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Content List</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Course Name </center></th>
                                                <th><center>Subject Name </center></th>
                                                <th><center>Chapter Name </center></th>
                                                <!-- <th><center>Wit card </center></th> -->
                                                <th><center>Chapter Coin</center></th>
                                                <th><center>File View </center></th>
                                                <th><center>Status</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Course Name </center></th>
                                                <th><center>Subject Name </center></th>
                                                <th><center>Chapter Name </center></th>
                                                <!-- <th><center>Wit card </center></th> -->
                                                <th><center>Chapter Coin</center></th>
                                                <th><center>File View </center></th>
                                                <th><center>Status</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($subjective))
                                        {   $i = 0;
                                            foreach($subjective as $key)
                                            { 
                                                $i++;

                                              ?>
                                            <tr>
                                                    <td><center><?php echo $i;?></center></td>   
                                                    <td><center><?php $course = $this->common_model->common_getRow('subjective_and_cards_course',array('id'=>$key->course_id)); echo ucwords($course->name);?></center></td>
                                                    <td><center><?php $course = $this->common_model->common_getRow('in_short_subject',array('id'=>$key->subject_id)); echo ucwords($course->subject_name);?></center></td>
                                                    <td><center><?php $course = $this->common_model->common_getRow('in_short_chapter',array('id'=>$key->chapter_id)); echo ucwords($course->chapter_name);?></center></td>
                                                    <td><center><?php echo $key->chapter_coin;?></center> </td>
                                                    <td><center><a target="_black" href="<?php echo base_url('uploads/subjective/content').'/'.$key->file_name;?>"><span  aria-hidden="true">View</a></span></center></td>

                                                    <td><center><select style="" class="form-control" onchange="changestatus('<?php echo $key->id;?>',this.value)">
                                                    
                                                    <option value="0"<?php if($key->status==0){ echo 'selected';} ?>> Inactive </option>
                                                    <option value="1"<?php if($key->status==1){ echo 'selected';} ?>> Active </option>
                                                    </select></center></td>

                                                    <td><center><a onclick='deletemain("<?php echo $key->id;?>");' href="javascript:void(0);" title="click here to delete"><span class="label label-sm label-danger"><i class="fa fa-trash-o"></center></i></span></a></td>  
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class=""></td>
                                                <td class=""></td>
                                                <td class=""></td>



                                                <!-- <td class=""></td> -->

                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>

<script type="text/javascript">
        function changestatus(id,status)
        { 
            var str = "user_id="+id+"&admin_status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('subjective/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }location.reload();
        }
    </script>
<script>
         function deletemain(id)
            {
                var r = confirm('Are you really want to delete?');
                if(r==true)
                {
                    $.ajax({
                       url:"<?php echo base_url('subjective/delete')?>/"+id,
                       success:function(data)
                       {
                           if(data==1000)
                           {
                                location.reload();
                           }
                       }
                    });
                }
            }
         </script>



     


